 #!/bin/sh

find $1 -name "*.bz2" -exec rm '{}' \;
find $1 -name "*.pyc" -exec rm '{}' \;
find $1 -name "*.sum" -exec rm '{}' \;
