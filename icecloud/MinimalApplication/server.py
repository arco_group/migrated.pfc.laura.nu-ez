#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
from time import sleep



class Server(Ice.Application):
    def run(self, argv):
        sleep(300)

        return 0


server = Server()
sys.exit(server.main(sys.argv))
