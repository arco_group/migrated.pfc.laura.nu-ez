import unittest



from nodeinfo_server import NodeInfoI;

class NodeInfoTests(unittest.TestCase):

    def setUp(self):
        self.nodeinfo = NodeInfoI().getNodeInfo()

    def test_width(self):
        self.assertEqual(self.nodeinfo.width, 64)

    def test_ram(self):
        self.assertEqual(self.nodeinfo.ram, 7762)

    def test_cpumodel(self):
        self.assertEqual(self.nodeinfo.cpuModel,
                         "Intel(R) Core(TM) i5-2450M CPU @ 2.50GHz")

    def test_cpuVendor(self):
        self.assertEqual(self.nodeinfo.cpuVendor, "GenuineIntel")

    def test_nproc(self):
        self.assertEqual(self.nodeinfo.cpuNproc, 4)

    def test_cpuarch(self):
        self.assertEqual(self.nodeinfo.cpuArchitecture, 'x86_64')

    def test_custom(self):
        self.assertEqual(self.nodeinfo.custom, 0)
