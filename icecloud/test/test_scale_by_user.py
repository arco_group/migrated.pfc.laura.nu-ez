import unittest

from doublex import Stub, Spy, assert_that, called
import doublex
import IceGrid
import os
import IceCloud
from scheduler import Scheduler
from monitor import MonitorTimer, Monitor
from nodeinfo_retriever import NodeInfoRetriever
from time import sleep
from icecloud_registry import ICRegistryI
from tempfile import NamedTemporaryFile
from ic_descriptor_builder import ICDescriptorBuilder
from grid_state import GridState


from exc import NoMatchingNodesForCategory, InvalidRule
import deployment_utilities

class ScaleTests(unittest.TestCase):

    def create_scheduler(self, retriever):

        return (Scheduler(retriever))



    def create_registry(self, retriever, scheduler):

        with Spy(IceGrid.AdminSession) as session:
            with Stub() as ig_admin:
                ig_admin.getServerState(doublex.ANY_ARG).returns(\
                                                    IceGrid.ServerState.Active)
                session.getAdmin().returns(ig_admin)

        monitor = Monitor(retriever, scheduler)

        timer = MonitorTimer(monitor)

        self.ig_admin_session = session
        self.ig_admin = session.getAdmin()

        self.categories = self.get_categories_file().name

        return ICRegistryI(self.ig_admin, retriever, timer, self.categories,
                           'test.log')

    def get_categories_file(self):
        xml_string = \
        '''<?xml version="1.0"?>
            <list>

             <category name="any-server">
             </category>

             <category name="great-server64">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
              </feature>
             </category>

             <category name="great-server32">
              <include>great-mem</include>
              <feature id="width">
                  <value>32</value>
              </feature>
             </category>

              <category name="great-server">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
               <value>32</value>
              </feature>
             </category>


             <category name="great-mem">
              <feature id="ram">
                 <min_value>30</min_value>
                 <max_value>400000</max_value>
              </feature>
             </category>

             <category name="big-mem">
              <feature id="ram">
               <min_value>20</min_value>
               <max_value>30</max_value>
              </feature>
             </category>

             <category name="med-mem">
              <feature id="ram">
                 <min_value>10</min_value>
                 <max_value>20</max_value>
              </feature>
             </category>

             <category name="low-mem">
              <feature id="ram">
                 <min_value>0</min_value>
                 <max_value>10</max_value>
              </feature>
             </category>
            </list> '''

        tempf = NamedTemporaryFile(delete=False)
        tempf.write(xml_string)
        tempf.seek(0)
        tempf.close()
        return tempf


    def get_minimal_application(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers"
               growingRule="num_servers gt 3"  activation="manual"
                exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_minimal_application_with_template(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem"
              allocationRule= "min_servers"
               growingRule="num_servers gt 3">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string




    def set_ic_application_descriptor(self, application_string):
        tempf = NamedTemporaryFile()
        tempf.write(application_string)
        tempf.seek(0)
        self.ic_app_descriptor = \
         ICDescriptorBuilder().getApplicationDescriptor(tempf.name)
        tempf.close()


    def test_grow_server(self):
        ''' Test to check the growth of a server
         commanded by user'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }



        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos1)



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(self.get_minimal_application())
        registry.addApplication(self.ic_app_descriptor)
        registry.growServer("Server0")
        # then

        secondary_servers = registry.get_grid_state().get_secondary_servers()
        self.assertIn("Server0", secondary_servers)
        self.assertEqual(len(secondary_servers["Server0"]), 1)

    def test_decrease_server(self):
        ''' Test to check the growth of a server
         commanded by user'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }



        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos1)



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(self.get_minimal_application())
        registry.addApplication(self.ic_app_descriptor)
        registry.growServer("Server0")

        # then
        secondary_servers = registry.get_grid_state().get_secondary_servers()
        self.assertIn("Server0", secondary_servers)
        self.assertEqual(len(secondary_servers["Server0"]), 1)

        registry.decreaseServer("Server0")
        secondary_servers = registry.get_grid_state().get_secondary_servers()
        self.assertNotIn("Server0", secondary_servers)


    def test_grow_template(self):
        ''' Test to check the growth of a template
         commanded by user'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }



        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos1)



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(
                                self.get_minimal_application_with_template())
        registry.addApplication(self.ic_app_descriptor)
        registry.growTemplate("ServerTemplate", "MinimalApplication")
        # then

        self.assertIn("ServerTemplate",
                      registry.get_grid_state().get_template_instances())
        self.assertEquals(len(registry.get_grid_state().get_template_instances()\
                        ["ServerTemplate"]), 2)


    def test_decrease_instance(self):

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }



        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos1)

        registry = self.create_registry(retriever,
                                        self.create_scheduler(retriever))

        # when

        self.set_ic_application_descriptor(
                                self.get_minimal_application_with_template())
        registry.addApplication(self.ic_app_descriptor)
        registry.growTemplate("ServerTemplate", "MinimalApplication")
        registry.decreaseTemplate("ServerTemplate", "MinimalApplication")
        # then

        self.assertIn("ServerTemplate",
                      registry.get_grid_state().get_template_instances())
        self.assertEquals(len(registry.get_grid_state().get_template_instances()\
                        ["ServerTemplate"]), 1)

