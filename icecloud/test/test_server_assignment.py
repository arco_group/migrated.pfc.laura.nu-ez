import unittest

from doublex import Stub, Spy, assert_that, called
import IceGrid

import IceCloud
from icecloud_registry import ICRegistryI
from nodeinfo_retriever import NodeInfoRetriever
from ic_descriptor_builder import ICDescriptorBuilder
from tempfile import NamedTemporaryFile
import deployment_utilities
from scheduler import Scheduler
from monitor import MonitorTimer, Monitor
from exc import NoMatchingNodesForCategory, InvalidRule

class ServerAssignmentTests(unittest.TestCase):

    def create_registry(self, infos):

        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos)

        with Spy(IceGrid.AdminSession) as session:
            with Spy() as ig_admin:
                 session.getAdmin().returns(ig_admin)

        self.ig_admin_session = session
        self.ig_admin = ig_admin
        self.retriever = retriever
        scheduler = Scheduler(None)
        monitor = Monitor(retriever, scheduler)
        timer = MonitorTimer(monitor)

        self.categories = self.get_categories_file().name
        return ICRegistryI(self.ig_admin, retriever, timer, self.categories,
                           'test.log', None)



    def set_ic_application_descriptor(self, application_string):
        tempf = NamedTemporaryFile()
        tempf.write(application_string)
        tempf.seek(0)
        self.ic_app_descriptor = \
         ICDescriptorBuilder().getApplicationDescriptor(tempf.name)
        tempf.close()


    def get_categories_file(self):
        xml_string = \
        '''<?xml version="1.0"?>
            <list>

             <category name="any-server">
             </category>

             <category name="great-server64">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
              </feature>
             </category>

             <category name="great-server32">
              <include>great-mem</include>
              <feature id="width">
                  <value>32</value>
              </feature>
             </category>

              <category name="great-server">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
               <value>32</value>
              </feature>
             </category>


             <category name="great-mem">
              <feature id="ram">
                 <min_value>30</min_value>
                 <max_value>400000</max_value>
              </feature>
             </category>

             <category name="big-mem">
              <feature id="ram">
               <min_value>20</min_value>
               <max_value>30</max_value>
              </feature>
             </category>

             <category name="med-mem">
              <feature id="ram">
                 <min_value>10</min_value>
                 <max_value>20</max_value>
              </feature>
             </category>

             <category name="low-mem">
              <feature id="ram">
                 <min_value>0</min_value>
                 <max_value>10</max_value>
              </feature>
             </category>
            </list> '''

        tempf = NamedTemporaryFile(delete=False)
        tempf.write(xml_string)
        tempf.seek(0)
        tempf.close()
        return tempf


    def get_application_1_great_mem_node(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server1" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string


    def get_application_1_static_server_1_great_mem(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node name="grid-node0">
              <server id="Server0" activation="manual" exe="./server.py">
             </server>
             </node>
             <node category="great-mem">
              <server id="Server1" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_application_1_low_mem_1_great_mem(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="low-mem">
              <server id="Server0" activation="manual" exe="./server.py">
              </server>
             </node>
             <node category="great-mem">
              <server id="Server1" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_1server_app_with_allocation_rule(self, rule):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "''' + rule + \
               '''" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_application_2_great_mem_alloc_min_servers(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers"
                activation="manual" exe="./server.py">
              </server>
              <server id="Server1" allocationRule= "min_servers"
               activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_application_4servers_alloc_min_servers(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers" activation="manual" exe="./server.py">
              </server>
              <server id="Server1" allocationRule= "min_servers" activation="manual" exe="./server.py">
              </server>
              <server id="Server2" allocationRule= "min_servers" activation="manual" exe="./server.py">
              </server>
              <server id="Server3" allocationRule= "min_servers" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_application_2servers_great_mem_alloc_min_servers_min_load_5(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers" activation="manual" exe="./server.py">
              </server>
              <server id="Server1" allocationRule= "min_load_5" activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string


    def test_not_matching_nodes_for_category(self):
        # There are no matching nodes for category great_mem
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(
                                        IceCloud.NodeInfoData(10, 32), 0 , 0)
            }  # not a great mem node

        registry = self.create_registry(infos)



        self.set_ic_application_descriptor(
                            self.get_application_1_great_mem_node())

        with self.assertRaises(IceCloud.NoMatchingNodesForCategory):
            registry.addApplication(self.ic_app_descriptor)

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 0)

    def test_not_matching_node_in_category_for_rule(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0)
            }  # a great mem node, with cpu_load_1 = 5



        registry = self.create_registry(infos)

        self.set_ic_application_descriptor(
                self.get_1server_app_with_allocation_rule("cpu_load_1 gt 5"))

        with self.assertRaises(IceCloud.NoMatchingNodesInCategoryForRule):
            registry.addApplication(self.ic_app_descriptor)

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 0)

    def test_node_assignment_is_right_1_server(self):
        # given

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # a non great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # when
        registry = self.create_registry(infos)


        self.set_ic_application_descriptor(
                            self.get_application_1_great_mem_node())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id, "Server1")
        self.assertNotIn("grid-node0", ig_app_descriptor.nodes)

        assert_that(self.ig_admin.addApplication, called())
        originalServerAssignment = registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 1)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server1"].node, "grid-node1")

    def test_node_assignment_is_right_1_server_1_static(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # non great mem
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # great-mem node
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                            self.get_application_1_static_server_1_great_mem())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                        "Server1")
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        assert_that(self.ig_admin.addApplication, called())
        originalServerAssignment = registry.get_grid_state().\
                                            get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 1)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server1"].node,
                          "grid-node1")

    def test_node_assignment_is_right_2_servers(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                5, 32), 0, 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                                self.get_application_1_low_mem_1_great_mem())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                    self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server1")
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        assert_that(self.ig_admin.addApplication, called())

        originalServerAssignment = registry.get_grid_state().\
                                                get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 2)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server1"].node,
                          "grid-node1")
        self.assertEquals(originalServerAssignment["Server0"].node,
                           "grid-node0")


    def test_node_assignment_is_right_1server_2nodes_same_category(self):

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                5, 32), 0, 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0),
            "grid-node2": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(self.
                                get_application_1_great_mem_node())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        servers_in_great_mem_node = []

        if "grid-node1" in ig_app_descriptor.nodes:
            for server in ig_app_descriptor.nodes["grid-node1"].servers:
                servers_in_great_mem_node.append(server)
        if "grid-node0" in ig_app_descriptor.nodes:
            for server in ig_app_descriptor.nodes["grid-node0"].servers:
                servers_in_great_mem_node.append(server)
        self.assertEquals(servers_in_great_mem_node[0].id, "Server1")

        registry.addApplication(self.ic_app_descriptor)
        assert_that(self.ig_admin.addApplication, called())

        originalServerAssignment = registry.get_grid_state().\
                                            get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 1)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue(
         originalServerAssignment["Server1"].node == "grid-node0" or
         originalServerAssignment["Server1"].node == "grid-node1")



    def test_node_assignment_is_right_2_servers_same_category_with_rule_min_server(self):

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32), 0, 0),  # great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                        self.get_application_2_great_mem_alloc_min_servers())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(len(ig_app_descriptor.nodes["grid-node0"].servers), 1)
        self.assertEquals(len(ig_app_descriptor.nodes["grid-node1"].servers), 1)
        assert_that(self.ig_admin.addApplication, called())

        originalServerAssignment = registry.get_grid_state().\
                                                get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 2)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue("Server0" in originalServerAssignment)

        self.assertTrue(
         (originalServerAssignment["Server0"].node == "grid-node0" and
         originalServerAssignment["Server1"].node == "grid-node1") or
         (originalServerAssignment["Server0"].node == "grid-node1" and
         originalServerAssignment["Server1"].node == "grid-node0"))


    def test_node_assignment_is_right_4_servers_same_category_with_rule_min_server(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32), 0, 0),  # great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                            self.get_application_4servers_alloc_min_servers())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(len(ig_app_descriptor.nodes["grid-node0"].servers), 2)
        self.assertEquals(len(ig_app_descriptor.nodes["grid-node1"].servers), 2)
        assert_that(self.ig_admin.addApplication, called())

        originalServerAssignment = registry.get_grid_state().\
                                                get_original_server_assignment()

        self.assertEquals(len(originalServerAssignment), 4)
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue("Server2" in originalServerAssignment)
        self.assertTrue("Server3" in originalServerAssignment)

        serversInNode0 = 0
        serversInNode1 = 0
        for server in originalServerAssignment:
            if originalServerAssignment[server].node == "grid-node0":
                serversInNode0 += 1
            elif originalServerAssignment[server].node == "grid-node1":
                serversInNode1 += 1
        self.assertEquals(serversInNode0, 2)
        self.assertEqual(serversInNode1, 2)


    def test_right_allocation_2_servers_same_category_different_rule_different_node(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32), IceGrid.LoadInfo(40, 40, 12) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), IceGrid.LoadInfo(10, 20, 12), 1)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
         self.get_application_2servers_great_mem_alloc_min_servers_min_load_5())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                    self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")
        self.assertTrue(len(ig_app_descriptor.nodes["grid-node0"].servers) == 1)
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server1")
        self.assertTrue(len(ig_app_descriptor.nodes["grid-node1"].servers) == 1)
        assert_that(self.ig_admin.addApplication, called())

        originalServerAssignment = registry.get_grid_state().\
                    get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 2)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue("Server0" in originalServerAssignment)

        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")
        self.assertEquals(originalServerAssignment["Server1"].node,
                          "grid-node1")



    def test_right_allocation_2_servers_same_category_different_rule_same_node(
                                                                        self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }
        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
         self.get_application_2servers_great_mem_alloc_min_servers_min_load_5())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                           "Server0")
        self.assertTrue(len(ig_app_descriptor.nodes["grid-node0"].servers) == 2)
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[1].id,
                           "Server1")
        self.assertNotIn("grid_node1", ig_app_descriptor.nodes)
        assert_that(self.ig_admin.addApplication, called())


        originalServerAssignment = registry.get_grid_state().\
                                                get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 2)
        self.assertTrue("Server1" in originalServerAssignment)
        self.assertTrue("Server0" in originalServerAssignment)

        self.assertEquals(originalServerAssignment["Server0"].node,
                           "grid-node0")
        self.assertEquals(originalServerAssignment["Server1"].node,
                          "grid-node0")


    def test_right_allocation_min_servers(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_servers"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = registry.get_grid_state().\
                                            get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_max_servers(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_servers"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                     self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = registry.get_grid_state().\
                                               get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node1")

    def test_right_allocation_min_load1(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_load_1"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_max_load_1(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_load_1"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node1")

    def test_right_allocation_min_load5(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100,), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100,), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_load_5"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_max_load_5(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_load_5"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                     self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node1")

    def test_right_allocation_min_load15(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_load_15"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_max_load_15(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given

        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_load_15"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                           "grid-node1")

    def test_right_allocation_min_custom(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 0, 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0, 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_custom"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node1")


    def test_right_allocation_max_custom(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 0, 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0, 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_custom"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_min_freeram(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }


        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("min_free_ram"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node1")

    def test_right_allocation_max_freeram(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }


        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_free_ram"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_right_allocation_complex_rule(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }


        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("cpu_load_1 = 5"))
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then

        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].servers[0].id,
                          "Server0")

        originalServerAssignment = \
                     registry.get_grid_state().get_original_server_assignment()
        self.assertTrue("Server0" in originalServerAssignment)
        self.assertEquals(originalServerAssignment["Server0"].node,
                          "grid-node0")

    def test_invalid_rule(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64, "", "", 0, "", 0),
                IceGrid.LoadInfo(10, 20, 12), 3)
            }


        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                    self.get_1server_app_with_allocation_rule("max_fee_ram"))

        with self.assertRaises(InvalidRule):
            ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 0)


