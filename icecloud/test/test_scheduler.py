import unittest

from doublex import Stub, Spy, assert_that, called
import doublex
import IceGrid
import os
import IceCloud
from scheduler import Scheduler
from monitor import MonitorTimer, Monitor
from nodeinfo_retriever import NodeInfoRetriever
from icecloud_registry import ICRegistryI
from tempfile import NamedTemporaryFile
from ic_descriptor_builder import ICDescriptorBuilder


from exc import NoMatchingNodesForCategory, InvalidRule
import deployment_utilities

class ServerAssignmentTests(unittest.TestCase):

    def create_scheduler(self, retriever):

        return (Scheduler(retriever))



    def create_registry(self, retriever, scheduler):

        with Spy(IceGrid.AdminSession) as session:
            with Stub() as ig_admin:
                ig_admin.getServerState(doublex.ANY_ARG).returns(\
                                                    IceGrid.ServerState.Active)
                session.getAdmin().returns(ig_admin)

        monitor = Monitor(retriever, scheduler)

        timer = MonitorTimer(monitor)

        self.ig_admin_session = session
        self.ig_admin = session.getAdmin()

        self.categories = self.get_categories_file().name

        return ICRegistryI(self.ig_admin, retriever, timer, self.categories,
                           'test.log')

    def get_categories_file(self):
        xml_string = \
        '''<?xml version="1.0"?>
            <list>

             <category name="any-server">
             </category>

             <category name="great-server64">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
              </feature>
             </category>

             <category name="great-server32">
              <include>great-mem</include>
              <feature id="width">
                  <value>32</value>
              </feature>
             </category>

              <category name="great-server">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
               <value>32</value>
              </feature>
             </category>


             <category name="great-mem">
              <feature id="ram">
                 <min_value>30</min_value>
                 <max_value>400000</max_value>
              </feature>
             </category>

             <category name="big-mem">
              <feature id="ram">
               <min_value>20</min_value>
               <max_value>30</max_value>
              </feature>
             </category>

             <category name="med-mem">
              <feature id="ram">
                 <min_value>10</min_value>
                 <max_value>20</max_value>
              </feature>
             </category>

             <category name="low-mem">
              <feature id="ram">
                 <min_value>0</min_value>
                 <max_value>10</max_value>
              </feature>
             </category>
            </list> '''

        tempf = NamedTemporaryFile(delete=False)
        tempf.write(xml_string)
        tempf.seek(0)
        tempf.close()
        return tempf


    def get_minimal_application(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers"
               growingRule="num_servers gt 3"  activation="manual"
                exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_minimal_application_with_template(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem"
              allocationRule= "min_servers"
               growingRule="num_servers gt 3">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_minimal_application_custom(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_custom"
               growingRule="num_custom gt 3"  activation="manual"
                exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_minimal_application2(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers"
               growingRule="num_servers gt 4"
               decreasingRule="num_servers le   2"
               activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string

    def get_minimal_application2_with_template(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem"
              allocationRule= "min_servers"
               growingRule="num_servers gt 4"
               decreasingRule="num_servers le   2">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_max_dupes(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <node category="great-mem">
              <server id="Server0" allocationRule= "min_servers"
               growingRule="num_servers gt 4"
               decreasingRule="num_servers le   2"
               maxDupes = "0"
               activation="manual" exe="./server.py">
              </server>
             </node>
            </application>
           </icecloud>'''
        return application_string


    def set_ic_application_descriptor(self, application_string):
        tempf = NamedTemporaryFile()
        tempf.write(application_string)
        tempf.seek(0)
        self.ic_app_descriptor = \
         ICDescriptorBuilder().getApplicationDescriptor(tempf.name)
        tempf.close()

    def test_check_rule_ge_OK(self):


        # given
        node = IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 4)

        # when
        scheduler = self.create_scheduler(None)

        # then
        self.assertTrue(
                deployment_utilities.check_rule('cpu_load_5 >= 2', node))
        self.assertTrue(
                deployment_utilities.check_rule('cpu_load_5 ge 2', node))
        self.assertTrue(
                deployment_utilities.check_rule('cpu_load_5 >= 10', node))
        self.assertFalse(
                deployment_utilities.check_rule('cpu_load_5 >= 20', node))
        # self.assertFalse(deployment_utilities.check_rule('max_servers = 2', node))

    def test_check_rule_le_OK(self):


        # given
        node = IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 4)

        # when
        scheduler = self.create_scheduler(None)

        # then
        self.assertTrue(deployment_utilities.check_rule(
                                                    'cpu_load_5 <= 10', node))
        self.assertTrue(deployment_utilities.check_rule(
                                                    'cpu_load_5 le 10', node))
        self.assertTrue(deployment_utilities.check_rule(
                                                    'cpu_load_5 le 10', node))
        self.assertFalse(deployment_utilities.check_rule(
                                                    'cpu_load_5 >= 20', node))
        # self.assertFalse(deployment_utilities.check_rule('max_servers = 2', node))

    def test_check_rule_invalid(self):
        '''Test for rules with errors'''

        # given
        node = IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0)

        # when
        scheduler = self.create_scheduler(None)

        # then
        self.assertRaises(InvalidRule,
                deployment_utilities.check_rule, 'asdfasdf >= 2', node)
        self.assertRaises(InvalidRule,
                deployment_utilities.check_rule, 'cpu_load >= a', node)
        self.assertRaises(InvalidRule,
                deployment_utilities.check_rule, 'cpu_load asdf 1', node)


    def test_grow(self):
        ''' Test to check the growth of a server
         depending on its growing rule'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2])



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(self.get_minimal_application())
        registry.addApplication(self.ic_app_descriptor)
        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # Server 0 should have been duplicated, then a Server0.0 should exist
        # in grid-node1

        serverIn = False
        for server in registry.get_grid_state().get_secondary_servers()[
                                                                    "Server0"]:
            if server.id == "Server0.0" and server.node == "grid-node1":
                serverIn = True

        self.assertTrue(serverIn)
#

    def test_grow_custom(self):
        ''' Test to check the growth of a server
         depending on its growing rule'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32, "", "", 0, "", 0), IceGrid.LoadInfo(
                                                                    5, 10, 5)),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64, "", "", 0, "", 3), IceGrid.LoadInfo(
                                                                10, 20, 12))
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32, "", "", 0, "", 6), IceGrid.LoadInfo(
                                                                    5, 10, 5)),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64, "", "", 0, "", 3), IceGrid.LoadInfo(
                                                                10, 20, 12))
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2])



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(self.get_minimal_application_custom())
        registry.addApplication(self.ic_app_descriptor)
        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # Server 0 should have been duplicated, then a Server0.0 should exist
        # in grid-node1
        serverIn = False
        for server in registry.get_grid_state().get_secondary_servers()[
                                                                    "Server0"]:
            if server.id == "Server0.0" and server.node == "grid-node1":
                serverIn = True

        self.assertTrue(serverIn)

    def test_not_grow_max_dupes(self):


        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32, "", "", 0, "", 0), IceGrid.LoadInfo(
                                                                    5, 10, 5)),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64, "", "", 0, "", 3), IceGrid.LoadInfo(
                                                                    10, 20, 12))
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32, "", "", 0, "", 6), IceGrid.LoadInfo(
                                                                    5, 10, 5)),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64, "", "", 0, "", 3), IceGrid.LoadInfo(
                                                                10, 20, 12))
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2])



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(self.get_application_max_dupes())
        registry.addApplication(self.ic_app_descriptor)
        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # Server 0 should have been duplicated, then a Server0.0 should exist
        # in grid-node1
        n_dupes = len(registry.get_grid_state().get_secondary_servers())

        self.assertEquals(n_dupes, 0)


    def test_decrease(self):

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        infos3 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 3),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 1)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2,
                                                   infos3])

        # infos1 returned when creating monitor
        # infos1 returned when add_application
        # infos2 returned when call_monitor 1
        # infos3 returned when call_monitor 2

        registry = self.create_registry(retriever,
                                        self.create_scheduler(retriever))

        # when

        self.set_ic_application_descriptor(self.get_minimal_application2())
        registry.addApplication(self.ic_app_descriptor)
            # then Server0 in grid-node0 (min_servers)
        registry.timer.call_monitor()
            # then Server0.0 in grid-node1

        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # Server0.0 not in secondary_servers

        self.assertNotIn("Server0",
                         registry.get_grid_state().get_secondary_servers())

    def test_decrease_original_node(self):
         # given

        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        infos3 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 2),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2,
                                                   infos3])

        # infos1 returned while creating monitor
        # infos1 returned when add_application
        # infos2 returned when call_monitor 1
        # infos3 returned when call_monitor 2

        registry = self.create_registry(retriever,
                                         self.create_scheduler(retriever))

        # when
        self.set_ic_application_descriptor(self.get_minimal_application2())
        registry.addApplication(self.ic_app_descriptor)
            # then Server0 in grid-node0 (min_servers)
        registry.timer.call_monitor()
            # then Server0.0 in grid-node1 - Server 0 grows
        registry.timer.call_monitor()
            # then Server0 decreases
        registry.timer.stop()

        # then
        # Server0.0 not in secondary_servers
        self.assertNotIn("Server0",
                         registry.get_grid_state().get_secondary_servers())


    def test_grow_template(self):
        ''' Test to check the growth of a template
         depending on its growing rule'''

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2])



        # when
        registry = self.create_registry(retriever, self.create_scheduler(
                                                                    retriever))
        self.set_ic_application_descriptor(
                                self.get_minimal_application_with_template())
        registry.addApplication(self.ic_app_descriptor)
        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # Server 0 should have been duplicated, then a Server0.0 should exist
        # in grid-node1

        self.assertIn("ServerTemplate",
                      registry.get_grid_state().get_template_instances())
        self.assertEquals(len(registry.get_grid_state().get_template_instances()\
                        ["ServerTemplate"]), 2)


    def test_decrease_instance(self):

        # given
        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        infos3 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 3),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 1)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2,
                                                   infos3])

        # infos1 returned when creating monitor
        # infos1 returned when add_application
        # infos2 returned when call_monitor 1
        # infos3 returned when call_monitor 2

        registry = self.create_registry(retriever,
                                        self.create_scheduler(retriever))

        # when

        self.set_ic_application_descriptor(self.get_minimal_application2_with_template())
        registry.addApplication(self.ic_app_descriptor)
            # then instance with count 0 in grid-node0 (min_servers)
        registry.timer.call_monitor()
            # then instance with count 1  in grid-node1

        registry.timer.call_monitor()
        registry.timer.stop()

        # then
        # instance with count 1 not in grid_state templates

        self.assertEqual(len(registry.get_grid_state().get_template_instances()\
                         ['ServerTemplate']), 1)
        self.assertEqual(registry.get_grid_state().get_template_instances()\
                         ['ServerTemplate'][0].count, 0)


    def test_decrease_original_instance(self):
         # given

        infos1 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 3)
            }

        infos2 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 5),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        infos3 = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "windows", 32), IceGrid.LoadInfo(5, 10, 5) , 2),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, "linux", 64), IceGrid.LoadInfo(10, 20, 12), 0)
            }

        with Stub() as retriever:
            retriever.getAllNodeInfos().delegates([infos1, infos1, infos2,
                                                   infos3])

        # infos1 returned while creating monitor
        # infos1 returned when add_application
        # infos2 returned when call_monitor 1
        # infos3 returned when call_monitor 2

        registry = self.create_registry(retriever,
                                         self.create_scheduler(retriever))

        # when
        self.set_ic_application_descriptor(self.get_minimal_application2_with_template())
        registry.addApplication(self.ic_app_descriptor)
            # then Server0 in grid-node0 (min_servers)
        registry.timer.call_monitor()
            # then Server1 in grid-node1 - Server0 grows
        registry.timer.call_monitor()
            # then Server0 decreases
        registry.timer.stop()

        # then
        # Server1 not in template_instances
        self.assertEqual(len(registry.get_grid_state().get_template_instances()\
                         ['ServerTemplate']), 1)
        self.assertEqual(registry.get_grid_state().get_template_instances()\
                         ['ServerTemplate'][0].count, 0)
        self.assertEqual(registry.get_grid_state().get_template_instances()\
                         ['ServerTemplate'][0].name, 'Server0')
