import unittest

from doublex import Stub, Spy, assert_that, called
import IceGrid

import IceCloud
from icecloud_registry import ICRegistryI
from nodeinfo_retriever import NodeInfoRetriever
from ic_descriptor_builder import ICDescriptorBuilder
from tempfile import NamedTemporaryFile
from time import sleep
import deployment_utilities
from scheduler import Scheduler
from monitor import MonitorTimer, Monitor
import os
from exc import NoMatchingNodesForCategory, InvalidRule

class InstanceAssignmentTests(unittest.TestCase):

    def create_registry(self, infos):

        with Stub() as retriever:
            retriever.getAllNodeInfos().returns(infos)

        with Spy(IceGrid.AdminSession) as session:
            with Spy() as ig_admin:
                 session.getAdmin().returns(ig_admin)

        self.ig_admin_session = session
        self.ig_admin = ig_admin
        self.retriever = retriever
        scheduler = Scheduler(None)
        monitor = Monitor(retriever, scheduler)
        timer = MonitorTimer(monitor)

#         self.categories = os.path.join(os.path.dirname(__file__),
#                                        "../src/config/categories.xml")
        self.categories = self.get_categories_file().name
        return ICRegistryI(self.ig_admin, retriever, timer, self.categories,
                           'test.log', None)



    def set_ic_application_descriptor(self, application_string):
        tempf = NamedTemporaryFile()
        tempf.write(application_string)
        tempf.seek(0)
        self.ic_app_descriptor = \
         ICDescriptorBuilder().getApplicationDescriptor(tempf.name)
        tempf.close()


    def get_categories_file(self):
        xml_string = \
        '''<?xml version="1.0"?>
            <list>

             <category name="any-server">
             </category>

             <category name="great-server64">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
              </feature>
             </category>

             <category name="great-server32">
              <include>great-mem</include>
              <feature id="width">
                  <value>32</value>
              </feature>
             </category>

              <category name="great-server">
              <include>great-mem</include>
              <feature id="width">
               <value>64</value>
               <value>32</value>
              </feature>
             </category>


             <category name="great-mem">
              <feature id="ram">
                 <min_value>30</min_value>
                 <max_value>400000</max_value>
              </feature>
             </category>

             <category name="big-mem">
              <feature id="ram">
               <min_value>20</min_value>
               <max_value>30</max_value>
              </feature>
             </category>

             <category name="med-mem">
              <feature id="ram">
                 <min_value>10</min_value>
                 <max_value>20</max_value>
              </feature>
             </category>

             <category name="low-mem">
              <feature id="ram">
                 <min_value>0</min_value>
                 <max_value>10</max_value>
              </feature>
             </category>
            </list> '''

        tempf = NamedTemporaryFile(delete=False)
        tempf.write(xml_string)
        tempf.seek(0)
        tempf.close()
        return tempf


    def get_application_1_great_mem_node(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_2_instances_great_mem_node(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_1_static_instance_1_great_mem(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <node name="grid-node0">
                <server-instance template="ServerTemplate"/>
             </node>
            <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_2_dif_instances_1_server(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem">
              <server id="ServerInstance" activation="manual" exe=".">
              </server>
             </category-server-template>
             <node name="grid-node1">
                <server-instance template="ServerTemplate"/>
             </node>
             <node category="great-mem">
                <server id="Server1" activation="manual" exe="./server.py"/>
             </node>
            <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_1_low_mem_1_great_mem(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
            <category-server-template id="ServerTemplateLow"
              category="low-mem">
              <server id="ServerLow" activation="manual" exe=".">
              </server>
             </category-server-template>
             <category-server-template id="ServerTemplateGreat"
              category="great-mem">
              <server id="ServerGreat" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplateLow"/>
             <server-instance template="ServerTemplateGreat"/>
            </application>
           </icecloud>'''
        return application_string

    def get_1template_app_with_allocation_rule(self, rule):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <category-server-template id="ServerTemplate"
              category="great-mem" allocationRule="''' + rule + '''">
              <server id="Server" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_application_2_great_mem_alloc_min_servers(self):
        application_string = \
          '''<icecloud>
            <application name="MinimalApplication">
            <category-server-template id="ServerTemplateGreat"
              category="great-mem" allocationRule="min_servers">
              <server id="ServerGreat1" activation="manual" exe=".">
              </server>
             </category-server-template>
             <server-instance template="ServerTemplateGreat"/>
             <server-instance template="ServerTemplateGreat"/>
            </application>
           </icecloud>'''
        return application_string



    def test_not_matching_nodes_for_category(self):
        # There are no matching nodes for category great_mem
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(
                                        IceCloud.NodeInfoData(10, 32), 0 , 0)
            }  # not a great mem node

        registry = self.create_registry(infos)



        self.set_ic_application_descriptor(
                            self.get_application_1_great_mem_node())

        with self.assertRaises(IceCloud.NoMatchingNodesForCategory):
            registry.addApplication(self.ic_app_descriptor)

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 0)

    def test_not_matching_node_in_category_for_rule(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32, "", "", 0, "", 3),
                IceGrid.LoadInfo(5, 10, 5) , 0)
            }  # a great mem node, with cpu_load_1 = 5



        registry = self.create_registry(infos)

        self.set_ic_application_descriptor(
                self.get_1template_app_with_allocation_rule("cpu_load_1 gt 5"))

        with self.assertRaises(IceCloud.NoMatchingNodesInCategoryForRule):
            registry.addApplication(self.ic_app_descriptor)

        originalServerAssignment = \
                    registry.get_grid_state().get_original_server_assignment()
        self.assertEquals(len(originalServerAssignment), 0)

    def test_node_assignment_is_right_1_instance(self):
        # given

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # a non great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # when
        registry = self.create_registry(infos)


        self.set_ic_application_descriptor(
                            self.get_application_1_great_mem_node())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)

        registry.addApplication(self.ic_app_descriptor)

        # then
        instances = ig_app_descriptor.nodes["grid-node1"].serverInstances
        self.assertNotIn("grid-node0", ig_app_descriptor.nodes)

        assert_that(self.ig_admin.addApplication, called())
        original_template_instances = \
                    registry.get_grid_state().get_template_instances()
        self.assertEquals(len(original_template_instances), 1)
        self.assertIn("ServerTemplate", original_template_instances)
        self.assertEquals(original_template_instances["ServerTemplate"][0].node,
                           "grid-node1")


    def test_node_assignment_is_right_2_instances(self):
        # given

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # a non great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # when
        registry = self.create_registry(infos)


        self.set_ic_application_descriptor(
                            self.get_application_2_instances_great_mem_node())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)

        registry.addApplication(self.ic_app_descriptor)

        # then
        instances = ig_app_descriptor.nodes["grid-node1"].serverInstances
        self.assertNotIn("grid-node0", ig_app_descriptor.nodes)

        assert_that(self.ig_admin.addApplication, called())
        original_template_instances = \
                    registry.get_grid_state().get_template_instances()
        self.assertEquals(len(original_template_instances), 1)
        self.assertIn("ServerTemplate", original_template_instances)
        self.assertEquals(len(original_template_instances["ServerTemplate"]), 2)
        self.assertNotEqual(original_template_instances["ServerTemplate"][0].count,
                        original_template_instances["ServerTemplate"][1].count)



    def test_node_assignment_is_right_1_server_1_static(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # non great mem
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # great-mem node
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                            self.get_application_1_static_instance_1_great_mem())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].
                          serverInstances[0].template , "ServerTemplate")
        self.assertEquals(ig_app_descriptor.nodes["grid-node1"].
                          serverInstances[0].parameterValues['servercount'], '0')

        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].
                          serverInstances[0].template , "ServerTemplate")
        self.assertEquals(ig_app_descriptor.nodes["grid-node0"].
                          serverInstances[0].parameterValues['servercount'], '1')

        assert_that(self.ig_admin.addApplication, called())
        instanceAssignment = registry.get_grid_state().\
                                            get_template_instances()
        self.assertEquals(len(instanceAssignment), 1)
        self.assertTrue(len(instanceAssignment["ServerTemplate"]), 2)


    def test_node_assignment_is_right_2_instances_different_node(self):
        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                5, 32), 0, 0),
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                                self.get_application_1_low_mem_1_great_mem())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                    self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        serverInstances0 = ig_app_descriptor.nodes["grid-node0"].serverInstances
        serverInstances1 = ig_app_descriptor.nodes["grid-node1"].serverInstances

        self.assertEquals(serverInstances0[0].template, "ServerTemplateLow")
        self.assertEquals(serverInstances1[0].template, "ServerTemplateGreat")


        assert_that(self.ig_admin.addApplication, called())

        instanceAssignment = registry.get_grid_state().\
                                            get_template_instances()
        self.assertEquals(len(instanceAssignment), 2)
        self.assertTrue("ServerTemplateLow" in instanceAssignment)
        self.assertTrue("ServerTemplateGreat" in instanceAssignment)



    def test_node_assignment_is_right_2_servers_same_category_with_rule_min_server(self):

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 32), 0, 0),  # great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # given
        registry = self.create_registry(infos)

        # when
        self.set_ic_application_descriptor(
                        self.get_application_2_great_mem_alloc_min_servers())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)
        registry.addApplication(self.ic_app_descriptor)

        # then
        serverInstances0 = ig_app_descriptor.nodes["grid-node0"].serverInstances
        serverInstances1 = ig_app_descriptor.nodes["grid-node1"].serverInstances

        self.assertEquals(len(serverInstances0), len(serverInstances1))
        self.assertEquals(len(serverInstances0), 1)


        assert_that(self.ig_admin.addApplication, called())

        instanceAssignment = registry.get_grid_state().\
                                            get_template_instances()
        self.assertEquals(len(instanceAssignment), 1)
        self.assertTrue("ServerTemplateGreat" in instanceAssignment)
        self.assertEqual(len(instanceAssignment["ServerTemplateGreat"]), 2)
        self.assertNotEqual(
            instanceAssignment["ServerTemplateGreat"][0].count,
            instanceAssignment["ServerTemplateGreat"][1].count)


    def test_node_assignment_is_right_2_instances_1_server(self):
        # given

        infos = {
            "grid-node0": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                10, 32), 0, 0),  # a non great-mem node
            "grid-node1": IceCloud.ExtendedNodeInfo(IceCloud.NodeInfoData(
                40 + 100, 64), 0, 0)  # a great-mem node
            }

        # when
        registry = self.create_registry(infos)


        self.set_ic_application_descriptor(
                            self.get_application_2_dif_instances_1_server())
        ig_app_descriptor = deployment_utilities.ICapp_to_IGapp(
                        self.ic_app_descriptor, self.retriever, self.categories)

        registry.addApplication(self.ic_app_descriptor)

        # then
        instances = ig_app_descriptor.nodes["grid-node1"].serverInstances
        self.assertNotIn("grid-node0", ig_app_descriptor.nodes)

        assert_that(self.ig_admin.addApplication, called())
        original_template_instances = \
                    registry.get_grid_state().get_template_instances()
        self.assertEquals(len(original_template_instances), 1)
        self.assertIn("ServerTemplate", original_template_instances)
        self.assertEquals(original_template_instances["ServerTemplate"][0].node,
                           "grid-node1")
