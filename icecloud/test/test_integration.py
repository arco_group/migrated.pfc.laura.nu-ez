import unittest

import IceGrid
import os
import constants
import IceCloud
import Ice
import IceStorm
from icecloud_registry import ICRegistryI
from nodeinfo_retriever import NodeInfoRetriever
from ic_descriptor_builder import ICDescriptorBuilder
from tempfile import NamedTemporaryFile
import deployment_utilities
from scheduler import Scheduler
from monitor import MonitorTimer, Monitor

from exc import NoMatchingNodesForCategory
import pxssh
import subprocess
import thread
import threading

class SupervisorI(IceCloud.Supervisor):
    def __init__(self, e=None):
        self.e = e
    def report(self, msg, current=None):
        print "Event received:", msg
        if self.e is not None:
           self.e.set()
           self.e.clear()

class Subscriber(Ice.Application):

    def __init__(self, communicator=None):
        self.ic = communicator

    def get_topic_manager(self):

        proxy = self.ic.stringToProxy("IceCloudApp.IceStorm/TopicManager")
        if proxy is None:
            print "Invalid proxy"
            return None


        return IceStorm.TopicManagerPrx.checkedCast(proxy)

    def run(self, e):



        topic_mgr = self.get_topic_manager()
        if not topic_mgr:
            print ': invalid proxy'
            return 2

        servant = SupervisorI(e)
        adapter = self.ic.createObjectAdapter("SubscriberAdapter")
        subscriber = adapter.addWithUUID(servant)

        topic_name = "SchedulerTopic"
        qos = {}
        try:
            topic = topic_mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:
            topic = topic_mgr.create(topic_name)

        topic.subscribe(qos, subscriber)
        print 'Waiting events...', subscriber

        adapter.activate()
        # self.shutdownOnInterrupt()
        self.ic.waitForShutdown()

        topic.unsubscribe(subscriber)

        return 0

class IntegrationTests(unittest.TestCase):


    def get_test_application(self):
        application_string = \
        '''<icecloud>
            <application name="MinimalApplication">
             <distrib icepatch="${application}.IcePatch2/server"/>
             <server-template id="IcePatch2">
              <parameter name="instance-name"
                default="${application}.IcePatch2"/>
              <parameter name="endpoints" default="default"/>
              <parameter name="directory"/>
              <server id="${instance-name}" activation="on-demand"
                application-distrib="false" exe="icepatch2server">
               <properties>
                <property name="IcePatch2.InstanceName" value="${instance-name}"/>
                <property name="IcePatch2.Directory" value="${directory}"/>
               </properties>
               <adapter name="IcePatch2" endpoints="${endpoints}"
                 id="${server}.IcePatch2">
                 <object identity="${instance-name}/server"
                 type="::IcePatch2::FileServer"/>
               </adapter>
              </server>
             </server-template>
             <category-server-template id="ServerTemplate"
              category="any-server"
              allocationRule= "min_custom"
              growingRule="num_custom gt 4"
              decreasingRule="num_custom lt  2"
              maxInstances = "2">
                <server id="ServerInstance" activation="manual"
                exe="./server.py" pwd="${application.distrib}">
                </server>
             </category-server-template>
             <node name = "grid-node0">
              <server-instance template="IcePatch2"
              directory="/home/laura/Clase/PFC/pfc.icecloud/icecloud/MinimalApplication"/>
             </node>
             <node category="any-mem">
              <server id="Server0" allocationRule= "min_custom"
               growingRule="num_custom gt 4"
               decreasingRule="num_custom lt  2"
               maxDupes = "1"
               exe="./server.py" pwd="${application.distrib}">
              </server>
             </node>
            <server-instance template="ServerTemplate"/>
            </application>
           </icecloud>'''
        return application_string

    def get_test_application_file(self):
        tempf = NamedTemporaryFile()
        tempf.write(self.get_test_application())
        tempf.seek(0)
        return tempf

    def set_ic_application_descriptor(self, application_string):
        tempf = NamedTemporaryFile()
        tempf.write(application_string)
        tempf.seek(0)
        self.ic_app_descriptor = \
         ICDescriptorBuilder().getApplicationDescriptor(tempf.name)
        tempf.close()

    def set_subscriber(self, broker):
        self.subscriber = self.createSubscriber(broker)

    def createSubscriber(self, broker):
        return Subscriber(broker)

    def test_running_application(self):

        # constants.TIMER_PERIOD = 4
        s = pxssh.pxssh()
        s.login ("node1.local", "root", "admin")
        configpath = os.path.join(os.path.dirname(__file__),
                                  constants.CONFIG_NODE)


        props = Ice.createProperties()
        props.setProperty("SubscriberAdapter.Endpoints", "tcp")
        props.setProperty("Ice.Default.Locator",
                          "IceGrid/Locator:tcp -h node1.local -p 4061")
        id = Ice.InitializationData()
        id.properties = props
        self.broker = Ice.initialize(id)

        proxy = self.broker.stringToProxy("ICRegistry" +
                                      "@ICRegistryServer" +
                                      ".ICRegistryAdapter")
        self.ic_registry = IceCloud.ICRegistryPrx.checkedCast(proxy)  # @UndefinedVariable

        self.ig_registry = IceGrid.RegistryPrx.checkedCast(\
           self.broker.stringToProxy(constants.IG_REGISTRY_PROXY))

        self.set_subscriber(self.broker)
        e = threading.Event()
        my_thread = threading.Thread(target=self.subscriber.run, args=[e])
        my_thread.setDaemon(True)
        my_thread.start()


        s.sendline ('echo "0"> ' +
         '/var/lib/ice/icegrid/node1/distrib/IceCloudApp/custom.info')
        subprocess.call('echo "1"> ' +
          '/home/laura/Clase/PFC/db/node0/distrib/IceCloudApp/custom.info',
          shell=True)

        self.set_ic_application_descriptor(self.get_test_application())
#        self.ic_registry.addApplication(self.ic_app_descriptor)
        tempf = self.get_test_application_file()
        subprocess.call('./../src/icecloudadmin.py --config=' + \
          '"IceGrid/Locator:tcp -h node1.local -p 4061" --add ' + tempf.name,
          shell=True)
        subprocess.call('./../src/icecloudadmin.py --config=' + \
          '"IceGrid/Locator:tcp -h node1.local -p 4061" --start Server0 ' ,
          shell=True)
        subprocess.call('./../src/icecloudadmin.py --config=' + \
          '"IceGrid/Locator:tcp -h node1.local -p 4061" --start ServerInstance0 ' ,
          shell=True)
        try:
#             self.ig_session = self.ig_registry.createAdminSession(
#                                     "laura", "")
#             self.ig_session.getAdmin().startServer("Server0")
            s = pxssh.pxssh()
            s.login ("node1.local", "root", "admin")



            ####### Server 0 and serverinstance1  should grow in grid-node0
            s.sendline ('echo "6"> ' +
                '/var/lib/ice/icegrid/node1/distrib/IceCloudApp/custom.info')
            e.wait()
            self.ig_session = self.ig_registry.createAdminSession(
                                    "laura", "")
            servers = self.ig_session.getAdmin().getAllServerIds()
            self.assertIn("Server0.0", servers)
            self.assertIn("ServerInstance1", servers)
            info1 = self.ig_session.getAdmin().getServerInfo("Server0.0")
            self.assertEqual(info1.node, "grid-node0")
            info2 = self.ig_session.getAdmin().getServerInfo("ServerInstance1")
            self.assertEqual(info2.node, "grid-node0")

            ########### Server 0  and serverinstance1 should decrease
            s.sendline ('echo "0"> ' +
             '/var/lib/ice/icegrid/node1/distrib/IceCloudApp/custom.info')
            e.wait()
            self.ig_session = self.ig_registry.createAdminSession(
                                "laura", "")
            with self.assertRaises(IceGrid.ServerNotExistException):
                self.ig_session.getAdmin().getServerState("Server0.0")

            with self.assertRaises(IceGrid.ServerNotExistException):
                self.ig_session.getAdmin().getServerState("ServerInstance1")

            ########## Server 0 and serverinstance1 should grow in node grid-node1
            subprocess.call('echo "7"> ' +
               '/home/laura/Clase/PFC/db/node0/distrib/IceCloudApp/custom.info',
                 shell=True)
            s.sendline ('echo "6"> ' +
                '/var/lib/ice/icegrid/node1/distrib/IceCloudApp/custom.info')
            e.wait()
            self.ig_session = self.ig_registry.createAdminSession("laura", "")
            servers = self.ig_session.getAdmin().getAllServerIds()
            self.assertIn("Server0.0", servers)
            self.assertIn("ServerInstance1", servers)
            info1 = self.ig_session.getAdmin().getServerInfo("Server0.0")
            self.assertEqual(info1.node, "grid-node1")
            info2 = self.ig_session.getAdmin().getServerInfo("ServerInstance1")
            self.assertEqual(info2.node, "grid-node1")

            subprocess.call('./../src/icecloudadmin.py --config=' + \
             '"IceGrid/Locator:tcp -h node1.local -p 4061" --stop "Server0.0"',
             shell=True)


#             self.ig_session = self.ig_registry.createAdminSession("laura", "")
#             state = self.ig_session.getAdmin().getServerState("Server0.0")
#             self.assertNotEqual(state, IceGrid.ServerState.Active)

            subprocess.call('./../src/icecloudadmin.py --config=' + \
             '"IceGrid/Locator:tcp -h node1.local -p 4061" --remove ' + \
             '"MinimalApplication"',
             shell=True)

            with self.assertRaises(IceGrid.ApplicationNotExistException):
                self.ig_session = self.ig_registry.createAdminSession("laura",
                                                                      "")
                self.ig_session.getAdmin().getApplicationInfo(
                                                        "MinimalApplication")


        except:
            self.ic_registry.removeApplication(self.ic_app_descriptor.name)
            raise


