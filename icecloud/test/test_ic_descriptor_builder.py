
import unittest
import sys
import Ice
import IceGrid
import os
from lxml import etree
import IceCloud
from ic_descriptor_builder import ICDescriptorBuilder



class DescriptorBuilderTests(unittest.TestCase):


    def setUp(self):
        self.root = self.getAppFile()
        self.nodeApp = self.root.find("application")
        self.descriptorBuilder = ICDescriptorBuilder()

        return 0



    def getAppFile(self):
        text = '''<icecloud>
   <application name="TestDescriptorApp">
      <description>Application for DescriptorBuilder testing</description>
      <variable name="variable1Name" value="variable1Value"/>
      <variable name="variable2Name" value="variable2Value"/>
      <distrib icepatch="${application}.IcePatch2/server">
         <directory>Directories</directory>
      </distrib>
      <service-template id="ServiceTemplate1">
         <parameter name="par1" default="par1value"/>
         <service name="Service1" entry="entrypoint">
            <description>Service1 Desc</description>
            <properties>
            </properties>
            <log path="log1path" property="log1prop"/>
         </service>
      </service-template>
      <service-template id="ServiceTemplate2">
         <service name="Service2" entry="entrypoint">
         </service>
      </service-template>
      <server-template id="IceBoxTemplate1">
         <parameter name="iceboxPar1Name" default="iceboxPar1Value"/>
         <icebox id="IceBoxServer" activation="manual"
          application-distrib="false" exe="path">
            <description>IceBoxServer description</description>
            <properties>
               <properties refid="propertySets"/>
               <property name="Property1" value="value"/>
            </properties>
            <log path="Log1" property="property1"/>
            <service name="Service1" entry="entrypoint">
               <description>service1 description</description>
               <properties>
                  <properties refid="ps"/>
               </properties>
               <log path="log1" property="p1name"/>
            </service>
         </icebox>
      </server-template>
      <server-template id="ServerTemplate1">
         <parameter name="st1par1Name" default="st1Default"/>
         <parameter name="st1par2Name"/>
         <server id="Server1st1" activation="manual" activation-timeout="2"
          deactivation-timeout="3" exe="exec" ice-version="iceVersion"
          pwd="WorkingDirectory">
            <description>Server 1 Server Template 1</description>
            <option>arg1</option>
            <option>arg2</option>
            <env>envVar1Name=envVar1Value</env>
            <env>envVar2Name=envVar2Value</env>
            <properties>
               <properties refid="PropertySets"/>
               <property name="aoproperty" value="ao2"/>
               <property name="prop1Name" value="prop1Value"/>
               <property name="prop2Name" value="prop2Value"/>
            </properties>
            <log path="path/log1" property="proplog1"/>
            <log path="path/log2" property="proplog2"/>
            <adapter name="AdapterST11" endpoints="Default"
             proxy-options="proxy options" id="${server}.AdapterST11"
             replica-group="ReplicaGroup1" priority="1" server-lifetime="false">
               <description>Adapter ServerTemplate1 1</description>
               <object identity="cat/wko1" type="wko"
                property="wko1property" proxy-options="po"/>
               <object identity="cat/wko2" type="wko"
                property="wko2property" proxy-options="po"/>
               <allocatable identity="cat/ao1" type="ao"
                property="ao1property" proxy-options="poa"/>
               <allocatable identity="cat/ao2" type="ao"
                property="ao2property" proxy-options="poa"/>
            </adapter>
            <dbenv name="DbEnv">
               <description>ServerTemplate1 DataBase Environment</description>
               <dbproperty name="property1Name" value="property1Value"/>
               <dbproperty name="property2Name" value="property2Value"/>
            </dbenv>
            <dbenv name="DbEnv2" home="home"/>
         </server>
      </server-template>
      <server-template id="ServerTemplate2">
         <parameter name="st2par1Name" default="st2Default"/>
         <parameter name="st2par2Name"/>
         <server id="Server1st2" activation="manual" activation-timeout="2"
           deactivation-timeout="3" exe="exec" ice-version="iceVersion"
           pwd="WorkingDirectory">
            <description>Server 1 Server Template 2</description>
            <option>arg1</option>
            <option>arg2</option>
            <env>envVar1Name=envVar1Value</env>
            <env>envVar2Name=envVar2Value</env>
            <properties>
               <properties refid="PropertySets"/>
               <property name="aoproperty" value="ao2"/>
               <property name="prop1Name" value="prop1Value"/>
               <property name="prop2Name" value="prop2Value"/>
            </properties>
            <log path="path/log1" property="proplog1"/>
            <log path="path/log2" property="proplog2"/>
            <adapter name="AdapterST21" endpoints="Default"
             proxy-options="proxy options" id="${server}.AdapterST21"
              register-process="true" replica-group="ReplicaGroup2"
              priority="1">
               <description>Adapter ServerTemplate2 1</description>
               <object identity="cat/wko1" type="wko" property="wko1property"
                proxy-options="po"/>
               <object identity="cat/wko2" type="wko" property="wko2property"
                proxy-options="po"/>
               <allocatable identity="cat/ao1" type="ao" property="ao1property"
                proxy-options="poa"/>
               <allocatable identity="cat/ao2" type="ao" property="ao2property"
                proxy-options="poa"/>
            </adapter>
         </server>
      </server-template>
      <category-server-template id="CategoryServerTemplate1"
      allocationRule = "min_servers" growingRule = "num_Servers gt 4"
      decreasingRule = "num_Servers gt 4" maxInstances = "5"
      category = "cat">
         <parameter name="st1par1Name" default="st1Default"/>
         <parameter name="st1par2Name"/>
         <server id="Server1st1" activation="manual" activation-timeout="2"
          deactivation-timeout="3" exe="exec" ice-version="iceVersion"
          pwd="WorkingDirectory">
            <description>Server 1 Server Template 1</description>
            <option>arg1</option>
            <option>arg2</option>
            <env>envVar1Name=envVar1Value</env>
            <env>envVar2Name=envVar2Value</env>
            <properties>
               <properties refid="PropertySets"/>
               <property name="aoproperty" value="ao2"/>
               <property name="prop1Name" value="prop1Value"/>
               <property name="prop2Name" value="prop2Value"/>
            </properties>
            <log path="path/log1" property="proplog1"/>
            <log path="path/log2" property="proplog2"/>
            <adapter name="AdapterST11" endpoints="Default"
             proxy-options="proxy options" id="${server}.AdapterST11"
             replica-group="ReplicaGroup1" priority="1" server-lifetime="false">
               <description>Adapter ServerTemplate1 1</description>
               <object identity="cat/wko1" type="wko"
                property="wko1property" proxy-options="po"/>
               <object identity="cat/wko2" type="wko"
                property="wko2property" proxy-options="po"/>
               <allocatable identity="cat/ao1" type="ao"
                property="ao1property" proxy-options="poa"/>
               <allocatable identity="cat/ao2" type="ao"
                property="ao2property" proxy-options="poa"/>
            </adapter>
            <dbenv name="DbEnv">
               <description>ServerTemplate1 DataBase Environment</description>
               <dbproperty name="property1Name" value="property1Value"/>
               <dbproperty name="property2Name" value="property2Value"/>
            </dbenv>
            <dbenv name="DbEnv2" home="home"/>
         </server>
      </category-server-template>
      <replica-group id="ReplicaGroup1" proxy-options="RP1 proxy options">
         <description>RP1</description>
         <load-balancing type="random" n-replicas="0"/>
         <object identity="cat/RP1-wko1" type="wko" proxy-options="wko1proxy"/>
         <object identity="cat/RP1-wko2" type="wko" proxy-options="wko2proxy"/>
      </replica-group>
      <replica-group id="ReplicaGroup2" proxy-options="RP2 proxy options">
         <description>RP2</description>
         <load-balancing type="adaptive" load-sample="1" n-replicas="0"/>
         <object identity="cat/RP2-wko1" type="wko" proxy-options="wko1proxy"/>
         <object identity="cat/RP2-wko2" type="wko" proxy-options="wko2proxy"/>
      </replica-group>
      <properties id="PropertySet1">
         <properties refid="PropertySet1ref"/>
         <property name="prop1-1Name" value="prop1-1Value"/>
         <property name="prop1-2Name" value="prop1-2Value"/>
      </properties>
      <properties id="PropertySet2">
         <properties refid="PropertySet2ref"/>
         <property name="prop2-1Name" value="prop2-1Value"/>
         <property name="prop2-2Name" value="prop2-2Value"/>
      </properties>
      <node name="Node1" load-factor="1">
         <description>Node1Desc</description>
         <variable name="var1name" value="var1value"/>
         <properties id="PropertySet">
            <properties refid="propertyset1"/>
            <property name="property1" value="property2"/>
         </properties>
         <icebox id="IceBoxServer" activation="manual" exe="exe">
            <service-instance template="ServiceTemplate1" par1="par1value"/>
         </icebox>
         <server id="server1node1" activation="manual" exe="exe">
            <properties>
            </properties>
            <adapter name="adapter1server1node1" endpoints="default"
             id="${server}.adapter1server1node1" server-lifetime="false"/>
         </server>
      </node>
      <node name="Node2">
         <server-instance template="IceBoxTemplate1" iceboxPar1Name="ParValue">
            <properties service="Service">
               <properties refid="propsetService"/>
               <property name="prop1name" value="prop1value"/>
            </properties>
         </server-instance>
         <server-instance template="ServerTemplate1" st1par1Name="hola"/>
      </node>
      <node category="catNode">
        <server id="serverCat" activation="manual" exe="exe" maxDupes="3">
         </server>
      </node>
    <server-instance template="CategoryServerTemplate1" st1par1Name="hola"
        servercount="0"/>
   </application>
</icecloud>'''

        parser = etree.XMLParser(remove_blank_text=True)
        root = etree.fromstring(text, parser)
        return root

    def getCategoryInstancesSeq(self):

        categoryInstancesSeq = []
        serverInstance1 = IceGrid.ServerInstanceDescriptor()

        serverInstance1.template = "CategoryServerTemplate1"

        serverInstance1.parameterValues = {}
        serverInstance1.parameterValues["st1par1Name"] = "hola"
        serverInstance1.parameterValues['servercount'] = "0"

        serverInstance1.descriptor = IceGrid.ServerDescriptor()
        serverInstance1.descriptor.adapters = []
        serverInstance1.descriptor.dbEnvs = []
        serverInstance1.descriptor.logs = []
        serverInstance1.descriptor.propertySet = IceGrid.PropertySetDescriptor()
        serverInstance1.descriptor.propertySet.references = []
        serverInstance1.descriptor.propertySet.properties = []

        serverInstance1.propertySet.references = []
        serverInstance1.propertySet.properties = []

        serverInstance1.servicePropertySets = {}

        categoryInstancesSeq.append(serverInstance1)
        return categoryInstancesSeq

    def getServerTemplatesDict(self):
        templatesDict = {}
        iceboxServerTemplate = IceGrid.TemplateDescriptor()
        iceboxServerTemplate.parameters = ["iceboxPar1Name"]
        iceboxServerTemplate.parameterDefaults = \
            {"iceboxPar1Name":"iceboxPar1Value"}
        iceboxServerTemplate.descriptor = IceGrid.IceBoxDescriptor()
        iceboxServerTemplate.descriptor.envs = []
        iceboxServerTemplate.descriptor.logs = []
        iceboxServerTemplate.descriptor.adapters = []
        iceboxServerTemplate.descriptor.description = "IceBoxServer description"
        iceboxServerTemplate.descriptor.id = "IceBoxServer"
        iceboxServerTemplate.descriptor.serverLifetime = True
        iceboxServerTemplate.descriptor.exe = "path"
        iceboxServerTemplate.descriptor.activation = "manual"
        iceboxServerTemplate.descriptor.applicationDistrib = False
        iceboxServerTemplate.descriptor.propertySet = \
                                            IceGrid.PropertySetDescriptor()
        iceboxServerTemplate.descriptor.propertySet.references = \
                                                             ["propertySets"]
        iceboxServerTemplate.descriptor.propertySet.properties = []
        prop = IceGrid.PropertyDescriptor()
        prop.name = "Property1"
        prop.value = "value"
        iceboxServerTemplate.descriptor.propertySet.properties.append(prop)
        property1 = IceGrid.PropertyDescriptor()
        property1.name = "property1"
        property1.value = "Log1"
        iceboxServerTemplate.descriptor.propertySet.properties.append(property1)
        iceboxServerTemplate.descriptor.logs = ["Log1"]

        iceboxServerTemplate.services = []
        service1 = IceGrid.ServiceInstanceDescriptor()

        service1.descriptor = IceGrid.ServiceDescriptor()
        service1.descriptor.name = "Service1"
        service1.descriptor.entry = "entrypoint"
        # service1.descriptor.adapters = self.getAdapterSeq(server)
        # service1.descriptor.dbEnvs = self.getDbEnvSeq(server)
        # service1.descriptor.logs = self.getLogs(server)
        service1.descriptor.description = "service1 description"
        service1.descriptor.logs = []
        service1.propertySet = IceGrid.PropertySetDescriptor()
        service1.propertySet.references = ["ps"]
        iceboxServerTemplate.descriptor.dbEnvs = []

        templatesDict["IceBoxTemplate1"] = iceboxServerTemplate

        serverTemplate1 = IceGrid.TemplateDescriptor()
        serverTemplate1.parameters = ["st1par1Name", "st1par2Name"]
        serverTemplate1.parameterDefaults = {"st1par1Name":"st1Default"}
        serverTemplate1.descriptor = IceGrid.ServerDescriptor()
        adapter1 = IceGrid.AdapterDescriptor()
        adapter1.name = "AdapterST11"
        adapter1.description = "Adapter ServerTemplate1 1"
        adapter1.id = "${server}.AdapterST11"
        adapter1.replicaGroupId = "ReplicaGroup1"
        adapter1.priority = "1"
        adapter1.registerProcess = False
        adapter1.serverLifeTime = True
        object1 = IceGrid.ObjectDescriptor()
        object1.id.category = "cat"
        object1.id.name = "wko1"
        object1.type = "wko"
        object1.proxyOptions = "po"
        object2 = IceGrid.ObjectDescriptor()
        object2.id.category = "cat"
        object2.id.name = "wko2"
        object2.type = "wko"
        object2.proxyOptions = "po"
        adapter1.objects = [object1, object2]
        allocatable1 = IceGrid.ObjectDescriptor()
        allocatable1.id.category = "cat"
        allocatable1.id.name = "ao1"
        allocatable1.type = "ao"
        allocatable1.proxyOptions = "poa"
        allocatable2 = IceGrid.ObjectDescriptor()
        allocatable2.id.category = "cat"
        allocatable2.id.name = "ao2"
        allocatable2.type = "ao"
        allocatable2.proxyOptions = "poa"
        adapter1.allocatables = [allocatable1, allocatable2]
        serverTemplate1.descriptor.adapters = [adapter1]
        serverTemplate1.descriptor.propertySet = \
            IceGrid.PropertySetDescriptor()
        serverTemplate1.descriptor.propertySet.references = ["PropertySets"]
        p1 = IceGrid.PropertyDescriptor()
        p1.name = "aoproperty"
        p1.value = "ao2"
        p2 = IceGrid.PropertyDescriptor()
        p2.name = "prop1Name"
        p2.value = "prop1Value"
        p3 = IceGrid.PropertyDescriptor()
        p3.name = "prop2Name"
        p3.value = "prop2Value"
        p4 = IceGrid.PropertyDescriptor()
        p4.name = "AdapterST11.Endpoints"
        p4.value = "Default"
        p5 = IceGrid.PropertyDescriptor()
        p5.name = "proplog1"
        p5.value = "path/log1"
        p6 = IceGrid.PropertyDescriptor()
        p6.name = "proplog2"
        p6.value = "path/log2"
        propertiesSeq = [p1, p2, p3, p4, p5, p6]
        serverTemplate1.descriptor.propertySet.properties = propertiesSeq
        dbEnv = IceGrid.DbEnvDescriptor()
        dbEnv.name = "DbEnv"
        dbEnv.description = "ServerTemplate1 DataBase Environment"
        property1 = IceGrid.PropertyDescriptor()
        property1.name = "property1Name"
        property1.value = "property1Value"
        property2 = IceGrid.PropertyDescriptor()
        property2.name = "property2Name"
        property2.value = "property2Value"

        dbEnv.properties = [property1, property2]
        serverTemplate1.descriptor.dbEnvs = []
        serverTemplate1.descriptor.dbEnvs.append(dbEnv)
        dbEnv2 = IceGrid.DbEnvDescriptor()
        dbEnv2.name = "DbEnv2"
        dbEnv2.dbHome = "home"
        dbEnv2.properties = []
        serverTemplate1.descriptor.dbEnvs.append(dbEnv2)
        serverTemplate1.descriptor.description = "Server 1 Server Template 1"
        serverTemplate1.descriptor.id = "Server1st1"
        serverTemplate1.descriptor.applicationDistrib = True
        serverTemplate1.descriptor.exe = "exec"
        serverTemplate1.descriptor.iceVersion = "iceVersion"
        serverTemplate1.descriptor.pwd = "WorkingDirectory"
        serverTemplate1.descriptor.options = ["arg1", "arg2"]
        serverTemplate1.descriptor.envs = ["envVar1Name=envVar1Value", \
                                           "envVar2Name=envVar2Value"]
        serverTemplate1.descriptor.logs = ["path/log1", "path/log2"]

        serverTemplate1.descriptor.activation = "manual"
        serverTemplate1.descriptor.activationTimeout = "2"
        serverTemplate1.descriptor.deactivationTimeout = "3"
        templatesDict["ServerTemplate1"] = serverTemplate1

        serverTemplate2 = IceGrid.TemplateDescriptor()
        serverTemplate2.parameters = ["st2par1Name", "st2par2Name"]
        serverTemplate2.parameterDefaults = {"st2par1Name":"st2Default"}
        serverTemplate2.descriptor = IceGrid.ServerDescriptor()
        adapter1 = IceGrid.AdapterDescriptor()
        adapter1.name = "AdapterST21"
        adapter1.description = "Adapter ServerTemplate2 1"
        adapter1.id = "${server}.AdapterST21"
        adapter1.replicaGroupId = "ReplicaGroup2"
        adapter1.priority = "1"
        adapter1.registerProcess = True
        adapter1.serverLifetime = True
        object1 = IceGrid.ObjectDescriptor()
        object1.id.category = "cat"
        object1.id.name = "wko1"
        object1.type = "wko"
        object1.proxyOptions = "po"
        object2 = IceGrid.ObjectDescriptor()
        object2.id.category = "cat"
        object2.id.name = "wko2"
        object2.type = "wko"
        object2.proxyOptions = "po"
        adapter1.objects = [object1, object2]
        allocatable1 = IceGrid.ObjectDescriptor()
        allocatable1.id.category = "cat"
        allocatable1.id.name = "ao1"
        allocatable1.type = "ao"
        allocatable1.proxyOptions = "poa"
        allocatable2 = IceGrid.ObjectDescriptor()
        allocatable2.id.category = "cat"
        allocatable2.id.name = "ao2"
        allocatable2.type = "ao"
        allocatable2.proxyOptions = "poa"
        adapter1.allocatables = [allocatable1, allocatable2]
        serverTemplate2.descriptor.adapters = [adapter1]
        serverTemplate2.descriptor.propertySet = \
            IceGrid.PropertySetDescriptor()
        serverTemplate2.descriptor.propertySet.references = ["PropertySets"]
        p1 = IceGrid.PropertyDescriptor()
        p1.name = "aoproperty"
        p1.value = "ao2"
        p2 = IceGrid.PropertyDescriptor()
        p2.name = "prop1Name"
        p2.value = "prop1Value"
        p3 = IceGrid.PropertyDescriptor()
        p3.name = "prop2Name"
        p3.value = "prop2Value"
        p4 = IceGrid.PropertyDescriptor()
        p4.name = "AdapterST21.Endpoints"
        p4.value = "Default"
        p5 = IceGrid.PropertyDescriptor()
        p5.name = "proplog1"
        p5.value = "path/log1"
        p6 = IceGrid.PropertyDescriptor()
        p6.name = "proplog2"
        p6.value = "path/log2"
        propertiesSeq = [p1, p2, p3, p4, p5, p6]
        serverTemplate2.descriptor.propertySet.properties = propertiesSeq

        serverTemplate2.descriptor.dbEnvs = []
        serverTemplate2.descriptor.description = "Server 1 Server Template 2"
        serverTemplate2.descriptor.id = "Server1st2"
        serverTemplate2.descriptor.applicationDistrib = True
        serverTemplate2.descriptor.exe = "exec"
        serverTemplate2.descriptor.iceVersion = "iceVersion"
        serverTemplate2.descriptor.pwd = "WorkingDirectory"
        serverTemplate2.descriptor.options = ["arg1", "arg2"]
        serverTemplate2.descriptor.envs = ["envVar1Name=envVar1Value", \
                                           "envVar2Name=envVar2Value"]
        serverTemplate2.descriptor.logs = ["path/log1", "path/log2"]
        serverTemplate2.descriptor.activation = "manual"
        serverTemplate2.descriptor.activationTimeout = "2"
        serverTemplate2.descriptor.deactivationTimeout = "3"
        templatesDict["ServerTemplate2"] = serverTemplate2


        return templatesDict

    def getCategoryServerTemplatesDict(self):


        templatesDict = {}


        serverTemplate1 = IceGrid.TemplateDescriptor()
        serverTemplate1.category = "cat"
        serverTemplate1.allocationRule = "min_servers"
        serverTemplate1.growingRule = "num_Servers gt 4"
        serverTemplate1.decreasingRule = "num_Servers gt 4"
        serverTemplate1.maxInstances = 5
        serverTemplate1.parameters = ["st1par1Name", "st1par2Name", "servercount"]
        serverTemplate1.parameterDefaults = {"st1par1Name":"st1Default"}
        serverTemplate1.descriptor = IceGrid.ServerDescriptor()
        adapter1 = IceGrid.AdapterDescriptor()
        adapter1.name = "AdapterST11"
        adapter1.description = "Adapter ServerTemplate1 1"
        adapter1.id = "${server}.AdapterST11"
        adapter1.replicaGroupId = "ReplicaGroup1"
        adapter1.priority = "1"
        adapter1.registerProcess = False
        adapter1.serverLifeTime = True
        object1 = IceGrid.ObjectDescriptor()
        object1.id.category = "cat"
        object1.id.name = "wko1"
        object1.type = "wko"
        object1.proxyOptions = "po"
        object2 = IceGrid.ObjectDescriptor()
        object2.id.category = "cat"
        object2.id.name = "wko2"
        object2.type = "wko"
        object2.proxyOptions = "po"
        adapter1.objects = [object1, object2]
        allocatable1 = IceGrid.ObjectDescriptor()
        allocatable1.id.category = "cat"
        allocatable1.id.name = "ao1"
        allocatable1.type = "ao"
        allocatable1.proxyOptions = "poa"
        allocatable2 = IceGrid.ObjectDescriptor()
        allocatable2.id.category = "cat"
        allocatable2.id.name = "ao2"
        allocatable2.type = "ao"
        allocatable2.proxyOptions = "poa"
        adapter1.allocatables = [allocatable1, allocatable2]
        serverTemplate1.descriptor.adapters = [adapter1]
        serverTemplate1.descriptor.propertySet = \
            IceGrid.PropertySetDescriptor()
        serverTemplate1.descriptor.propertySet.references = ["PropertySets"]
        p1 = IceGrid.PropertyDescriptor()
        p1.name = "aoproperty"
        p1.value = "ao2"
        p2 = IceGrid.PropertyDescriptor()
        p2.name = "prop1Name"
        p2.value = "prop1Value"
        p3 = IceGrid.PropertyDescriptor()
        p3.name = "prop2Name"
        p3.value = "prop2Value"
        p4 = IceGrid.PropertyDescriptor()
        p4.name = "AdapterST11.Endpoints"
        p4.value = "Default"
        p5 = IceGrid.PropertyDescriptor()
        p5.name = "proplog1"
        p5.value = "path/log1"
        p6 = IceGrid.PropertyDescriptor()
        p6.name = "proplog2"
        p6.value = "path/log2"
        propertiesSeq = [p1, p2, p3, p4, p5, p6]
        serverTemplate1.descriptor.propertySet.properties = propertiesSeq
        dbEnv = IceGrid.DbEnvDescriptor()
        dbEnv.name = "DbEnv"
        dbEnv.description = "ServerTemplate1 DataBase Environment"
        property1 = IceGrid.PropertyDescriptor()
        property1.name = "property1Name"
        property1.value = "property1Value"
        property2 = IceGrid.PropertyDescriptor()
        property2.name = "property2Name"
        property2.value = "property2Value"
        dbEnv.properties = [property1, property2]
        serverTemplate1.descriptor.dbEnvs = []
        serverTemplate1.descriptor.dbEnvs.append(dbEnv)
        dbEnv2 = IceGrid.DbEnvDescriptor()
        dbEnv2.name = "DbEnv2"
        dbEnv2.dbHome = "home"
        dbEnv2.properties = []
        serverTemplate1.descriptor.dbEnvs.append(dbEnv2)
        serverTemplate1.descriptor.logs = ['path/log1', 'path/log2']
        serverTemplate1.descriptor.description = "Server 1 Server Template 1"
        serverTemplate1.descriptor.id = "Server1st1${servercount}"
        serverTemplate1.descriptor.applicationDistrib = True
        serverTemplate1.descriptor.exe = "exec"
        serverTemplate1.descriptor.iceVersion = "iceVersion"
        serverTemplate1.descriptor.pwd = "WorkingDirectory"
        serverTemplate1.descriptor.options = ["arg1", "arg2"]
        serverTemplate1.descriptor.envs = ["envVar1Name=envVar1Value", \
                                           "envVar2Name=envVar2Value"]
        serverTemplate1.descriptor.activation = "manual"
        serverTemplate1.descriptor.activationTimeout = "2"
        serverTemplate1.descriptor.deactivationTimeout = "3"
        templatesDict["CategoryServerTemplate1"] = serverTemplate1



        return templatesDict

    def getVariables(self):
        return dict({'variable1Name':'variable1Value', \
                 'variable2Name':'variable2Value'})

    def getReplicaGroups(self):
        rp1 = self.getReplicaGroup(1)
        rp2 = self.getReplicaGroup(2)
        return [rp1, rp2]

    def getReplicaGroup(self, num):
        rp = IceGrid.ReplicaGroupDescriptor()
        if num == 1:
            rp.id = "ReplicaGroup1"
            rp.loadBalancing = IceGrid.RandomLoadBalancingPolicy()
            rp.loadBalancing.nReplicas = "0"
            rp.proxyOptions = "RP1 proxy options"
            object1 = IceGrid.ObjectDescriptor()
            object1.id = Ice.Identity()
            object1.id.category = "cat"
            object1.id.name = "RP1-wko1"
            object1.type = "wko"
            object1.proxyOptions = "wko1proxy"
            object2 = IceGrid.ObjectDescriptor()
            object2.id = Ice.Identity()
            object2.id.category = "cat"
            object2.id.name = "RP1-wko2"
            object2.type = "wko"
            object2.proxyOptions = "wko2proxy"
            rp.objects = [object1, object2]
            rp.description = 'RP1'
        elif num == 2:
            rp.id = "ReplicaGroup2"
            loadBalancing = IceGrid.AdaptiveLoadBalancingPolicy()
            loadBalancing.nReplicas = '0'
            loadBalancing.loadSample = '1'
            rp.loadBalancing = loadBalancing
            rp.proxyOptions = "RP2 proxy options"
            object1 = IceGrid.ObjectDescriptor()
            object1.id = Ice.Identity()
            object1.id.category = "cat"
            object1.id.name = "RP2-wko1"
            object1.type = "wko"
            object1.proxyOptions = "wko1proxy"
            object2 = IceGrid.ObjectDescriptor()
            object2.id = Ice.Identity()
            object2.id.category = "cat"
            object2.id.name = "RP2-wko2"
            object2.type = "wko"
            object2.proxyOptions = "wko2proxy"
            rp.objects = [object1, object2]
            rp.description = 'RP2'
        return rp

    def getServiceTemplatesDict(self):
        serviceTemplatesDict = {}
        serviceTemplate1 = IceGrid.TemplateDescriptor()
        serviceTemplate1.parameters = ["par1"]
        serviceTemplate1.parameterDefaults = {"par1":"par1value"}
        serviceTemplate1.descriptor = IceGrid.ServiceDescriptor()
        serviceTemplate1.descriptor.adapters = []
        serviceTemplate1.descriptor.dbEnvs = []
        serviceTemplate1.descriptor.logs = ['log1path']
        serviceTemplate1.descriptor.name = "Service1"
        serviceTemplate1.descriptor.entry = "entrypoint"
        serviceTemplate1.descriptor.propertySet = \
            IceGrid.PropertySetDescriptor()
        serviceTemplate1.descriptor.propertySet.references = []
        serviceTemplate1.descriptor.propertySet.properties = []
        property = IceGrid.PropertyDescriptor()
        property.name = 'log1prop'
        property.value = 'log1path'
        serviceTemplate1.descriptor.propertySet.properties.append(property)
        serviceTemplate1.descriptor.description = "Service1 Desc"
        serviceTemplatesDict["ServiceTemplate1"] = serviceTemplate1

        serviceTemplate2 = IceGrid.TemplateDescriptor()
        serviceTemplate2.parameters = []
        serviceTemplate2.parameterDefaults = {}
        serviceTemplate2.descriptor = IceGrid.ServiceDescriptor()
        serviceTemplate2.descriptor.adapters = []
        serviceTemplate2.descriptor.dbEnvs = []
        serviceTemplate2.descriptor.name = "Service2"
        serviceTemplate2.descriptor.entry = "entrypoint"
        serviceTemplate2.descriptor.logs = []
        serviceTemplate2.descriptor.propertySet = \
            IceGrid.PropertySetDescriptor()
        serviceTemplate2.descriptor.propertySet.references = []
        serviceTemplate2.descriptor.propertySet.properties = []
        serviceTemplatesDict["ServiceTemplate2"] = serviceTemplate2

        return serviceTemplatesDict

    def getNodes(self):
        nodes = {}

        node1 = IceGrid.NodeDescriptor()
        node1.name = "Node1"
        node1.description = "Node1Desc"
        node1.loadFactor = "1"
        node1.variables = {"var1name":"var1value"}
        node1.serverInstances = []
        node1.propertySets = {}
        propertySet1 = IceGrid.PropertySetDescriptor()
        propertySet1.references = ["propertyset1"]
        propertySet1.properties = []
        property1 = IceGrid.PropertyDescriptor()
        property1.name = "property1"
        property1.value = "property2"
        propertySet1.properties.append(property1)
        node1.propertySets["PropertySet"] = propertySet1
        server1 = IceGrid.ServerDescriptor()
        server1.dbEnvs = []
        server1.id = "server1node1"
        server1.serverLifetime = False
        server1.activation = "manual"
        server1.applicationDistrib = True
        server1.exe = "exe"
        server1.propertySet = IceGrid.PropertySetDescriptor()
        server1.propertySet.references = []
        server1.propertySet.properties = []
        property1server1 = IceGrid.PropertyDescriptor()
        property1server1.name = "adapter1server1node1.Endpoints"
        property1server1.value = "default"
        server1.propertySet.properties.append(property1server1)
        server1.envs = []
        adapter1 = IceGrid.AdapterDescriptor()
        adapter1.name = "adapter1server1node1"
        adapter1.id = "${server}.adapter1server1node1"
        adapter1.serverLifeTime = True
        adapter1.objects = []
        adapter1.allocatables = []
        server1.adapters = []
        server1.adapters.append(adapter1)
        node1.servers = []
        node1.servers.append(server1)
        iceboxserver = IceGrid.IceBoxDescriptor()
        iceboxserver.dbEnvs = []
        iceboxserver.options = []
        iceboxserver.id = "IceBoxServer"
        iceboxserver.activation = "manual"
        iceboxserver.applicationDistrib = True
        iceboxserver.exe = "exe"
        serviceInstance1 = IceGrid.ServiceInstanceDescriptor()
        serviceInstance1.template = "ServiceTemplate1"
        serviceInstance1.parameterValues = {}
        serviceInstance1.parameterValues["par1"] = "par1value"
        serviceInstance1.descriptor = IceGrid.ServiceDescriptor()
        serviceInstance1.descriptor.adapters = []
        serviceInstance1.descriptor.dbEnvs = []
        serviceInstance1.descriptor.logs = []
        serviceInstance1.descriptor.propertySet.references = []
        serviceInstance1.descriptor.propertySet.properties = []
        serviceInstance1.propertySet.references = []
        serviceInstance1.propertySet.properties = []
        iceboxserver.adapters = []
        iceboxserver.services = []
        iceboxserver.propertySet = IceGrid.PropertySetDescriptor()
        iceboxserver.propertySet.references = []
        iceboxserver.propertySet.properties = []
        iceboxserver.envs = []
        iceboxserver.services.append(serviceInstance1)
        node1.servers.append(iceboxserver)
        nodes["Node1"] = node1


        node2 = IceGrid.NodeDescriptor()
        node2.name = "Node2"
        node2.variables = {}
        node2.propertySets = {}
        propertySet2 = IceGrid.PropertySetDescriptor()
        propertySet2.references = []
        propertySet2.properties = []
        serverInstance1 = IceGrid.ServerInstanceDescriptor()
        serverInstance1.template = "IceBoxTemplate1"
        serverInstance1.parameterValues = {"iceboxPar1Name":"ParValue"}

        serverInstance1.propertySet = IceGrid.PropertySetDescriptor()
        serverInstance1.propertySet.references = []
        serverInstance1.propertySet.properties = []
        serverInstance1.servicePropertySets = {}
        propertySet3 = IceGrid.PropertySetDescriptor()
        propertySet3.references = ["propsetService"]
        propertySet3.properties = []
        prop1 = IceGrid.PropertyDescriptor()
        prop1.name = "prop1name"
        prop1.value = "prop1value"
        propertySet3.properties.append(prop1)
        serverInstance1.servicePropertySets["Service"] = propertySet3
        node2.serverInstances = []
        node2.serverInstances.append(serverInstance1)
        serverInstance2 = IceGrid.ServerInstanceDescriptor()
        serverInstance2.template = "ServerTemplate1"
        serverInstance2.parameterValues = {}
        serverInstance2.parameterValues["st1par1Name"] = "hola"
        serverInstance2.propertySet = IceGrid.PropertySetDescriptor()
        serverInstance2.propertySet.references = []
        serverInstance2.propertySet.properties = []
        serverInstance2.servicePropertySets = {}
        node2.serverInstances.append(serverInstance2)
        node2.servers = []
        nodes["Node2"] = node2


        return nodes

    def getCategoryNodes(self):
        nodes = {}

        node1 = IceCloud.NodeDescriptor()
        node1.name = "catNode"
        node1.serverInstances = []
        server1 = IceCloud.ServerDescriptor()
        server1.dbEnvs = []
        server1.id = "serverCat"
        server1.serverLifetime = False
        server1.activation = "manual"
        server1.applicationDistrib = True
        server1.exe = "exe"
        server1.maxDupes = 3
        server1.propertySet = IceGrid.PropertySetDescriptor()
        server1.propertySet.references = []
        server1.propertySet.properties = []
        server1.envs = []
        server1.adapters = []
        node1.servers = []
        node1.servers.append(server1)
        nodes["catNode"] = node1

        return nodes



    def getApplicationPropertySets(self):

        propertySet1 = IceGrid.PropertySetDescriptor()
        propertySet1.references = ["PropertySet1ref"]
        property11 = IceGrid.PropertyDescriptor()
        property11.name = "prop1-1Name"
        property11.value = "prop1-1Value"
        property12 = IceGrid.PropertyDescriptor()
        property12.name = "prop1-2Name"
        property12.value = "prop1-2Value"
        propertySet1.properties = [property11, property12]

        propertySet2 = IceGrid.PropertySetDescriptor()
        propertySet2.references = ["PropertySet2ref"]
        property21 = IceGrid.PropertyDescriptor()
        property21.name = "prop2-1Name"
        property21.value = "prop2-1Value"
        property22 = IceGrid.PropertyDescriptor()
        property22.name = "prop2-2Name"
        property22.value = "prop2-2Value"
        propertySet2.properties = [property21, property22]

        return {"PropertySet1":propertySet1, "PropertySet2":propertySet2}




    def getDistribution(self):
        distributionDescriptor = IceGrid.DistributionDescriptor()
        distributionDescriptor.icepatch = "${application}.IcePatch2/server"
        distributionDescriptor.directories = ["Directories"]
        return distributionDescriptor



    def test_getVariables(self):
        varResult = self.descriptorBuilder.getVariables(self.nodeApp)
        self.assertDictEqual(varResult, self.getVariables())

    def test_getReplicaGroups(self):
        replicaGroupSeq1 = self.descriptorBuilder.getReplicaGroups(self.nodeApp)
        replicaGroupSeq2 = self.getReplicaGroups()
        self.assertEqual(replicaGroupSeq1 is not None, \
                         replicaGroupSeq2 is not None)
        self.assertEqual(len(replicaGroupSeq1), len(replicaGroupSeq2))

        for i, replica1 in enumerate(replicaGroupSeq1):
            replica2 = replicaGroupSeq2[i]
            self.assertEqual(type(replica2), type(replica1))
            self.assertEqual(replica1.id, replica2.id)
            self.assertEqual(replica1.proxyOptions, replica2.proxyOptions)
            self.assertEqual(type(replica1.loadBalancing), \
                             type(replica2.loadBalancing))
            self.assertEqual(replica1.loadBalancing.nReplicas, \
                             replica2.loadBalancing.nReplicas)
            if isinstance(replica1.loadBalancing, \
                          IceGrid.AdaptiveLoadBalancingPolicy):
                self.assertEqual(replica1.loadBalancing.loadSample, \
                                 replica2.loadBalancing.loadSample)
            self.assertEqual(replica1.description, replica2.description)
            self.assertEqual(len(replica1.objects), len(replica2.objects))
            for object in replica1.objects:
                self.assertIn(object, replica2.objects)


    def test_getPropertySetDict(self):
        propertySetDictResult = \
         self.descriptorBuilder.getPropertySetDict(self.nodeApp)
        self.assertTrue(propertySetDictResult == \
                        self.getApplicationPropertySets())

    def test_getDistributionDescriptor(self):
        distributionResult = \
            self.descriptorBuilder.getDistributionDescriptor(self.nodeApp)
        myDistribution = self.getDistribution()
        self.assertEqual(distributionResult.icepatch, myDistribution.icepatch)
        self.assertListEqual(distributionResult.directories, \
                             myDistribution.directories)


    def test_getServerTemplates_dict(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "server-template")
        t2 = self.getServerTemplatesDict()
        self.assertEqual(len(t), len(t2))
        self.assertEqual((t is None), (t2 is None))
        for template in iter(t):
            self.assertIsNotNone(t2[template])

    def test_getServerTemplates_templates_parameters(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "server-template")
        t2 = self.getServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertDictEqual(template1.parameterDefaults, \
                                  template2.parameterDefaults)

    def test_getServerTemplates_templates_parameterDefaults(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "server-template")
        t2 = self.getServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertEqual(template1.parameters is None, \
                             template2.parameters is None)
            if template1.parameters != None:
                self.assertEqual(len(template1.parameters), \
                                  len(template2.parameters))
            for par in template1.parameters:
                self.assertIn(par, template2.parameters)

    def test_getServerTemplates_templates_descriptor(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "server-template")
        t2 = self.getServerTemplatesDict()
        self.maxDiff = None
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            descriptor1 = template1.descriptor
            descriptor2 = template2.descriptor
            self.assertEqual(type(descriptor1), type(descriptor2))
            self.assertEqual(descriptor1.description, descriptor2.description)
            self.assertListEqual(descriptor1.adapters, descriptor2.adapters)
            self.assertEqual(descriptor1.propertySet, descriptor2.propertySet)
            self.assertEqual(descriptor1.dbEnvs, descriptor2.dbEnvs)
            self.assertEqual(descriptor1.logs, descriptor2.logs, descriptor1.id)
            self.assertEqual(descriptor1.id, descriptor2.id)
            self.assertEqual(descriptor1.exe, descriptor2.exe)
            self.assertEqual(descriptor1.pwd, descriptor2.pwd)
            self.assertEqual(descriptor1.activation, descriptor2.activation)
            self.assertEqual(descriptor1.activationTimeout, \
                              descriptor2.activationTimeout)
            self.assertEqual(descriptor1.applicationDistrib, \
                             descriptor2.applicationDistrib, descriptor1.id)
            self.assertEqual(descriptor1.allocatable, descriptor2.allocatable)
            self.assertEqual(descriptor1.user, descriptor2.user)
            self.assertEqual(descriptor1.envs is None, descriptor2.envs is None)
            if descriptor1.envs is not None:
                self.assertEquals(len(descriptor1.envs), len(descriptor2.envs))
            for env in descriptor1.envs:
                self.assertIn(env, descriptor2.envs)


    def test_getServiceTemplates_dict(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "service-template")
        t2 = self.getServiceTemplatesDict()
        self.assertEqual(len(t), len(t2))
        self.assertEqual((t is None), (t2 is None))
        for template in iter(t):
            self.assertIsNotNone(t2[template])

    def test_getServiceTemplates_templates_parameters(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "service-template")
        t2 = self.getServiceTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertDictEqual(template1.parameterDefaults, \
                                  template2.parameterDefaults)

    def test_getServiceTemplates_templates_parameterDefaults(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "service-template")
        t2 = self.getServiceTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertEqual(template1.parameters is None, \
                             template2.parameters is None)
            if template1.parameters != None:
                self.assertEqual(len(template1.parameters), \
                                  len(template2.parameters))
            for par in template1.parameters:
                self.assertIn(par, template2.parameters)

    def test_getServiceTemplates_templates_descriptor(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "service-template")
        t2 = self.getServiceTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            descriptor1 = template1.descriptor
            descriptor2 = template2.descriptor
            self.assertEqual(type(descriptor1), type(descriptor2))
            self.assertEqual(descriptor1.description, descriptor2.description)
            self.assertListEqual(descriptor1.adapters, descriptor2.adapters)
            self.assertEqual(descriptor1.propertySet, descriptor2.propertySet)
            self.assertEqual(descriptor1.dbEnvs, descriptor2.dbEnvs)
            self.assertEqual(descriptor1.logs, descriptor2.logs)
            self.assertEqual(descriptor1.name, descriptor2.name)
            self.assertEqual(descriptor1.entry, descriptor2.entry)

    def test_getNameNodeDescriptors_dict(self):
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                                    self.nodeApp, "name",
                        self.descriptorBuilder.getTemplateDescriptorDict(
                                            self.nodeApp, "server-template"), {})
        dict2 = self.getNodes()
        self.assertEqual(len(dict1), len(dict2))
        self.assertEqual((dict1 is None), (dict2 is None))
        for node in iter(dict1):
            self.assertIsNotNone(dict2[node])

    def test_getNameNodeDescriptors_attributes(self):

        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
            self.nodeApp, "name",
            self.descriptorBuilder.getTemplateDescriptorDict(
                                            self.nodeApp, "server-template"), {})
        dict2 = self.getNodes()
        for key in iter(dict1):
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(node1.loadFactor, node2.loadFactor)
            self.assertEqual(node1.description, node2.description)
            self.assertDictEqual(node1.propertySets, node2.propertySets)
            self.assertDictEqual(node1.variables, node2.variables)

    def test_getNameNodeDescriptors_instances(self):
        self.maxDiff = None
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                                    self.nodeApp, "name",
                            self.descriptorBuilder.getTemplateDescriptorDict(
                                        self.nodeApp, "server-template"), {})
        dict2 = self.getNodes()
        for key in iter(dict1):
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(len(node1.serverInstances), len(node2.serverInstances))
            for instance in node1.serverInstances:
                self.assertIn(instance, node2.serverInstances)


    def test_getNameNodeDescriptors_servers(self):
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                                    self.nodeApp, "name",
                        self.descriptorBuilder.getTemplateDescriptorDict(
                                        self.nodeApp, "server-template"), {})
        dict2 = self.getNodes()
        for key in dict1:
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(len(node1.servers), len(node2.servers))
            for i, server1 in enumerate(node1.servers):
                server2 = node2.servers[i]
                self.assertEqual(type(server1), type(server2))
                self.assertEqual(server1.description, server2.description)
                self.assertListEqual(server1.adapters, server2.adapters)
                self.assertEqual(server1.propertySet, server2.propertySet)
                self.assertEqual(server1.dbEnvs, server2.dbEnvs)
                self.assertEqual(server1.id, server2.id)
                self.assertEqual(server1.exe, server2.exe)
                self.assertEqual(server1.pwd, server2.pwd)
                self.assertEqual(server1.activation, server2.activation)
                self.assertEqual(server1.activationTimeout, \
                                  server2.activationTimeout)
                self.assertEqual(server1.applicationDistrib, \
                                 server2.applicationDistrib)
                self.assertEqual(server1.allocatable, server2.allocatable)
                self.assertEqual(server1.user, server2.user)
                self.assertEqual(server1.envs is None, server2.envs is None)
                if server1.envs is not None:
                    self.assertEquals(len(server1.envs), len(server2.envs))
                for env in server1.envs:
                    self.assertIn(env, server2.envs)


    def test_getNameNodeDescriptors_servers_icebox(self):
        self.descriptorBuilder.setTemplates(self.nodeApp)
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                                    self.nodeApp, "name",
                            self.descriptorBuilder.getTemplateDescriptorDict(
                                    self.nodeApp, "server-template"), {})
        dict2 = self.getNodes()
        for key in dict1:
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(len(node1.servers), len(node2.servers))
            for i, server1 in enumerate(node1.servers):
                server2 = node2.servers[i]
                if isinstance(server1, \
                          IceGrid.IceBoxDescriptor):
                    servicesSeq1 = server1.services
                    servicesSeq2 = server2.services
                    self.assertTrue(len(servicesSeq1), len(servicesSeq2))
                    for i, serviceInstance1 in enumerate(servicesSeq1):
                        serviceInstance2 = servicesSeq2[i]
                        self.assertEqual(serviceInstance1.template, \
                                         serviceInstance2.template)
                        self.assertEqual(serviceInstance1.parameterValues, \
                                         serviceInstance2.parameterValues)
                        self.assertEqual(serviceInstance1.propertySet, \
                                         serviceInstance2.propertySet)
                        descriptor1 = serviceInstance1.descriptor
                        descriptor2 = serviceInstance2.descriptor
                        self.assertEqual(descriptor1.name, descriptor2.name)
                        self.assertEqual(descriptor1.entry, descriptor2.entry)
                        self.assertEqual(descriptor1.adapters, \
                                         descriptor2.adapters)
                        self.assertEqual(descriptor1.propertySet, \
                                         descriptor2.propertySet)
                        self.assertEqual(descriptor1.dbEnvs, descriptor2.dbEnvs)
                        self.assertEqual(descriptor1.logs, descriptor2.logs)
                        self.assertEqual(descriptor1.description, \
                                         descriptor2.description)


    def test_getCategoryNodeDescriptors_dict(self):
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                    self.nodeApp, "category", {}, {})
        dict2 = self.getCategoryNodes()
        self.assertEqual(len(dict1), len(dict2))
        self.assertEqual((dict1 is None), (dict2 is None))
        for node in iter(dict1):
            self.assertIsNotNone(dict2[node])

    def test_getCategoryNodeDescriptors_attributes(self):
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                self.nodeApp, "category", {}, {})
        dict2 = self.getCategoryNodes()
        for key in iter(dict1):
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(node1.description, node2.description)

    def test_getCategoryNodeDescriptors_servers(self):
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                        self.nodeApp, "category", {}, {})
        dict2 = self.getCategoryNodes()
        for key in dict1:
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(len(node1.servers), len(node2.servers))
            for i, server1 in enumerate(node1.servers):
                server2 = node2.servers[i]
                self.assertEqual(type(server1), type(server2))
                self.assertEqual(server1.description, server2.description)
                self.assertListEqual(server1.adapters, server2.adapters)
                self.assertEqual(server1.propertySet, server2.propertySet)
                self.assertEqual(server1.dbEnvs, server2.dbEnvs)
                self.assertEqual(server1.id, server2.id)
                self.assertEqual(server1.exe, server2.exe)
                self.assertEqual(server1.pwd, server2.pwd)
                self.assertEqual(server1.activation, server2.activation)
                self.assertEqual(server1.activationTimeout, \
                                  server2.activationTimeout)
                self.assertEqual(server1.applicationDistrib, \
                                 server2.applicationDistrib)
                self.assertEqual(server1.maxDupes, \
                                 server2.maxDupes)
                self.assertEqual(server1.allocatable, server2.allocatable)
                self.assertEqual(server1.user, server2.user)
                self.assertEqual(server1.envs is None, server2.envs is None)
                if server1.envs is not None:
                    self.assertEquals(len(server1.envs), len(server2.envs))
                for env in server1.envs:
                    self.assertIn(env, server2.envs)


    def test_getCategoryNodeDescriptors_servers_icebox(self):
        self.descriptorBuilder.setTemplates(self.nodeApp)
        dict1 = self.descriptorBuilder.getNodeDescriptorDict(
                                             self.nodeApp, "category", {}, {})
        dict2 = self.getCategoryNodes()
        for key in dict1:
            node1 = dict1[key]
            node2 = dict2[key]
            self.assertEqual(len(node1.servers), len(node2.servers))
            for i, server1 in enumerate(node1.servers):
                server2 = node2.servers[i]
                if isinstance(server1, \
                          IceGrid.IceBoxDescriptor):
                    servicesSeq1 = server1.services
                    servicesSeq2 = server2.services
                    self.assertTrue(len(servicesSeq1), len(servicesSeq2))
                    for i, serviceInstance1 in enumerate(servicesSeq1):
                        serviceInstance2 = servicesSeq2[i]
                        self.assertEqual(serviceInstance1.template, \
                                         serviceInstance2.template)
                        self.assertEqual(serviceInstance1.parameterValues, \
                                         serviceInstance2.parameterValues)
                        self.assertEqual(serviceInstance1.propertySet, \
                                         serviceInstance2.propertySet)
                        descriptor1 = serviceInstance1.descriptor
                        descriptor2 = serviceInstance2.descriptor
                        self.assertEqual(descriptor1.name, descriptor2.name)
                        self.assertEqual(descriptor1.entry, descriptor2.entry)
                        self.assertEqual(descriptor1.adapters, \
                                         descriptor2.adapters)
                        self.assertEqual(descriptor1.propertySet, \
                                         descriptor2.propertySet)
                        self.assertEqual(descriptor1.dbEnvs, descriptor2.dbEnvs)
                        self.assertEqual(descriptor1.logs, descriptor2.logs)
                        self.assertEqual(descriptor1.description, \
                                         descriptor2.description)

    def test_getCategoryServerTemplates_dict(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "category-server-template")
        t2 = self.getCategoryServerTemplatesDict()
        self.assertEqual(len(t), len(t2))
        self.assertEqual((t is None), (t2 is None))
        for template in iter(t):
            self.assertIsNotNone(t2[template])

    def test_getCategoryServerTemplates_category_features(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                     self.nodeApp, "category-server-template")
        t2 = self.getCategoryServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertEqual(template1.category, \
                                  template2.category)
            self.assertEqual(template1.allocationRule, \
                                  template2.allocationRule)
            self.assertEqual(template1.growingRule, \
                                  template2.growingRule)
            self.assertEqual(template1.decreasingRule, \
                                  template2.decreasingRule)
            self.assertEqual(template1.maxInstances, \
                                  template2.maxInstances)


    def test_getCategoryServerTemplates_templates_parameters(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "category-server-template")
        t2 = self.getCategoryServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertDictEqual(template1.parameterDefaults, \
                                  template2.parameterDefaults)

    def test_getCategoryServerTemplates_templates_parameterDefaults(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "category-server-template")
        t2 = self.getCategoryServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            self.assertEqual(template1.parameters is None, \
                             template2.parameters is None)
            if template1.parameters != None:
                self.assertEqual(len(template1.parameters), \
                                  len(template2.parameters))
            for par in template1.parameters:
                self.assertIn(par, template2.parameters)

    def test_getCategoryServerTemplates_templates_descriptor(self):
        t = self.descriptorBuilder.getTemplateDescriptorDict(\
                                        self.nodeApp, "category-server-template")
        t2 = self.getCategoryServerTemplatesDict()
        for key in iter(t):
            template1 = t[key]
            template2 = t2[key]
            descriptor1 = template1.descriptor
            descriptor2 = template2.descriptor
            self.assertEqual(type(descriptor1), type(descriptor2))
            self.assertEqual(descriptor1.description, descriptor2.description)
            self.assertListEqual(descriptor1.adapters, descriptor2.adapters)
            self.assertEqual(descriptor1.propertySet, descriptor2.propertySet)
            self.assertEqual(descriptor1.dbEnvs, descriptor2.dbEnvs)
            self.assertEqual(descriptor1.id, descriptor2.id)
            self.assertEqual(descriptor1.exe, descriptor2.exe)
            self.assertEqual(descriptor1.pwd, descriptor2.pwd)
            self.assertEqual(descriptor1.activation, descriptor2.activation)
            self.assertEqual(descriptor1.activationTimeout, \
                              descriptor2.activationTimeout)
            self.assertEqual(descriptor1.applicationDistrib, \
                             descriptor2.applicationDistrib, descriptor1.id)
            self.assertEqual(descriptor1.allocatable, descriptor2.allocatable)
            self.assertEqual(descriptor1.user, descriptor2.user)
            self.assertEqual(descriptor1.envs is None, descriptor2.envs is None)
            if descriptor1.envs is not None:
                self.assertEquals(len(descriptor1.envs), len(descriptor2.envs))
            for env in descriptor1.envs:
                self.assertIn(env, descriptor2.envs)

    def test_getCategoryServerInstances(self):
        seq1 = self.descriptorBuilder.getServerInstanceSeq(self.nodeApp, {},
           self.descriptorBuilder.getTemplateDescriptorDict(self.nodeApp,
                                            "category-server-template"))
        seq2 = self.getCategoryInstancesSeq()
        self.assertEqual(len(seq1), len(seq2))
        instance1 = seq1[0]
        instance2 = seq2[0]
        self.assertEqual(instance1.template, instance2.template)
        self.assertEqual(instance1.parameterValues, instance2.parameterValues)
        self.assertEqual(instance1.propertySet, instance2.propertySet)
        self.assertEqual(instance1.servicePropertySets,
                         instance2.servicePropertySets)
