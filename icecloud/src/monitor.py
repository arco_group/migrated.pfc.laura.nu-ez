from threading import Thread, Event
import deployment_utilities
import Ice
from time import sleep
import IceGrid
from exc import NotDecreasableServer, NotDecreasableTemplate

class MonitorTimer(Thread):
    def __init__(self, monitor=None, period=None):
        super(MonitorTimer, self).__init__()
        self._stop = Event()
        self.period = period
        self.monitor = monitor

    def set_monitor(self, monitor):
        self.monitor = monitor

    def set_period(self, period):
        self.period = period

    def get_monitor(self):
        return self.monitor

    def run(self):
        if self.period is not None:
            while (self.stopped() == False):
                sleep(self.period)
                self.call_monitor()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def call_monitor(self):
        if not self.stopped():
            self.monitor.run()




class Monitor():
    def __init__(self, nodeinfo_retriever=None, scheduler=None, grid_state=None):
        self.nodeinfo_retriever = nodeinfo_retriever
        if self.nodeinfo_retriever != None:
            self.oldNodesState = self.nodeinfo_retriever.getAllNodeInfos()
        self.updatedServers = []
        self.grid_state = grid_state
        self.scheduler = scheduler

    def set_session_admin(self, admin):
        self.admin = admin

    def set_grid_state(self, state):
        self.grid_state = state
        self.scheduler.set_grid_state(state)

    def set_logger(self, logger):
        self.logger = logger
        self.scheduler.set_logger(logger)

    def set_scheduler(self, scheduler):
        self.scheduler = scheduler

    def get_grid_state(self):
        return self.grid_state

    def get_scheduler(self):
        return self.scheduler

    def run(self):
        try:
            self.grid_state.acquire_lock()
            node_infos = self.nodeinfo_retriever.getAllNodeInfos()
            checked_and_updated_server = {}
            checked_and_updated_template = {}
            for server in self.grid_state.get_original_server_assignment():
                checked_and_updated_server[server] = False
            for template in self.grid_state.get_template_instances():
                checked_and_updated_template[template] = False
            self.updatedServers = []
            for application in self.get_grid_state().get_applications():
                for node_info_id in node_infos:
                    original_server_assignment = self.grid_state.\
                                            get_original_server_assignment()
                    self.scale_servers(application, node_infos, node_info_id,
                                       original_server_assignment,
                                       checked_and_updated_server)
                    assigned_instances = self.grid_state.\
                                                        get_template_instances()
                    self.scale_templates(application, node_infos, node_info_id,
                            assigned_instances, checked_and_updated_template)

                self.scheduler.commit(application)
            self.grid_state.release_lock()
        except (Ice.CommunicatorDestroyedException):
            self.grid_state.release_lock()
            raise
        except (IceGrid.ServerNotExistException):
            self.grid_state.release_lock()
            raise

    def scale_servers(self, application, node_infos, node_info_id,
                       original_server_assignment, checked_and_updated_server):
        for assignedId in original_server_assignment:
            if ((original_server_assignment[assignedId].application == application)
            and  self.admin.getServerState(assignedId) == IceGrid.ServerState.Active):
                if (original_server_assignment[assignedId].node == node_info_id
                        and (not checked_and_updated_server[assignedId])):
                    # the server is in the node and hasn't been updated
                    assigned_server = original_server_assignment[assignedId]
                    if (self.check_growing_server(assigned_server,
                                 node_infos[node_info_id],
                                 checked_and_updated_server, assignedId)):
                        checked_and_updated_server[assignedId] = True
                        self.scheduler.grow_server(
                                                    assigned_server, node_infos)

                    if (self.check_decreasing_server(assigned_server,
                     node_infos[node_info_id], checked_and_updated_server,
                     assignedId)):
                        checked_and_updated_server[assignedId] = True
                        try:
                            self.scheduler.decrease_server(assignedId)
                        except NotDecreasableServer:
                            pass

                if (not checked_and_updated_server[assignedId]):
                # if the original server hasn't been updated, check secondary servers
                    for secondaryServer in \
                        self.get_grid_state().get_secondary_servers(assignedId):

                        if (secondaryServer.node == node_info_id and
                         not checked_and_updated_server[assignedId] and
                         self.admin.getServerState() == IceGrid.ServerState.Active):

                            assigned_server = original_server_assignment[assignedId]

                            if (self.check_growing_server(secondaryServer,
                             node_infos[node_info_id],
                             checked_and_updated_server, assignedId)):

                                checked_and_updated_server[assignedId] = True
                                self.scheduler.grow_server(assigned_server,
                                                            node_infos)

                            if (self.check_decreasing_server(
                             secondaryServer, node_infos[node_info_id],
                             checked_and_updated_server, assignedId)):
                                checked_and_updated_server[assignedId] = True
                                self.scheduler.decrease_server(
                                                    assignedId, secondaryServer)

    def scale_templates(self, application_name, node_infos, node_info_id,
        assigned_instances, checked_and_updated_template):
        for template_name in assigned_instances:
            template = self.get_grid_state().get_application(application_name).\
                        categoryServerTemplates[template_name]

            for assigned_instance in assigned_instances[template_name]:
                if (assigned_instance.node == node_info_id and
                    assigned_instances[template_name][0].application ==
                    application_name and
                    self.admin.getServerState(assigned_instance.name) ==
                    IceGrid.ServerState.Active):
                    if not checked_and_updated_template[template_name]:
                        if (self.check_growing_template(template_name, template,
                        node_infos[node_info_id], checked_and_updated_template)):
                            checked_and_updated_template[template_name] = True
                            self.scheduler.grow_template(template_name,
                                                node_infos, assigned_instance)

                        if (self.check_decreasing_template(template_name,
                         template, node_infos[node_info_id],
                         checked_and_updated_template)):
                            checked_and_updated_template[template_name] = True
                            try:
                                self.scheduler.decrease_template(template,
                                                        assigned_instance)
                            except NotDecreasableTemplate:
                                pass


    def check_growing_rule(self, assigned_server, node_info):
        return deployment_utilities.check_rule(assigned_server.growingRule,
                                               node_info)

    def check_decreasing_rule(self, assigned_server, node_info):
        return deployment_utilities.check_rule(assigned_server.decreasingRule,
                                               node_info)


    def check_growing_server(self, server, node_info,
                             checked_and_updated, assignedId):
        if (self.check_growing_rule(server, node_info) and
            (not checked_and_updated[assignedId])):
            return True
        else:
            return False

    def check_growing_template(self, template_name, template, node_info,
                             checked_and_updated):
        if (self.check_growing_rule(template, node_info) and
            (not checked_and_updated[template_name])):
            return True
        else:
            return False

    def check_decreasing_template(self, template_name, template, node_info,
                             checked_and_updated):
        if (self.check_decreasing_rule(template, node_info) and
            (not checked_and_updated[template_name])):
            return True
        else:
            return False

    def check_decreasing_server(self, server, node_info,
                                checked_and_updated, assignedId):
        if (self.check_decreasing_rule(server, node_info) and
            (not checked_and_updated[assignedId])):
            return True
        else:
            return False
