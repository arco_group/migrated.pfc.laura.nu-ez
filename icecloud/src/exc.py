class NoMatchingNodesForCategory(Exception):
    pass

class InvalidRule(Exception):
    pass

class NoMatchingNodesInCategoryForRule(Exception):
    pass

class NotDecreasableServer(Exception):
    pass

class NotDecreasableTemplate(Exception):
    pass

class InvalidXMLTemplate(Exception):
    pass

class InvalidFeature(Exception):
    pass

class ICRegistryNotAvailable(Exception):
    pass