#pragma once
#include <IceGrid/Descriptor.ice>
#include <IceGrid/Admin.ice>

#include <ICDescriptor.ice>

module IceCloud {
   
	struct NodeInfoData {
    	long ram;                  // MiB
    	int width;                 //Architecture width 32-64              
    	string cpuModel;           // As in /proc/cpuinfo
    	string cpuVendor;          // As in /proc/cpuinfo
        int cpuNproc;              // Number of processing cores
        string cpuArchitecture;    // CPU architecture as in lscpu
        int freeRam;               //MiB
        int custom;                // custom.info file contents
            
	};
	
	struct ExtendedNodeInfo{
	   NodeInfoData hostInfo;
	   IceGrid::LoadInfo loadInfo;     // CPU load in the last 1, 5, and 15 min
	   int numberOfServers;	           // number of server in the node
	   	   
	};

	interface NodeInfo {
		idempotent NodeInfoData getNodeInfo();
	};

    dictionary<string, ExtendedNodeInfo> NodeInfoMap;
    
    sequence<string> StringSeq;
    
    interface Scheduler {
          void grow(string serverName);
          void decrease(string serverName);
    };

    interface Supervisor{
          void report(string msg);
    };
    
    exception NoMatchingNodesForCategory {
        string reason;
    };
    
    exception NoMatchingNodesInCategoryForRule{
        string reason;
    };
    
    exception InvalidXMLTemplate{
        string reason;
    };
    
   exception InvalidTemplateException{
        string reason;
   };
    
   exception InvalidServerException{
        string reason;
    };
    interface ICRegistry {
            
        void addApplication(IceCloud::ApplicationDescriptor 
                                 applicationDescriptor) throws 
            IceGrid::AccessDeniedException, IceGrid::DeploymentException,
            NoMatchingNodesInCategoryForRule,
            NoMatchingNodesForCategory;
            
                                
        void removeApplication(string appName) throws 
            IceGrid::AccessDeniedException, IceGrid::DeploymentException, 
            IceGrid::ApplicationNotExistException;
            
        void startServer(string serverName) throws 
            IceGrid::ServerNotExistException, IceGrid::ServerStartException, 
            IceGrid::NodeUnreachableException, IceGrid::DeploymentException;
            
        void stopServer(string serverName) throws 
            IceGrid::ServerNotExistException, IceGrid::ServerStopException,
            IceGrid::NodeUnreachableException, IceGrid::DeploymentException;
        
                    
        void growServer(string serverName) throws InvalidServerException;
        
        void decreaseServer(string serverName) throws InvalidServerException;
        
        void growTemplate(string templateName, string application) throws 
                                                    InvalidTemplateException;
                                                    
        void decreaseTemplate(string templateName, string application) throws 
                                                    InvalidTemplateException;
                                                    
        IceGrid::ApplicationDescriptor getAppInfo(string application);

    };
    

};


