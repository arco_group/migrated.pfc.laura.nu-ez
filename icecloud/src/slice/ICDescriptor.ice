#pragma once


#include <Ice/Identity.ice>
#include <Ice/BuiltinSequences.ice>
#include <IceGrid/Descriptor.ice>

module IceCloud
{

/**
 *
 * An IceCloud server descriptor.---------------------------------------------
 *
 **/
class ServerDescriptor extends IceGrid::CommunicatorDescriptor
{
    /**
     *
     * The server id.
     *
     **/
    string id;
     /**
     *
     * The allocation rule
     *
     **/
    string allocationRule;
     /**
     *
     * The growing rule
     *
     **/
    string growingRule;
     /**
     *
     * The decreasing rule
     *
     **/
    string decreasingRule;
     /**
     *
     * Maximum dupes of the server
     *
     **/
    int maxDupes;
    /**
     *
     * The path of the server executable.
     *
     **/
    string exe;

    /**
     *
     * The Ice version used by this server. This is only required if
     * backward compatibility with servers using old Ice versions is
     * needed (otherwise the registry will assume the server is using
     * the same Ice version).
     * For example "3.1.1", "3.2", "3.3.0".
     *
     **/
    string iceVersion;

    /**
     *
     * The path to the server working directory.
     *
     **/
    string pwd;

    /**
     *
     * The command line options to pass to the server executable.
     *
     **/
    Ice::StringSeq options;

    /**
     *
     * The server environment variables.
     *
     **/
    Ice::StringSeq envs;

    /**
     *
     * The server activation mode (possible values are "on-demand" or
     * "manual").
     *
     **/
    string activation;

    /**
     *
     * The activation timeout (an integer value representing the
     * number of seconds to wait for activation).
     *
     **/
    string activationTimeout;

    /**
     *
     * The deactivation timeout (an integer value representing the
     * number of seconds to wait for deactivation).
     *
     **/
    string deactivationTimeout;

    /**
     *
     * Specifies if the server depends on the application
     * distribution.
     *
     **/
    bool applicationDistrib;

    /**
     *
     * The distribution descriptor.
     *
     **/
    IceGrid::DistributionDescriptor distrib;

    /**
     *
     * Specifies if the server is allocatable.
     *
     **/
    bool allocatable;

    /**
     *
     * The user account used to run the server.
     *
     **/
    string user;
};

/**
 *
 * A sequence of server descriptors.
 *
 **/
sequence<ServerDescriptor> ServerDescriptorSeq;

/**
 *
 * A server template instance descriptor.--------------------------------------
 *
 **/

struct ServerInstanceDescriptor
{
    /**
     *
     * The template used by this instance.
     *
     **/
    string template;

    /**
     *
     * The template parameter values.
     *
     **/
    IceGrid::StringStringDict parameterValues;

    /**
     *
     * The property set.
     *
     **/
    IceGrid::PropertySetDescriptor propertySet;

    /**
     *
     * The services property sets. It's only valid to set these
     * property sets if the template is an IceBox server template.
     *
     **/
    IceGrid::PropertySetDescriptorDict servicePropertySets;
};

/**
 *
 * A sequence of server instance descriptors.
 *
 **/

sequence<ServerInstanceDescriptor> ServerInstanceDescriptorSeq;

/**
 *
 * A template for category server templates.-----------------------------------
 *
 **/
struct CategoryTemplateDescriptor
{
    /**
     *
     * The template.
     *
     **/
    IceGrid::CommunicatorDescriptor descriptor;

    /**
     *
     * The parameters required to instantiate the template.
     *
     **/
    Ice::StringSeq parameters;

    /**
     *
     * The parameters default values.
     *
     **/
    IceGrid::StringStringDict parameterDefaults;
    
           /**
     *
     * The category
     *
     **/
    string category;
    
     
        /**
     *
     * The allocation rule
     *
     **/
    string allocationRule;
    
    
     /**
     *
     * The growing rule
     *
     **/
    string growingRule;
     /**
     *
     * The decreasing rule
     *
     **/
    string decreasingRule;
     /**
     *
     * Maximum instances of the template
     *
     **/
    int maxInstances;

};

/**
 *
 * A mapping of template identifier to template descriptor.
 *
 **/
dictionary<string, CategoryTemplateDescriptor> CategoryTemplateDescriptorDict;


/**
 *
 * A node descriptor.
 *
 **/
struct NodeDescriptor
{


    /**
     *
     * The server instances.
     *
     **/
    IceGrid::ServerInstanceDescriptorSeq serverInstances;

    /**
     *
     * Servers (which are not template instances).
     *
     **/
    ServerDescriptorSeq servers;


    /**
     *
     * The description of this node.
     *
     **/
    string description;

};

/**
 *
 * Mapping of node name to node descriptor.
 *
 **/
dictionary<string, NodeDescriptor> NodeDescriptorDict;



/**
 *
 * An application descriptor.--------------------------------------------------
 *
 **/
struct ApplicationDescriptor
{
    /**
     *
     * The application name.
     *
     **/
    string name;

    /**
     *
     * The variables defined in the application descriptor.
     *
     **/
    IceGrid::StringStringDict variables;

    /**
     *
     * The replica groups.
     *
     **/
    IceGrid::ReplicaGroupDescriptorSeq replicaGroups;

    /**
     *
     * The server templates.
     *
     **/
    IceGrid::TemplateDescriptorDict serverTemplates;

    /**
     *
     * The category server templates.
     *
     **/
    CategoryTemplateDescriptorDict categoryServerTemplates;
    
    
    /**
     *
     * The service templates.
     *
     **/
    IceGrid::TemplateDescriptorDict serviceTemplates;

    /**
     *
     * The application categories.
     *
     **/
    IceCloud::NodeDescriptorDict categoryNodes;

    /**
     *
     * The application nodes.
     *
     **/
    IceGrid::NodeDescriptorDict nodes;
    
    /**
     *
     * The category template instances.
     *
     **/   
    IceGrid::ServerInstanceDescriptorSeq categoryServerInstances;
    
    
    /**
     *
     * The application distribution.
     *
     **/
    IceGrid::DistributionDescriptor distrib;

    /**
     *
     * The description of this application.
     *
     **/
    string description;

    /**
     *
     * Property set descriptors.
     *
     **/
    IceGrid::PropertySetDescriptorDict propertySets;
};



};

