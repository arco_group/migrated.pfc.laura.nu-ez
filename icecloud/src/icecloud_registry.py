#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
import IceGrid
import os
import constants
import ConfigParser
from mako.runtime import TemplateNamespace
slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + ' --all ' + ic_slice)

import IceCloud
from ig_descriptor_builder import IGDescriptorBuilder
from lxml import etree
from nodeinfo_retriever import NodeInfoRetriever
import threading
import deployment_utilities
from assigned_server import AssignedServer, AssignedInstance
from grid_state import GridState
import logging
from monitor import MonitorTimer, Monitor
from scheduler import Scheduler
from exc import InvalidXMLTemplate, NoMatchingNodesForCategory, \
                NoMatchingNodesInCategoryForRule


def keepSessionAlive(ig_session, session_timeout):
    try:
        if ig_session != None:
            ig_session.keepAlive()
            threading.Timer(session_timeout / 2, keepSessionAlive,
                            [ig_session, session_timeout]).start()
    except (Ice.CommunicatorDestroyedException):  # @UndefinedVariable
        pass

class ICRegistryI(IceCloud.ICRegistry):  # @UndefinedVariable

    def __init__(self, ig_session_admin, nodeinfo_retriever, timer, cat,
                 logpath, communicator=None):



        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # create a file handler

        handler = logging.FileHandler(logpath)
        handler.setLevel(logging.INFO)

        # create a logging format

        formatter = logging.Formatter('%(asctime)s - %(module)s.%(funcName)s' +
                                      ' - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger

        self.logger.addHandler(handler)

        self.categories_file = cat
        self.ig_session_admin = ig_session_admin
        self.nodeinfo_retriever = nodeinfo_retriever
        self.grid_state = GridState()
        self.timer = timer
        timer.get_monitor().set_logger(self.logger)
        timer.get_monitor().set_grid_state(self.grid_state)
        timer.get_monitor().get_scheduler().set_session_admin(ig_session_admin)
        timer.get_monitor().get_scheduler().set_category_file(
                                                        self.categories_file)
        timer.get_monitor().get_scheduler().set_publisher(communicator)
        timer.get_monitor().set_session_admin(ig_session_admin)
        self.timer.start()






    def getAllNodeNames(self, current=None):
        return self.ig_session_admin.getAllNodeNames();

    def getAppDesc(self, current=None):
        return self.ig_session_admin.getApplicationInfo("NodoInfoApp")

    def patchApplication(self, applicationName, shutdown, current=None):
        self.get_grid_state().acquire_lock()
        self.ig_session_admin.patchApplication(applicationName, shutdown)
        self.get_grid_state().release_lock()

    def getAppInfo(self, name, current=None):
        return self.ig_session_admin.getApplicationInfo(name).descriptor


    def addApplication(self, ic_app_descriptor, current=None):
        self.grid_state.acquire_lock()
        ig_app_descriptor = None
        try:
            ig_app_descriptor, instanceAssignment = \
            deployment_utilities.ICapp_to_IGapp(ic_app_descriptor,
                            self.nodeinfo_retriever, self.categories_file, True)
            app_name = ic_app_descriptor.name
            self.get_grid_state().get_applications()[app_name] = \
                                                             ic_app_descriptor

            for category in ic_app_descriptor.categoryNodes:
                for serverDesc in (
                            ic_app_descriptor.categoryNodes[category].servers):
                    server = AssignedServer(ic_app_descriptor.name,
                            serverDesc.id, category, serverDesc.allocationRule,
                             serverDesc.growingRule, serverDesc.decreasingRule,
                             serverDesc.maxDupes)
                    self.grid_state.add_original_server(serverDesc.id, server)

            for node in ig_app_descriptor.nodes:
                for server in ig_app_descriptor.nodes[node].servers:
                    if server.id in self.get_grid_state().\
                                        get_original_server_assignment():
                        self.grid_state.\
                         get_original_server_assignment()[server.id].node = node

                for instance in ig_app_descriptor.nodes[node].serverInstances:
                    if instance.template in \
                                    ic_app_descriptor.categoryServerTemplates:
                        template = ic_app_descriptor.categoryServerTemplates[
                                                            instance.template]
                        currentInstances = self.get_grid_state().\
                        get_template_instances()
                        if instance.template in currentInstances:
                            count = len(currentInstances[instance.template])
                        else:
                            count = 0
                        instance.parameterValues["servercount"] = str(count)
                        serverInstance = AssignedInstance(ic_app_descriptor.name,
                         count, instance.template,
                         deployment_utilities.get_instance_name(instance,
                                                                template),
                         template.allocationRule,
                         template.growingRule, template.decreasingRule, node,
                         True)
                        self.get_grid_state().add_template_instance(
                                            instance.template, serverInstance)

            self.ig_session_admin.addApplication(ig_app_descriptor)
            self.ig_session_admin.patchApplication(ig_app_descriptor.name, True)







            self.logger.info("Added application: " + ig_app_descriptor.name)
#         try:
#             self.ig_session_admin.patchApplication(app_name, True)
#         except (IceGrid.PatchException):
#             pass
        except NoMatchingNodesForCategory, e:
            self.remove_app_from_grid_state(ic_app_descriptor, ig_app_descriptor)
            self.grid_state.release_lock()
            raise IceCloud.NoMatchingNodesForCategory(e.message)

        except NoMatchingNodesInCategoryForRule, e:
            self.remove_app_from_grid_state(ic_app_descriptor, ig_app_descriptor)
            self.grid_state.release_lock()
            raise IceCloud.NoMatchingNodesInCategoryForRule(e.message)
        except InvalidXMLTemplate, e:
            self.remove_app_from_grid_state(ic_app_descriptor, ig_app_descriptor)
            self.grid_state.release_lock()
            raise IceCloud.InvalidXMLTemplate(e.message)
        except Exception:
            self.remove_app_from_grid_state(ic_app_descriptor, ig_app_descriptor)
            self.grid_state.release_lock()
            raise
        self.grid_state.release_lock()


    def remove_app_from_grid_state(self, ic_app_descriptor, ig_app_descriptor):
        if ig_app_descriptor is not None:
            app_name = ic_app_descriptor.name
            self.get_grid_state().get_applications().pop(app_name)

            for category in ic_app_descriptor.categoryNodes:
                for serverDesc in (
                                ic_app_descriptor.categoryNodes[category].servers):

                        self.grid_state.get_original_server_assignment().pop(serverDesc.id)

            for node in ig_app_descriptor.nodes:
                for instance in ig_app_descriptor.nodes[node].serverInstances:
                    if instance.template in \
                                        ic_app_descriptor.categoryServerTemplates:
                        if instance.template in self.get_grid_state().get_template_instances():
                            self.get_grid_state().get_template_instances().pop(instance.template)


    def get_grid_state(self):
        return self.grid_state

    def removeApplication(self, appName, current=None):
        self.grid_state.acquire_lock()
        try:
            self.ig_session_admin.removeApplication(appName)
            self.grid_state.remove_servers(appName)
            self.grid_state.remove_templates(appName)
            if appName in self.grid_state.get_applications():
                self.grid_state.get_applications().pop(appName)
        except:
            self.grid_state.release_lock()
            raise
        self.grid_state.release_lock()

    def startServer(self, serverName, current=None):
        self.get_grid_state().acquire_lock()
        try:
            self.ig_session_admin.startServer(serverName)
            self.get_grid_state().release_lock()
        except:
            self.get_grid_state().release_lock()
            raise

    def growServer(self, serverName, current=None):
        self.get_grid_state().acquire_lock()
        servers = self.get_grid_state().get_original_server_assignment()
        if serverName not in servers:
            raise IceCloud.InvalidServerException(serverName)  # @UndefinedVariable
        try:
            assignedServer = servers[serverName]
            node_infos = self.nodeinfo_retriever.getAllNodeInfos()
            self.timer.get_monitor().get_scheduler().grow_server(assignedServer,
                                                            node_infos, True)
            self.timer.get_monitor().get_scheduler().commit(
                                                    assignedServer.application)
        except:
            self.get_grid_state().release_lock()
            raise
        self.get_grid_state().release_lock()

    def decreaseServer(self, serverName, current=None):
        self.get_grid_state().acquire_lock()
        servers = self.get_grid_state().get_original_server_assignment()
        if serverName not in servers:
            raise IceCloud.InvalidServerException(serverName)
        try:
            assignedServer = servers[serverName]
            self.timer.get_monitor().get_scheduler().decrease_server(
                                                                serverName)
            self.timer.get_monitor().get_scheduler().commit(
                                                    assignedServer.application)
        except:
            self.get_grid_state().release_lock()
            raise

    def growTemplate(self, templateName, application, current=None):
        self.get_grid_state().acquire_lock()
        try:
            instance = AssignedInstance(None, None, None)
            instance.application = application
            node_infos = self.nodeinfo_retriever.getAllNodeInfos()
            self.timer.get_monitor().get_scheduler().grow_template(
                                    templateName, node_infos, instance, True)
            self.timer.get_monitor().get_scheduler().commit(application)
        except:
            self.get_grid_state().release_lock()
            raise
        self.get_grid_state().release_lock()

    def decreaseTemplate(self, templateName, application, current=None):
        self.get_grid_state().acquire_lock()
        instances = self.get_grid_state().get_template_instances()
        removed_instance = None
        if templateName in instances:
            for instance in instances[templateName]:
                if not instance.original:
                    removed_instance = instance
        else:
            raise IceCloud.InvalidTemplateException(templateName)  # @UndefinedVariable
        try:
            if removed_instance:
                self.timer.get_monitor().get_scheduler().decrease_template(
                                                templateName, removed_instance)
                self.timer.get_monitor().get_scheduler().commit(application)
        except:
            self.get_grid_state().release_lock()
            raise

class RegistryApp(Ice.Application):

    def run(self, argv):
        try:
            config_file_name = argv[1]
            cfg = ConfigParser.ConfigParser()
            config_file = cfg.read([config_file_name])
            self.broker = self.communicator()
            self.set_ig_registry()
            user = cfg.get("ic_registry", "user")
            password = cfg.get("ic_registry", "password")
            self.set_nodeinfo_retriever(user, password)

            self.set_ig_registry_session(user, password)
            self.set_timer(cfg.getint("ic_registry", "timerperiod"))

            categories_file = os.path.join(os.path.dirname(__file__),
                                           cfg.get("ic_registry", "catfile"))
            logpath = os.path.join(os.path.dirname(__file__),
                                           cfg.get("ic_registry", "logpath"))

            servant = ICRegistryI(self.ig_session.getAdmin(),
                                  self.nodeinfo_retriever, self.timer,
                                  categories_file, logpath, self.broker)
            threading.Thread(target=keepSessionAlive,
             args=(self.ig_session, self.registry.getSessionTimeout(),)).start()

            adapter = self.broker.createObjectAdapter("ICRegistryAdapter")

            proxy = adapter.add(servant, self.broker.stringToIdentity(
                                                                "ICRegistry"))
            adapter.activate()
            self.shutdownOnInterrupt()
            self.broker.waitForShutdown()
        except Ice.CommunicatorDestroyedException:  # @UndefinedVariable
            if self.broker.isShutdown():
                pass
        except ConfigParser.Error:
            raise
        return 0

    def set_nodeinfo_retriever(self, user, password):
        self.nodeinfo_retriever = NodeInfoRetriever(self.broker, user, password)

    def set_ig_registry(self):
        self.registry = IceGrid.RegistryPrx.checkedCast(\
           self.broker.stringToProxy(constants.IG_REGISTRY_PROXY))

    def set_ig_registry_session(self, user, password):
        self.ig_session = self.registry.createAdminSession(
                                user, password)

    def set_timer(self, period):
        self.timer = MonitorTimer(Monitor(self.nodeinfo_retriever, Scheduler()),
                                  period)




if __name__ == '__main__':
    server = RegistryApp()

    sys.exit(server.main(sys.argv))



