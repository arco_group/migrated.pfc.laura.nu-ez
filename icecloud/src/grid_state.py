from threading import Lock
from copy import deepcopy

class GridState():
    def __init__(self):
        self.original_server_assignment = {}
        self.secondary_servers = {}
        self.template_instances = {}
        self.application_descriptor_dict = {}
        self.lock = Lock()

    def acquire_lock(self):
        return self.lock.acquire()

    def release_lock(self):
        return self.lock.release()

    def add_original_server(self, serverid, server):
        self.original_server_assignment[serverid] = server

    def get_original_server_assignment(self):
        return self.original_server_assignment

    def get_template_instances(self):
        return self.template_instances

    def add_template_instance(self, template_name, instance):
        if not template_name in self.template_instances:
            self.template_instances[template_name] = []
        self.template_instances[template_name].append(instance)

    def remove_template_instance(self, template_name, instance):
        index = None
        for index, ins in enumerate(self.template_instances[template_name]):
            if ins.count == instance.count:
                pos = index
        if index:
            self.template_instances[template_name].pop(index)

    def get_applications(self):
        return self.application_descriptor_dict

    def get_secondary_servers(self, serverid=None):
        if serverid == None:
            return self.secondary_servers
        elif serverid in self.secondary_servers:
            return self.secondary_servers[serverid]
        else:
            return []

    def add_secondary_server(self, parentid, server):
        if parentid not in self.secondary_servers:
            self.secondary_servers[parentid] = []
        self.secondary_servers[parentid].append(server)

    def get_application(self, app_id):
        return self.application_descriptor_dict[app_id]

    def remove_servers(self, app_id):
        removed = []
        for server in self.original_server_assignment:
            if self.original_server_assignment[server].application == app_id:
                if server in self.get_secondary_servers():
                    self.get_secondary_servers().pop(server)
                removed.append(server)
        for server in removed:
            self.original_server_assignment.pop(server)

    def remove_templates(self, app_id):
        for template in self.template_instances:
            for instance in list(self.template_instances[template]):
                if instance.application == app_id:
                    self.template_instances[template].remove(instance)



