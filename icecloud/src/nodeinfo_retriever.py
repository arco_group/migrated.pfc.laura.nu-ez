import constants

import Ice
import os
slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + ' --all ' + ic_slice)


import IceCloud
import IceGrid




class NodeInfoRetriever:
    def __init__(self, communicator, user, password):
        self.communicator = communicator
        self.user = user
        self.password = password

    def getAllNodeInfos(self):
        ni = {}
        nodeNames = self.getAllNodeNames()
        for node in nodeNames:
            try:
                proxy = self.communicator.stringToProxy("nodeinfo" +
                                          "@" + node + "-infoServer" +
                                          ".NodeInfoAdapter")
                remObject = IceCloud.NodeInfoPrx.checkedCast(proxy)  # @UndefinedVariable
                if remObject:
                    mynode = IceCloud.ExtendedNodeInfo()  # @UndefinedVariable
                    mynode.hostInfo = remObject.getNodeInfo()
                    mynode.numberOfServers = 0
                    for server_id in self.getAdmin().getAllServerIds():
                        if (self.getAdmin().getServerInfo(server_id).node == node):
                            mynode.numberOfServers += 1
                    ni[node] = mynode
            except Ice.NoEndpointException:  # @UndefinedVariable
                pass
            except Ice.CommunicatorDestroyedException:  # @UndefinedVariable
                if self.communicator.isShutdown():
                    pass
        return ni

    def getAllNodeNames(self):
        nodeNames = self.getAdmin().getAllNodeNames();
        return nodeNames

    def getAdminSession(self):
        registry = IceGrid.RegistryPrx.uncheckedCast(\
          self.communicator.stringToProxy(constants.IG_REGISTRY_PROXY))
        session = registry.createAdminSession(\
          self.user, self.password)
        return session

    def getAdmin(self):
        return self.getAdminSession().getAdmin()




