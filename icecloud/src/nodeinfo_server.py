#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import os
import Ice
slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + ' --all ' + ic_slice)

import IceCloud
import subprocess
from lxml import etree
import re

class NodeInfoI(IceCloud.NodeInfo):  # @UndefinedVariable

    def getNodeInfo(self, current=None):
        ni = IceCloud.NodeInfoData()  # @UndefinedVariable
        res = str(subprocess.check_output('lshw -xml', shell=True))
        xmldoc = etree.fromstring(res)
        mem = xmldoc.xpath("//node[@id='memory']")
        for element in mem[0].iter("size"):
            ni.ram = int(int(element.text) / 2 ** 20)

        ni.width = int((xmldoc[0].xpath("width"))[0].text)

        try:
            architecture = str(subprocess.check_output('lscpu | ' +
                                        'grep "Architecture:"', shell=True))
            architecture = architecture.partition(
                                    "Architecture:")[2].partition("\n")[0]

        except subprocess.CalledProcessError:
            architecture = str(subprocess.check_output('lscpu | ' +
                                        'grep "Arquitectura:"', shell=True))
            architecture = architecture.partition(
                                        "Arquitectura:")[2].partition("\n")[0]

        ni.cpuArchitecture = architecture.replace(" ", "")

        cpu_model = str(subprocess.check_output('cat /proc/cpuinfo ' +
            '|grep -m1 \"model name\"', shell=True))
        ni.cpuModel = (cpu_model.partition(": ")[2]).partition("\n")[0]

        vendor_id = str(subprocess.check_output('cat /proc/cpuinfo ' +
            '|grep -m1 \"vendor_id\"', shell=True))

        ni.cpuVendor = (vendor_id.partition(": ")[2]).partition("\n")[0]

        ni.cpuNproc = int(subprocess.check_output('nproc', shell=True))

        free_ram = subprocess.check_output('free -t -m | grep Mem:', shell=True)
        free_ram = re.findall('Mem:\s+(\d+)\s+(\d+)\s+(\d+)', free_ram)
        ni.freeRam = int(free_ram[0][2])

        custom_file = os.path.join(os.path.dirname(__file__), "custom.info")
        f = open(custom_file)
        custom = f.read()
        try:
            ni.custom = int(custom)
        except:
            ni.custom = 0
        f.close()

        return ni


class NodeInfoServer(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = NodeInfoI()

        adapter = broker.createObjectAdapter("NodeInfoAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("nodeinfo"))
        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()
        return 0

if __name__ == '__main__':
    server = NodeInfoServer()
    sys.exit(server.main(sys.argv))



