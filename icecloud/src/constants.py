IG_REGISTRY_PROXY = "IceGrid/Registry"
CONFIG_NODE = "/home/laura/Clase/PFC/pfc.icecloud/icecloud/src/config/node0.cfg"
RULES_DICT = {'min_servers':['numberOfServers'],
    'min_load_1':['loadInfo', 'avg1'], 'min_load_5':['loadInfo', 'avg5'],
    'min_load_15':['loadInfo', 'avg15'], 'min_custom':['hostInfo', 'custom'],
    'max_servers':['numberOfServers'], 'max_load_1':['loadInfo', 'avg1'],
    'max_load_5':['loadInfo', 'avg5'], 'max_load_15':['loadInfo', 'avg15'],
    'max_custom':['hostInfo', 'custom'], 'num_custom':['hostInfo', 'custom'],
    'cpu_load_5':['loadInfo', 'avg5'], 'num_servers':['numberOfServers'],
    'cpu_load_1':['loadInfo', 'avg1'], 'cpu_load_15':['loadInfo', 'avg15'],
    'min_free_ram':['hostInfo', 'freeRam'],
    'max_free_ram':['hostInfo', 'freeRam'],
    'free_ram':['hostInfo', 'freeRam'],
    None:None}
SIMPLE_RULES = ['min_servers', 'min_load_1', 'min_load_5', 'min_load_15',
                'min_free_ram', 'min_custom', 'max_servers', 'max_load_1',
                'max_load_5', 'max_load_15',
                'max_free_ram', 'max_custom', None]
CATEGORIES_FEATURES = {'width':['hostInfo', 'width'],
                       'ram':['hostInfo', 'ram'],
                       'cpu-model':['hostInfo', 'cpuModel'],
                       'cpu-vendor':['hostInfo', 'cpuVendor'],
                       'n-proc':['hostInfo', 'cpuNproc'],
                       'arch': ['hostInfo', 'cpuArchitecture'],
                       'custom':['hostInfo', 'custom']}

