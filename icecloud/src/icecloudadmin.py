#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import Ice
slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + ' --all ' + ic_slice)
import IceCloud
import constants
import argparse
from exc import ICRegistryNotAvailable
from ic_descriptor_builder import ICDescriptorBuilder

class IceCloudAdmin:

    def __init__(self, locatorconfig):
        prop = Ice.createProperties()
        prop.setProperty("Ice.Default.Locator", locatorconfig)
        id = Ice.InitializationData()
        id.properties = prop
        self.ic = Ice.initialize(id)

        proxy = self.ic.stringToProxy("ICRegistry" +
                                      "@ICRegistryServer" +
                                      ".ICRegistryAdapter")
        self.iceCloudRegistry = IceCloud.ICRegistryPrx.checkedCast(proxy)  # @UndefinedVariable


    def addApplication(self, applicationFile):
        desc = self.getApplicationDescriptor(applicationFile)
        self.iceCloudRegistry.addApplication(desc)
        return desc.name

    def removeApplication(self, applicationName):
        self.iceCloudRegistry.removeApplication(applicationName)

    def patchApplication(self, applicationName, shutdown):
        self.iceCloudRegistry.patchApplication(applicationName, shutdown)

    def startServer(self, serverId):
        self.iceCloudRegistry.startServer(serverId)

    def stopServer(self, serverId):
        self.iceCloudRegistry.stopServer(serverId)

    def growServer(self, serverId):
        self.iceCloudRegistry.growServer(serverId)

    def decreaseServer(self, serverId):
        self.iceCloudRegistry.decreaseServer(serverId)

    def growTemplate(self, template, app):
        self.iceCloudRegistry.growTemplate(template, app)

    def decreaseTemplate(self, template, app):
        self.iceCloudRegistry.decreaseTemplate(template, app)

    def getApplicationInfo(self, applicationName):
        return self.iceCloudRegistry.getApplicationInfo(applicationName)

    def getApplicationDescriptor(self, appXML):
        descriptorBuilder = ICDescriptorBuilder()
        return descriptorBuilder.getApplicationDescriptor(appXML)


def getRegistry(config):
    try:
        ic_admin = IceCloudAdmin(config)
    except:
        raise ICRegistryNotAvailable()

    return ic_admin

if __name__ == '__main__':

    try:

        parser = argparse.ArgumentParser("IceCloud Administration Tool")
        parser.add_argument('--config', '-c', help='Locator configuration',
                            dest='config', action='store',
                            metavar='LOCATOR_CONFIG')
        group = parser.add_mutually_exclusive_group()
        group.add_argument('--add', '-a', help='Add application', dest='add_app',
                            action='store', metavar='APP_FILE')
        group.add_argument('--remove', '-r', help='Remove application',
                           dest='remove_app', metavar='APP_NAME', action='store')
        group.add_argument('--start', help='Start server', dest='start_server',
                            action='store', metavar='SERVER_NAME')
        group.add_argument('--stop', help='Stop server', dest='stop_server',
                            action='store', metavar='SERVER_NAME')

        group.add_argument('--duplicate-server', help='Duplicate server SERVER_NAME',
                           dest='grow', action='store', metavar='SERVER_NAME')
        group.add_argument('--decrease-server', help='Decrease server SERVER_NAME',
                           dest='decrease', action='store', metavar='SERVER_NAME')

        group.add_argument('--grow-template', help='Instantiate template TEMPLATE_NAME',
                           dest='growt', action='store', metavar='TEMPLATE_NAME')
        group.add_argument('--decrease-template', help='Remove template instance TEMPLATE_NAME',
                           dest='decreaset', action='store', metavar='TEMPLATE_NAME')


        group.add_argument('--app', help='Application name',
                           dest='app_name', action='store', metavar='APP_NAME')

        args = parser.parse_args()

        try:
            if not args.config:
                parser.error('Locator configuration not given')

            else:

                ic_admin = getRegistry(args.config)

                if args.add_app:
                    print "Adding Application ..."
                    name = ic_admin.addApplication(args.add_app)
                    print "Application was added: " + name


                elif args.remove_app:
                    print "Removing application... "
                    ic_admin.removeApplication(args.remove_app)
                    print "Application was removed " + args.remove_app

                elif args.start_server:
                    print "Starting server " + args.start_server + " ..."
                    ic_admin.startServer(args.start_server)
                    print "Server was started: " + args.start_server


                elif args.stop_server:
                    print "Stopping server " + args.stop_server + "..."
                    ic_admin.stopServer(args.stop_server)
                    print "Server was stopped: " + args.stop_server


                elif args.grow:
                    print "Duplicating server " + args.grow
                    ic_admin.grow_server(args.grow)
                    print "Server " + args.grow + " was duplicated."
                elif args.decrease:
                    print "Decreasing server " + args.decrease
                    ic_admin.decreaseServer(args.decrease)
                    print "Server " + args.grow + " was decreased."

                elif args.growt:
                    if not args.app_name:
                        parser.error('Must specify the application name with --app option')
                    else:
                        print "Instatiating template" + args.growt
                        ic_admin.growTemplate(args.growt, args.app_name)
                        print "Template " + args.growt + " was instantiated."

                elif args.decreaset:
                    if not args.app_name:
                        parser.error('Must specify the application name with --app option')
                    else:
                        print "Removing instance of " + args.decreaset
                        ic_admin.decreaseTemplate(args.decreaset, args.app_name)
                        print "Instance of " + args.decreaset + " was removed."

                else:
                    parser.print_help()

        except ICRegistryNotAvailable:
            print "Aborted: IceCloud Registry not available"

        except Exception as e:
            print "Aborted: ", e
    except Exception as e:
        raise


