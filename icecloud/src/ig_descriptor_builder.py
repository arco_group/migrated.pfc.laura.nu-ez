import Ice
import os
slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + \
              ' --all ' + ic_slice)

import IceCloud
import IceGrid

class IGDescriptorBuilder():


    def getApplicationDescriptor(self, iCapplicationDescriptor,
                                 serversInNodeDict, instancesInNodeDict):
        applicationDescriptor = IceGrid.ApplicationDescriptor()  # @UndefinedVariable
        applicationDescriptor.variables = self.getVariables(
                                            iCapplicationDescriptor.variables)
        applicationDescriptor.replicaGroups = self.getReplicaGroups(
                                        iCapplicationDescriptor.replicaGroups)
        applicationDescriptor.serverTemplates = self.getServerTemplates(
                            iCapplicationDescriptor.serverTemplates,
                            iCapplicationDescriptor.categoryServerTemplates)
        applicationDescriptor.serviceTemplates = self.getServerTemplates(
                                     iCapplicationDescriptor.serviceTemplates,
                                     {})
        applicationDescriptor.name = iCapplicationDescriptor.name
        applicationDescriptor.distrib = self.getDistributionDescriptor(
                                              iCapplicationDescriptor.distrib)
        applicationDescriptor.description = iCapplicationDescriptor.description
        applicationDescriptor.propertySets = self.getPropertySetDict(
                                          iCapplicationDescriptor.propertySets)

        applicationDescriptor.nodes = self.getNodeDescriptorDict(
                                                 iCapplicationDescriptor.nodes)
        self.addNodesDefinedByCategory(applicationDescriptor.nodes,
                    iCapplicationDescriptor.categoryNodes, serversInNodeDict)
        self.addCategoryInstances(applicationDescriptor.nodes,
            self.reverse(instancesInNodeDict))

        return applicationDescriptor

    def reverse(self, instancesInNodeDict):
        reversed = {}
        for instance in instancesInNodeDict:
            if instancesInNodeDict[instance] not in reversed:
                reversed[instancesInNodeDict[instance]] = [instance]
            else:
                reversed[instancesInNodeDict[instance]].append(instance)
        return reversed

    def addNodesDefinedByCategory(self, nodes, categoryNodes,
                                  serversInNodeDict):
        for key in categoryNodes:
            for server in categoryNodes[key].servers:
                self.addServer(nodes, server, serversInNodeDict[server.id])

    def addCategoryInstances(self, nodes, instancesInNodeDict):
        for node in instancesInNodeDict:
            if node not in nodes:
                self.addNode(nodes, node)
            for instance in instancesInNodeDict[node]:
                self.addInstance(nodes, instance, node)

    def addInstance(self, nodes, instance, node):
        if node not in nodes:
            self.addNode(nodes, node)
        nodes[node].serverInstances.append(
                                    self.getServerInstanceDescriptor(instance))


    def addServer(self, nodes, server, deployingNode):
        if deployingNode not in nodes:
            self.addNode(nodes, deployingNode)
        nodes[deployingNode].servers.append(self.getCommunicatorDescriptor(
                                                                        server))


    def addNode(self, nodes, key):
        node = IceGrid.NodeDescriptor()  # @UndefinedVariable
        node.variables = {}
        node.serverInstances = []
        node.servers = []
        node.propertySets = {}
        nodes[key] = node

    def getVariables(self, variables):
        return variables

    def getReplicaGroups(self, iCreplicaGroups):
        replicaGroups = []
        for replicaGroup in iCreplicaGroups:
            replicaGroups.append(self.getReplicaGroupDescriptor(replicaGroup))
        return replicaGroups

    def getReplicaGroupDescriptor(self, replicaGroup):
        replicaGroupDesc = IceGrid.ReplicaGroupDescriptor()  # @UndefinedVariable
        replicaGroupDesc.id = replicaGroup.id
        replicaGroupDesc.loadBalancing = \
            self.getLoadBalancingPolicy(replicaGroup.loadBalancing)
        replicaGroupDesc.proxyOptions = replicaGroup.proxyOptions
        replicaGroupDesc.description = replicaGroup.description
        replicaGroupDesc.objects = self.getObjectDescriptorSeq(
                                                        replicaGroup.objects)
        return replicaGroupDesc

    def getLoadBalancingPolicy(self, loadBalancing):
        if isinstance(loadBalancing, IceCloud.RandomLoadBalancingPolicy):  # @UndefinedVariable
            loadBalancingPol = IceGrid.RandomLoadBalancingPolicy()  # @UndefinedVariable
        elif isinstance(loadBalancing, IceCloud.OrderedLoadBalancingPolicy):  # @UndefinedVariable
            loadBalancingPol = IceGrid.OrderedLoadBalancingPolicy()  # @UndefinedVariable
        elif isinstance(loadBalancing, IceCloud.RoundRobinLoadBalancingPolicy):  # @UndefinedVariable
            loadBalancingPol = IceGrid.RoundRobinLoadBalancingPolicy()  # @UndefinedVariable
        elif isinstance(loadBalancing, IceCloud.AdaptiveLoadBalancingPolicy):  # @UndefinedVariable
            loadBalancingPol = IceGrid.AdaptiveLoadBalancingPolicy()  # @UndefinedVariable
            loadBalancingPol.loadSample = loadBalancing.loadSample
        loadBalancingPol.nReplicas = loadBalancing.nReplicas
        return loadBalancingPol

    def getObjectDescriptorSeq(self, objects):
        objectSeq = []
        for obj in objects:
            objectSeq.append(self.getObjectDescriptor(obj))
        return objectSeq

    def getObjectDescriptor(self, obj):
        objectDesc = IceGrid.ObjectDescriptor()  # @UndefinedVariable
        iden = Ice.Identity()  # @UndefinedVariable
        iden.name = obj.id.name
        iden.category = obj.id.category
        objectDesc.id = iden
        objectDesc.type = obj.type
        if (obj.proxyOptions is not None):
            objectDesc.proxyOptions = obj.proxyOptions
        return objectDesc

    def getServerTemplates(self, serverTemplates, categoryServerTemplates):
        templateDescriptorDict = {}
        for key in serverTemplates:
            templateDescriptorDict[key] = self.getTemplateDescriptor(
                                                        serverTemplates[key])
        for key in categoryServerTemplates:
            templateDescriptorDict[key] = self.getTemplateDescriptor(
                                                categoryServerTemplates[key])
        return templateDescriptorDict


    def getTemplateDescriptor(self, serverTemplate):
        templateDescriptor = IceGrid.TemplateDescriptor()  # @UndefinedVariable
        templateDescriptor.descriptor = self.getCommunicatorDescriptor(
                                                    serverTemplate.descriptor)
        templateDescriptor.parameters = serverTemplate.parameters
        templateDescriptor.parameterDefaults = serverTemplate.parameterDefaults
        return templateDescriptor

    def getServiceInstanceDescriptorSeq(self, services):
        serviceInstanceDescriptorSeq = []
        for service in services:
            serviceInstanceDescriptorSeq.append(\
                                self.getServiceInstanceDescriptor(service))
        return serviceInstanceDescriptorSeq


    def getServiceInstanceDescriptor(self, service):
        serviceInstanceDescriptor = IceGrid.ServiceInstanceDescriptor()
        serviceInstanceDescriptor.descriptor = \
            self.getCommunicatorDescriptor(service.descriptor)
        serviceInstanceDescriptor.template = service.template
        serviceInstanceDescriptor.parameterValues = service.parameterValues
        serviceInstanceDescriptor.propertySet = self.getPropertySet(
                                                        service.propertySet)
        return serviceInstanceDescriptor


    def getCommunicatorDescriptor(self, communicator):
        communicatorDescriptor = IceGrid.CommunicatorDescriptor()  # @UndefinedVariable
        if isinstance(communicator, IceCloud.ServerDescriptor) or \
            isinstance(communicator, IceGrid.ServerDescriptor) or \
            isinstance(communicator, IceGrid.IceBoxDescriptor):
            if isinstance(communicator, IceCloud.ServerDescriptor) or\
                isinstance(communicator, IceGrid.ServerDescriptor):  # @UndefinedVariable
                communicatorDescriptor = IceGrid.ServerDescriptor()  # @UndefinedVariable


            if isinstance(communicator, IceGrid.IceBoxDescriptor):
                communicatorDescriptor = IceGrid.IceBoxDescriptor()  # @UndefinedVariable
                communicatorDescriptor.services = \
                    self.getServiceInstanceDescriptorSeq(communicator.services)
            communicatorDescriptor.id = communicator.id
            communicatorDescriptor.exe = communicator.exe
            communicatorDescriptor.iceVersion = communicator.iceVersion
            communicatorDescriptor.pwd = communicator.pwd
            communicatorDescriptor.options = communicator.options
            communicatorDescriptor.envs = communicator.envs
            communicatorDescriptor.activation = communicator.activation
            communicatorDescriptor.activationTimeout = \
                                                communicator.activationTimeout
            communicatorDescriptor.deactivationTimeout = \
                                            communicator.deactivationTimeout
            communicatorDescriptor.applicationDistrib = \
                                            communicator.applicationDistrib
            communicatorDescriptor.distrib = \
                self.getDistributionDescriptor(communicator.distrib)
            communicatorDescriptor.allocatable = communicator.allocatable
            communicatorDescriptor.user = communicator.user
        elif isinstance(communicator, IceGrid.ServiceDescriptor):  # @UndefinedVariable
            communicatorDescriptor = IceGrid.ServiceDescriptor()  # @UndefinedVariable
            communicatorDescriptor.name = communicator.name
            communicatorDescriptor.entry = communicator.entry
        communicatorDescriptor.adapters = self.getAdapterSeq(
                                                        communicator.adapters)
        communicatorDescriptor.propertySet = \
             self.getPropertySet(communicator.propertySet)
        communicatorDescriptor.dbEnvs = self.getDbEnvSeq(communicator.dbEnvs)
        # serverDescriptor.logs = self.getLogs(server)
        communicatorDescriptor.description = communicator.description
        return communicatorDescriptor

    def getAdapterSeq(self, adapters):
        adapterDescriptorSeq = []
        for adapter in adapters:
            adapterDescriptorSeq.append(self.getAdapterDescriptor(adapter))
        return adapterDescriptorSeq

    def getAdapterDescriptor(self, adapter):
        adapterDescriptor = IceGrid.AdapterDescriptor()  # @UndefinedVariable
        adapterDescriptor.name = adapter.name
        adapterDescriptor.description = adapter.description
        adapterDescriptor.id = adapter.id
        adapterDescriptor.replicaGroupId = adapter.replicaGroupId
        adapterDescriptor.priority = adapter.priority
        adapterDescriptor.registerProcess = adapter.registerProcess
        adapterDescriptor.serverLifetime = adapter.serverLifetime
        adapterDescriptor.objects = \
         self.getObjectDescriptorSeq(adapter.objects)
        adapterDescriptor.allocatables = \
         self.getObjectDescriptorSeq(adapter.allocatables)
        return adapterDescriptor


    def getPropertySet(self, propertySet):
        propertySetDesc = IceGrid.PropertySetDescriptor()  # @UndefinedVariable
        propertySetDesc.references = propertySet.references
        propertySetDesc.properties = self.getProperties(propertySet.properties)
        return propertySetDesc


    def getProperties(self, properties):
        propertiesSeq = []
        for prop in properties:
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = prop.name
            property.value = prop.value
            propertiesSeq.append(property)
        return propertiesSeq

    def getDbProperties(self, properties):
        propertiesSeq = []
        for prop in properties:
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = prop.name
            property.value = prop.value
            propertiesSeq.append(property)
        return propertiesSeq

    def getPropertySetDict(self, propertySets):
        propertySetDict = {}
        for key in propertySets:
            propertySetDict[key] = self.getPropertySet(propertySets[key])
        return propertySetDict



    def getDbEnvSeq(self, dbEnvs):
        dbEnvSeq = []
        for dbEnv in dbEnvs:
            db = IceGrid.DbEnvDescriptor()  # @UndefinedVariable
            db.name = dbEnv.name
            db.description = dbEnv.description
            db.dbHome = dbEnv.dbHome
            db.properties = self.getDbProperties(dbEnv.properties)
            dbEnvSeq.append(db)
        return dbEnvSeq

    def getDistributionDescriptor(self, distrib):
        distributionDescriptor = IceGrid.DistributionDescriptor()  # @UndefinedVariable
        distributionDescriptor.icepatch = distrib.icepatch
        distributionDescriptor.directories = distrib.directories
        return distributionDescriptor


    def getServerInstanceDescriptor(self, serverInstance):
        serverInstanceDescriptor = IceGrid.ServerInstanceDescriptor()  # @UndefinedVariable
        serverInstanceDescriptor.template = serverInstance.template
        serverInstanceDescriptor.parameterValues = \
         serverInstance.parameterValues
        serverInstanceDescriptor.propertySet = \
         self.getPropertySet(serverInstance.propertySet)
        serverInstanceDescriptor.servicePropertySets = \
            self.getPropertySetDict(serverInstance.servicePropertySets)
        return serverInstanceDescriptor


    def getServerInstanceSeq(self, serverInstances):
        serverInstanceSeq = []
        for serverInstance in serverInstances:
            serverInstanceSeq.append(\
                             self.getServerInstanceDescriptor(serverInstance))
        return serverInstanceSeq


    def getServerDescriptorSeq(self, servers):
        serverDescriptorSeq = []
        for server in servers:
            serverDescriptor = self.getCommunicatorDescriptor(server)
            serverDescriptorSeq.append(serverDescriptor)
        return serverDescriptorSeq


    def getNodeDescriptor(self, node):
        nodeDescriptor = IceGrid.NodeDescriptor()  # @UndefinedVariable
        nodeDescriptor.variables = self.getVariables(node.variables)
        nodeDescriptor.serverInstances = self.getServerInstanceSeq(
                                                        node.serverInstances)
        nodeDescriptor.loadFactor = node.loadFactor
        nodeDescriptor.description = node.description
        nodeDescriptor.propertySets = self.getPropertySetDict(node.propertySets)
        nodeDescriptor.servers = self.getServerDescriptorSeq(node.servers)
        return nodeDescriptor


    def getNodeDescriptorDict(self, nodes):
        nodeDescriptorDict = {}
        for key in nodes:
            nodeDescriptorDict[key] = self.getNodeDescriptor(nodes[key])
        return nodeDescriptorDict

    def getApplicationUpdateDescriptor(self, ig_app_descriptor):
        application_update_descriptor = \
            IceGrid.ApplicationUpdateDescriptor()  # @UndefinedVariable
        application_update_descriptor.name = ig_app_descriptor.name
        application_update_descriptor.distrib = \
            IceGrid.BoxedDistributionDescriptor()  # @UndefinedVariable
        application_update_descriptor.distrib.value = ig_app_descriptor.distrib
        application_update_descriptor.nodes = []
        for node in ig_app_descriptor.nodes:
            updated_node = IceGrid.NodeUpdateDescriptor()  # @UndefinedVariable
            updated_node.removeServers = []
            updated_node.name = node
            for server in ig_app_descriptor.nodes[node].servers:
                updated_node.removeServers.append(server.id)
            updated_node.servers = ig_app_descriptor.nodes[node].servers
            updated_node.serverInstances = \
                ig_app_descriptor.nodes[node].serverInstances
            application_update_descriptor.nodes.append(updated_node)
        return application_update_descriptor
