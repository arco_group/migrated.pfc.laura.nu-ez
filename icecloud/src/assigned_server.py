class AssignedServer():
    def __init__(self, application, id, category, rule=None, growing=None, decreasing=None, maxDupes=None, node=None, original=True):

        '''
        id : string, id of server
        category : string, category where the server should be deployed
        rule : string, rule to decide the preferred nodes in a category
        growing: string, growing rule
        decreasing: string, decreasing rule
        maxDupes: int, max dupes of the server
        node: string, assigned node name
        original: boolean, true if the server was in the original description
        '''
        self.application = application
        self.id = id
        self.category = category
        self.allocationRule = rule
        self.growingRule = growing
        self.decreasingRule = decreasing
        self.maxDupes = maxDupes
        self.node = node
        self.original = original



    def set_node(self, node):
        self.node = node



    def __repr__(self):
        return ("\n{\n id = %s\n application = %s\n category = %s\n allocationRule = %s\n " + \
        "growingRule = %s\n decreasingRule = %s\n node = %s\n original = %s\n}\n") \
        % (self.id, self.application, self.category, self.allocationRule,
            self.growingRule, self.decreasingRule, self.node, self.original)


class AssignedInstance():
    def __init__(self, application, count, template, name=None, rule=None,
                 growing=None, decreasing=None, node=None, original=True):

        '''
        template: string
        category : string, category where the server should be deployed
        rule : string, rule to decide the preferred nodes in a category
        growing: string, growing rule
        decreasing: string, decreasing rule
        node: string, assigned node name
        original: boolean, true if the server was in the original description

        '''
        self.application = application
        self.count = count
        self.template = template
        self.name = name
        self.allocationRule = rule
        self.growingRule = growing
        self.decreasingRule = decreasing
        self.node = node
        self.original = original



    def set_node(self, node):
        self.node = node



    def __repr__(self):
        return ("\n{\n application = %s\n name = %s\n count = %s\n template = %s\n allocationRule = %s\n " + \
        "growingRule = %s\n decreasingRule = %s\n node = %s\n original = %s\n}\n") \
        % (self.application, self.name, str(self.count), self.template,
           self.allocationRule,
            self.growingRule, self.decreasingRule, self.node, self.original)

