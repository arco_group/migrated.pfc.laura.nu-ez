from ig_descriptor_builder import IGDescriptorBuilder
import constants
from exc import NoMatchingNodesForCategory, InvalidRule, NoMatchingNodesInCategoryForRule
from lxml import etree
import os
import operator
import exc
import re

def ICapp_to_IGapp(IC_app_descriptor, nodeinfo_retriever, filename,
                   returnTemplateAssignment=False):
    node_infos = nodeinfo_retriever.getAllNodeInfos()
    server_assignment = getServerDeployedInNodeDict(IC_app_descriptor,
                                                 node_infos, filename)
    templates_instances_assignment = getInstanceDeployedInNodeDict(
                            IC_app_descriptor, node_infos, filename)
    descriptorBuilder = IGDescriptorBuilder()
    ret = descriptorBuilder.getApplicationDescriptor(
            IC_app_descriptor, server_assignment, templates_instances_assignment)

    if returnTemplateAssignment:
        ret = ret, templates_instances_assignment

    return ret

def getInstanceDeployedInNodeDict(applicationDescriptor, allNodesInfoDict,
                                 filename):
    """
        Returns a dict with  the assigned node for each instance according to
        template category: {"node1": serverInstance1, "node": serverInstance2, ...
    """
    instanceDeploymentDict = {}
    if not allNodesInfoDict:
        raise RuntimeError("No nodes available")

    nodesByCategoryDict = getNodesByCategory(allNodesInfoDict, filename)
    for instance in applicationDescriptor.categoryServerInstances:
        template = applicationDescriptor.categoryServerTemplates[instance.template]
        cat = template.category
        if cat not in nodesByCategoryDict:
            raise NoMatchingNodesForCategory(cat)
        elif len(nodesByCategoryDict[cat]) == 0:
            raise NoMatchingNodesForCategory

        assignedNode = getServerDeployedInNode(template,
                                    nodesByCategoryDict[cat], allNodesInfoDict)
        instanceDeploymentDict[instance] = assignedNode
        allNodesInfoDict[assignedNode].numberOfServers += 1
    return instanceDeploymentDict

def getInstanceDeployedInNode(template, nodes, allNodesInfoDict):

    rule = getRule(template.allocationRule)

    if rule in constants.SIMPLE_RULES:
        sortedNodes = sortNodes(\
                    nodes, allNodesInfoDict, rule)
    else:
        sortedNodes = sortNodes(\
                    nodes, allNodesInfoDict, None)
    assignedNode = chooseNode(sortedNodes, rule, allNodesInfoDict)
    return assignedNode

def get_instance_name(instance, template):
    name = template.descriptor.id
    pars = re.findall('\${(.*)}', name)
    for par in pars:
        if par in instance.parameterValues:
            name = name.replace('${' + par + '}', instance.parameterValues[par])
        elif par in template.parameterDefaults:
            name.replace('${' + par + '}', template.parameterDefaults[par])
        else:
            name.replace('${' + par + '}', "")
    return name


def getServerDeployedInNodeDict(applicationDescriptor, allNodesInfoDict,
                                 filename):
    """
        Returns a dict with  the assigned node for each server according to
        category: {"server1": "node1", "server2": "node2", ...
    """


    serverDeploymentDict = {}
    if not allNodesInfoDict:
        raise RuntimeError("No nodes available")

    nodesByCategoryDict = getNodesByCategory(allNodesInfoDict, filename)
    for cat in applicationDescriptor.categoryNodes:
        if cat not in nodesByCategoryDict:
            raise NoMatchingNodesForCategory(cat)
        elif len(nodesByCategoryDict[cat]) == 0:
            raise NoMatchingNodesForCategory
        for server in applicationDescriptor.categoryNodes[cat].servers:
            assignedNode = getServerDeployedInNode(server,
                                    nodesByCategoryDict[cat], allNodesInfoDict)
            serverDeploymentDict[server.id] = assignedNode
            allNodesInfoDict[assignedNode].numberOfServers += 1
    return serverDeploymentDict

def getServerDeployedInNode(server, nodes, allNodesInfoDict):

    rule = getRule(server.allocationRule)

    if rule in constants.SIMPLE_RULES:
        sortedNodes = sortNodes(\
                    nodes, allNodesInfoDict, rule)
    else:
        sortedNodes = sortNodes(\
                    nodes, allNodesInfoDict, None)
    assignedNode = chooseNode(sortedNodes, rule, allNodesInfoDict)
    return assignedNode

def chooseNode(sortedNodes, rule, allNodesInfoDict):
    if isinstance(rule, str) or rule == None:
            return sortedNodes[0]

    else:
        for i in sortedNodes:
            if check_rule(rule, allNodesInfoDict[i]):
                return i
    raise NoMatchingNodesInCategoryForRule(rule)


def sortNodes(suitableNodesList, allNodesDict, rule):
    attrib = constants.RULES_DICT[rule]
    sortedNodeNames = suitableNodesList
    if attrib:
        sortedNodeNames = qsort(suitableNodesList, allNodesDict, attrib)
    if rule is not None:
        if (rule[:3] == "max"):
            sortedNodeNames.reverse()
    return sortedNodeNames

def getAttribute(object1, attribList):
    try:
        auxlist = list(attribList)
        if (len(auxlist) == 1):
            return getattr(object1, auxlist[0])
        else:
            attrib1 = auxlist.pop(0)
            return getAttribute(getattr(object1, attrib1), auxlist)
    except AttributeError:
        raise

def qsort(suitableNodesList, allNodesDict, attrib):
    attrib1 = list(attrib)
    attrib2 = list(attrib)
    suitableNodesList.sort(cmp=lambda x,
                           y: cmp(getAttribute(allNodesDict[x], attrib1), \
                              getAttribute(allNodesDict[y], attrib2)))

    return suitableNodesList


def getNodesByCategory(allNodesInfoDict, filename):
    # filename = os.path.join(os.path.dirname(__file__), constants.CATEGORIES_PATH)

    treeApp = etree.parse(filename)

    catlist = treeApp.xpath("/list/category")
    for cat in catlist:
        includes = cat.iterfind("include")
        for inc in includes:
            inc_cat = treeApp.xpath(".//category[@name=" + inc.text + "]")
            for i in inc_cat:
                for feat in i.iterfind("feature"):
                    cat.append(feat)

    catlist = treeApp.xpath("/list/category")
    nodesByCategoryDict = {}
    for cat in catlist:
        nodesInCatList = []
        for node in allNodesInfoDict:
            res = True
            for f in cat.iter("feature"):
                if res:
                    res = checkFeature(f, allNodesInfoDict[node])
            if res == True:
                nodesInCatList.append(node)
        nodesByCategoryDict.update({cat.get("name"):nodesInCatList})
    return nodesByCategoryDict

def checkFeature(feature, node):
    featureId = feature.get("id")
    try:
        attribList = constants.CATEGORIES_FEATURES[featureId]
    except:
        raise exc.InvalidFeature(featureId)

    attrib = getAttribute(node, attribList)
    value = next(feature.iter("value"), None)
    if value is not None:
        if str(attrib) == value.text:
            return True
        else:
            return False
    min_value = next(feature.iter("min_value"), None)
    max_value = next(feature.iter("max_value"), None)

    if (min_value is not None):
        min_value = int(min_value.text)
        if (max_value is None) and (attrib >= min_value):
            return True
    if (max_value is not None):
        max_value = int(max_value.text)
        if min_value is None:
            min_value = -1
        if (attrib >= min_value and attrib < max_value):
            return True
        else:
            return False
    elif (min_value is None):
        for value in feature.iter():
            if attrib == value:
                return True
    return False



def check_rule(rule, node):
    if rule == None:
        return True


    attributesDict = constants.RULES_DICT

    ops = { ">": operator.gt, ">=": operator.ge, '<': operator.lt,
            '<=': operator.le, '==': operator.eq, "gt":operator.gt,
            "lt": operator.lt, "ge" : operator.ge, "le":operator.le,

            '=':operator.eq}

    if isinstance(rule, str):
        rule = rule.replace(" ", "")
        rule = getRule(rule)
    try:
        op1 = getAttribute(node, attributesDict[rule[0]])
        if rule[0] in constants.SIMPLE_RULES:
            op2 = constants.SIMPLE_RULES[rule[0]]
        else:
            op2 = rule[1]
        result = ops[op2](op1, int(rule[2]))
    except Exception, e:
        raise InvalidRule(rule, e)

    return result

def getRule(strRule):
    if strRule is not None:
        strRule = strRule.replace(" ", "")
    if strRule in constants.SIMPLE_RULES:
        return strRule
    if len(strRule.split('le')) == 2:
        return strRule.partition('le')
    if len(strRule.split('ge')) == 2:
        return strRule.partition('ge')
    if len(strRule.split('lt')) == 2:
        return strRule.partition('lt')
    if len(strRule.split('gt')) == 2:
        return strRule.partition('gt')
    if len(strRule.split('>=')) == 2:
        return strRule.partition('>=')
    elif  len(strRule.split('<=')) == 2:
        return strRule.partition('<=')
    elif  len(strRule.split('==')) == 2:
        return strRule.partition('==')
    elif  len(strRule.split('<')) == 2:
        return strRule.partition('<')
    elif  len(strRule.split('>')) == 2:
        return strRule.partition('>')
    elif  len(strRule.split('=')) == 2:
        return strRule.partition('=')
    else :
        raise InvalidRule(strRule)
