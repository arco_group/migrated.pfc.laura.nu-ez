#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
import os
slice_file = os.path.join(os.path.dirname(__file__), "BlenderApp.ice")
Ice.loadSlice(slice_file)
import BlenderApp  # @UnresolvedImport
import re

class DelivererI(BlenderApp.Deliverer):
    def  __init__(self, filename):

        self.startFrame, self.endFrame = self.getFrames(filename)
        self.renderedFrames = []
        self.count = 0
        for i in range(self.startFrame, self.endFrame + 1):
            self.renderedFrames.append(False)


    def write(self, name, offset, bytes, numberOfFrame, current=None):
        if not self.renderedFrames[numberOfFrame - self.startFrame]:
            if not os.path.isdir("results"):
                os.makedirs("results")
            f = open("results/" + name, 'a+b')
            f.seek(offset)
            f.write(bytes)
            f.close()

    def endedFrame(self, numberOfFrame, current=None):
        self.renderedFrames[numberOfFrame - self.startFrame] = True

    def getFrameToRender(self, current=None):
        frameToRender = None
        end = False
        in_frame = self.count % (self.endFrame - self.startFrame + 1)
        for i in range(self.endFrame - self.startFrame + 1):
            frame = self.count % (self.endFrame - self.startFrame + 1)
            if not end:
                if not self.renderedFrames[frame] :
                    end = True
                    frameToRender = frame + 1
                self.count = self.count + 1

        if frameToRender == None:
            frameToRender = -1

#         for nframe, val in enumerate(self.renderedFrames):
#             if not val and self.count < 1:
#                 frameToRender = nframe + self.startFrame
#                 self.count = self.count + 1
#         if not frameToRender:
#             frameToRender = -1
        return frameToRender

    def getFrames(self, filename):
#         strFrames = ""
#         script = os.path.join(os.path.dirname(__file__), "script.py")
#         try:
#             strFrames = subprocess.call(
#                 'blender -b ' + file_name + ' -P  ' + \
#                 script + '> frames.info', shell=True)
#         except subprocess.CalledProcessError, e:
#             strFrames = str(e.output)
        f = open(filename)
        strFrames = f.read()
        f.close()

        startFrame = int(re.findall('Start frame:\s*(\d+)',
                            strFrames)[0])
        endFrame = int(re.findall('End frame:\s*(\d+)',
                            strFrames)[0])


        return startFrame, endFrame


class DelivererApp(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = DelivererI(argv[1])

        adapter = broker.createObjectAdapter("DelivererAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("deliverer"))

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

if __name__ == '__main__':

    deliverer = DelivererApp()
    sys.exit(deliverer.main(sys.argv))


