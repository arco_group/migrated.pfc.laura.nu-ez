#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import Ice
Ice.loadSlice('BlenderApp.ice')
import BlenderApp
import subprocess
import  sys
import os

class rendererApp(Ice.Application):
    def __init__(self, argv):
        self.filename = argv[1]
        self.dst = argv[2] + "_" + argv[3]
        if not os.path.isdir(self.dst):
            os.makedirs(self.dst)

    def run(self, argv):
        proxy = self.communicator().stringToProxy("deliverer")
        deliverer = BlenderApp.DelivererPrx.checkedCast(proxy)
        if not deliverer:
            raise RuntimeError('Invalid proxy, no deliverer found')
        frame = deliverer.getFrameToRender()

        while frame >= 0:
            res = subprocess.check_call('blender -b ' + self.filename + \
                ' -x 1 -F PNG -o ' + self.dst + '/ -s ' + str(frame) + ' -e ' + \
                str(frame) + ' -a', shell=True)
            outputfile = ("%04d" % frame) + '.png'

            self.send(self.dst, outputfile, deliverer, frame)
            frame = deliverer.getFrameToRender()


        return 0


    def send(self, dst, filename, deliverer, frame):
        file = open(dst + "/" + filename)
        ft = deliverer
        chunkSize = 800000
        offset = 0

        bytes = file.read(chunkSize)  # Read a chunk

        while len(bytes) > 0:
            ft.write(filename, offset, bytes, frame)
            offset += len(bytes)

            bytes = file.read(chunkSize)  # Read a chunk

        deliverer.endedFrame(frame)


if __name__ == '__main__':
    sys.exit(rendererApp(sys.argv).main(sys.argv))
