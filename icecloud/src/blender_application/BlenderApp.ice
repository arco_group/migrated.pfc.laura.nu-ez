module BlenderApp{

    sequence<byte> ByteSeq;    
    
    interface Deliverer   
    {
        void write(string name, int offset, ByteSeq bytes, int numberOfFrame);
        void endedFrame(int numberOfFrame);
        int getFrameToRender();
    };
};