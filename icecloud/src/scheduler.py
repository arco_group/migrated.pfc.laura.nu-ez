import deployment_utilities
import IceGrid
from assigned_server import AssignedServer, AssignedInstance
import copy
from ig_descriptor_builder import IGDescriptorBuilder
from exc import NotDecreasableServer, NotDecreasableTemplate
import Ice
import IceStorm
import os

slice_dir = os.path.join(os.path.dirname(__file__), "slice")
ic_slice = os.path.join(slice_dir, "IceCloud.ice")

Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + '-I' + slice_dir + \
              ' --all ' + ic_slice)

import IceCloud
import threading


class SchedulerPublisher():
    def __init__(self, communicator):

        self.communicator = communicator
        topic_mgr = self.get_topic_manager()
        if not topic_mgr:
            return 2

        topic_name = "SchedulerTopic"
        try:
            topic = topic_mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:  # @UndefinedVariable
            print "no such topic found, creating"
            topic = topic_mgr.create(topic_name)

        publisher = topic.getPublisher()
        self.supervisor = IceCloud.SupervisorPrx.uncheckedCast(publisher)

    def get_topic_manager(self):
        proxy = self.communicator.stringToProxy("IceCloudApp.IceStorm/TopicManager")
        if proxy is None:
            return None

        return IceStorm.TopicManagerPrx.checkedCast(proxy)  # @UndefinedVariable

    def sendEvent(self, msg):
        self.supervisor.report(msg)

    def run(self, argv=None):
        self.communicator.waitForShutdown()
        return 0

class Scheduler():
    def __init__(self, grid_state=None):
        self.grid_state = grid_state
        self.application_updates = {}
        self.application_instances = {}
        self.logger = None
        self.communicator = None
        self.publisher = None

    def set_grid_state(self, state):
        self.application_updates = {}

        self.grid_state = state

    def set_category_file(self, filename):
        self.categories_file = filename

    def set_logger(self, logger):
        self.logger = logger

    def set_session_admin(self, ig_session_admin):
        self.ig_session_admin = ig_session_admin

    def set_publisher(self, communicator):
        self.communicator = communicator
        try:
            self.createPublisher(communicator)
        except Exception, e:
            self.logger.warning("Couldn't create publisher " + str(communicator) + e.message)

    def createPublisher(self, communicator):
        self.publisher = SchedulerPublisher(communicator)


    def commit(self, application):
        if application in self.application_updates:

            self.ig_session_admin.updateApplication(
                                        self.application_updates[application])

            for node in self.application_updates[application].nodes:
                for server in node.servers:
                    try:
                        self.ig_session_admin.patchServer(server.id, False)
                    except IceGrid.PatchException:  # @UndefinedVariable
                        self.logger.warning("Couldn't complete the patch for "
                                            "server " + server.id)
                    self.ig_session_admin.startServer(server.id)
            if application in self.application_instances:
                for instance in self.application_instances[application]:
                    self.get_grid_state().add_template_instance(instance.template,
                                                           instance)
                    try:
                        self.ig_session_admin.patchServer(instance.name, False)
                    except IceGrid.PatchException:  # @UndefinedVariable
                        self.logger.warning("Couldn't complete the patch for "
                                                "server " + server.id)
                    self.ig_session_admin.startServer(instance.name)
                self.application_instances.pop(application)
            self.application_updates.pop(application)
            if(self.publisher):
                self.publisher.sendEvent("Application updated : " + application)


    def grow_server(self, assignedServer, node_infos, user=None):
        maxDupes = assignedServer.maxDupes
        if user is not None:
            maxDupes = None
        if (maxDupes is None or
            (len(self.get_grid_state().get_secondary_servers(assignedServer.id))\
             < assignedServer.maxDupes)):
            server_descriptor = IceGrid.ServerDescriptor()  # @UndefinedVariable
            nodesByCategoryDict = deployment_utilities.getNodesByCategory(
                                            node_infos, self.categories_file)
            assignedNode = deployment_utilities.getServerDeployedInNode(\
                assignedServer, nodesByCategoryDict[assignedServer.category],
                node_infos)

            new_id = assignedServer.id + '.' + str(len(\
                self.get_grid_state().get_secondary_servers(assignedServer.id)))
            newAssignedServer = AssignedServer(assignedServer.application,
                new_id, assignedServer.category)
            newAssignedServer.allocationRule = assignedServer.allocationRule
            newAssignedServer.growingRule = assignedServer.growingRule
            newAssignedServer.decreasingRule = assignedServer.decreasingRule
            newAssignedServer.original = False
            newAssignedServer.node = assignedNode
            node_infos[assignedNode].numberOfServers += 1

            self.get_grid_state().add_secondary_server(assignedServer.id,
                                                       newAssignedServer)




            server_descriptor
            categoryNodesDict = self.get_grid_state().get_application(
                    assignedServer.application).categoryNodes
            for categoryNode in categoryNodesDict:
                for server in categoryNodesDict[categoryNode].servers:
                    if server.id == assignedServer.id:
                        server_descriptor = copy.deepcopy(server)
            server_descriptor.id = new_id


            application_name = assignedServer.application
            if application_name not in self.application_updates:
                app = self.createApplicationUpdateDescriptor(application_name)
                self.application_updates[application_name] = app

            app = self.application_updates[application_name]

            node_in_app = False
            node = None
            for n in app.nodes:
                if n.name == newAssignedServer.node:
                    node = n
                    node_in_app = True

            if not node_in_app:
                node = self.createNodeUpdateDescriptor(newAssignedServer.node)
                self.application_updates[application_name].nodes.append(node)


            node.servers.append(IGDescriptorBuilder().getCommunicatorDescriptor(
                                                            server_descriptor))
            self.logger.info("Duplicated server " + assignedServer.id + \
                             " as " + server_descriptor.id + " in node " + \
                             newAssignedServer.node)

    def decrease_server(self, originalServerId, secondaryServer=None):
        secondary_servers = self.get_grid_state().get_secondary_servers(
                                                            originalServerId)

        if len (secondary_servers) == 0:
            raise NotDecreasableServer

        else:
            if secondaryServer == None:
                deletedServer = secondary_servers.pop()

            else:
                deletedServer = secondary_servers.pop(secondary_servers.index(
                                                            secondaryServer))
        if len(secondary_servers) == 0:
            self.get_grid_state().get_secondary_servers().pop(originalServerId)
        application_name = deletedServer.application
        if application_name not in self.application_updates:
            app = self.createApplicationUpdateDescriptor(application_name)
            self.application_updates[application_name] = app

        app = self.application_updates[application_name]
        node_in_app = False
        node = None
        for n in app.nodes:
            if n.name == deletedServer.node:
                node_in_app = True
                node = n


        if not node_in_app:
            node = self.createNodeUpdateDescriptor(deletedServer.node)
            self.application_updates[application_name].nodes.append(node)


        node.removeServers.append(deletedServer.id)
        self.logger.info("Removed " + originalServerId + " dupe with id " + \
                         deletedServer.id + " from node " + deletedServer.node)

    def grow_template(self, template_name, node_infos, instance, user=None):

        grid_state = self.get_grid_state()
        template = grid_state.get_application(instance.application).\
                categoryServerTemplates[template_name]
        maxInstances = template.maxInstances
        if user is not None:
            maxInstances = None
        if (maxInstances is None or
            (len(grid_state.get_template_instances()[template_name]) <
             maxInstances)):
            server_instance = IceGrid.ServerInstanceDescriptor()  # @UndefinedVariable
            nodesByCategoryDict = deployment_utilities.getNodesByCategory(
                                            node_infos, self.categories_file)
            assignedNode = deployment_utilities.getInstanceDeployedInNode(\
                template, nodesByCategoryDict[template.category],
                node_infos)
            server_instance.template = template_name
            server_instance.parameterValues = {}
            count = len(grid_state.get_template_instances()[template_name])
            server_instance.parameterValues['servercount'] = str(count)
            name = deployment_utilities.get_instance_name(server_instance, template)
            newAssignedInstance = AssignedInstance (instance.application, count,
                template_name, name, rule=None, growing=None,
                 decreasing=None, node=None, original=True)

            newAssignedInstance.allocationRule = template.allocationRule
            newAssignedInstance.growingRule = template.growingRule
            newAssignedInstance.decreasingRule = template.decreasingRule
            newAssignedInstance.original = False
            newAssignedInstance.node = assignedNode
            node_infos[assignedNode].numberOfServers += 1

            application_name = instance.application
            if application_name not in self.application_updates:
                app = self.createApplicationUpdateDescriptor(application_name)
                self.application_updates[application_name] = app

            if application_name not in self.application_instances:
                self.application_instances[application_name] = []
            self.application_instances[application_name].append(newAssignedInstance)
            app = self.application_updates[application_name]

            node_in_app = False
            node = None
            for n in app.nodes:
                if n.name == newAssignedInstance.node:
                    node = n
                    node_in_app = True

            if not node_in_app:
                node = self.createNodeUpdateDescriptor(newAssignedInstance.node)
                self.application_updates[application_name].nodes.append(node)

            node.serverInstances.append(server_instance)
            self.logger.info("Instantiated server from template " +
                             template_name + " with servercount = " +
                             str(count) + " in node " + newAssignedInstance.node)

    def decrease_template(self, template, server_instance):
        removed = False
        if not server_instance.original:
            removed_instance = server_instance
            removed = True
        else:
            for instance in self.get_grid_state().get_template_instances()\
                                                    [server_instance.template]:
                if not instance.original and not removed:
                    removed_instance = instance
                    removed = True
        if removed:
            self.get_grid_state().get_template_instances()\
                [server_instance.template].remove(removed_instance)
        else:
            raise NotDecreasableTemplate

        application_name = removed_instance.application
        if application_name not in self.application_updates:
            app = self.createApplicationUpdateDescriptor(application_name)
            self.application_updates[application_name] = app

        app = self.application_updates[application_name]
        node_in_app = False
        node = None
        for n in app.nodes:
            if n.name == removed_instance.node:
                node_in_app = True
                node = n


        if not node_in_app:
            node = self.createNodeUpdateDescriptor(removed_instance.node)
            self.application_updates[application_name].nodes.append(node)

        node.removeServers.append(removed_instance.name)
#         self.get_grid_state().remove_template_instance(removed_instance.template,
#                                                        removed_instance)
        self.logger.info("Removed " + removed_instance.template + \
            " instance with id " + removed_instance.name + " from node " + \
            removed_instance.node)

    def createApplicationUpdateDescriptor(self, name):
        app = IceGrid.ApplicationUpdateDescriptor()  # @UndefinedVariable
        app.name = name
        app.variables = {}
        app.removeVariables = []
        app.propertySets = {}
        app.removePropertySets = []
        app.replicaGroups = []
        app.removeReplicaGroups = []
        app.serverTemplates = {}
        app.removeServerTemplates = []
        app.serviceTemplates = {}
        app.removeServiceTemplates = []
        app.nodes = []
        app.removeNodes = []
        return app

    def createNodeUpdateDescriptor(self, name):
        node = IceGrid.NodeUpdateDescriptor()  # @UndefinedVariable
        node.name = name
        node.variables = {}
        node.removeVariables = []
        node.propertySets = {}
        node.removePropertySets = []
        node.serverInstances = []
        node.servers = []
        node.removeServers = []

        return node

    def get_grid_state(self):
        return self.grid_state


