#!/usr/bin/python
# -*- coding: utf-8 -*-

from lxml import etree
import Ice
import os
ic_slice = os.path.join(os.path.dirname(__file__), "slice/ICDescriptor.ice")
Ice.loadSlice("-I" + Ice.getSliceDir() + ' ' + ic_slice)
import IceCloud
import IceGrid
from exc import InvalidXMLTemplate

class ICDescriptorBuilder():

    def __init__(self):
        self.serverTemplates = {}
        self.serviceTemplates = {}

    def setTemplates(self, nodeApp):
        self.serverTemplates = \
            self.getTemplateDescriptorDict(nodeApp, "server-template")
        self.serviceTemplates = \
            self.getTemplateDescriptorDict(nodeApp, "service-template")
        self.categoryServerTemplates = \
            self.getTemplateDescriptorDict(nodeApp, "category-server-template")

    def getApplicationDescriptor(self, filename):
        try:
            treeApp = etree.parse(filename)
        except Exception, e:
            raise InvalidXMLTemplate("Invalid file ", e)
        applicationDescriptor = IceCloud.ApplicationDescriptor()  # @UndefinedVariable
        root = treeApp.getroot()
        if not root.tag == "icecloud":
            raise InvalidXMLTemplate("Fist tag must be <icecloud>")
        nodeApp = root.find("application")
        if nodeApp is None:
            raise InvalidXMLTemplate("No application found ")
        self.setTemplates(nodeApp)
        applicationDescriptor.name = nodeApp.attrib.get("name")
        if applicationDescriptor.name is None:
            raise InvalidXMLTemplate("Application must have a name ")
        applicationDescriptor.variables = self.getVariables(nodeApp)
        applicationDescriptor.replicaGroups = self.getReplicaGroups(nodeApp)
        applicationDescriptor.serverTemplates = \
         self.getTemplateDescriptorDict(nodeApp, "server-template")
        applicationDescriptor.serviceTemplates = \
         self.getTemplateDescriptorDict(nodeApp, "service-template")
        applicationDescriptor.categoryServerTemplates = \
         self.getTemplateDescriptorDict(nodeApp, "category-server-template")
        applicationDescriptor.nodes = self.getNodeDescriptorDict(nodeApp, "name",
                                applicationDescriptor.serverTemplates,
                                applicationDescriptor.categoryServerTemplates)
        applicationDescriptor.categoryNodes = \
         self.getNodeDescriptorDict(nodeApp, "category",
                                    applicationDescriptor.serverTemplates,
                                applicationDescriptor.categoryServerTemplates)
        applicationDescriptor.categoryServerInstances = \
            self.getServerInstanceSeq(nodeApp,
                                      applicationDescriptor.serverTemplates,
                                applicationDescriptor.categoryServerTemplates)
        applicationDescriptor.distrib = self.getDistributionDescriptor(nodeApp)
        if  nodeApp.find("description") is not None:
            applicationDescriptor.description = nodeApp.find("description").text
        applicationDescriptor.propertySets = self.getPropertySetDict(nodeApp)
        return applicationDescriptor


    def getVariables(self, node):
        variables = {}
        for var in node.iterfind("variable"):
            variables[var.get("name")] = var.get("value")
        return variables

    def getReplicaGroups(self, node):
        replicaGroups = []
        for rep in node.iterfind("replica-group"):
            replicaGroups.append(self.getReplicaGroupDescriptor(rep))
        return replicaGroups

    def getReplicaGroupDescriptor(self, rep):
        replicaGroupDesc = IceGrid.ReplicaGroupDescriptor()  # @UndefinedVariable
        replicaGroupDesc.id = rep.get("id")
        replicaGroupDesc.loadBalancing = \
            self.getLoadBalancingPolicy(rep.find("load-balancing"))
        replicaGroupDesc.proxyOptions = rep.get("proxy-options")
        if rep.find("description") is not None:
            replicaGroupDesc.description = rep.find("description").text
        replicaGroupDesc.objects = self.getObjectDescriptorSeq(rep, "object")
        return replicaGroupDesc

    def getLoadBalancingPolicy(self, load):
        ltype = load.get("type")
        if ltype == "random":
            loadBalancingPol = IceGrid.RandomLoadBalancingPolicy()  # @UndefinedVariable
        elif ltype == "ordered":
            loadBalancingPol = IceGrid.OrderedLoadBalancingPolicy()  # @UndefinedVariable
        elif ltype == "round-robin":
            loadBalancingPol = IceGrid.RoundRobinLoadBalancingPolicy()  # @UndefinedVariable
        elif ltype == "adaptive":
            loadBalancingPol = IceGrid.AdaptiveLoadBalancingPolicy()  # @UndefinedVariable
            loadBalancingPol.loadSample = load.get("load-sample")
        else:
            raise InvalidXMLTemplate("Invalid load balancing policy " + ltype +
                                     ". Line " + str(load.sourceline))
        loadBalancingPol.nReplicas = load.get("n-replicas")
        return loadBalancingPol

    def getObjectDescriptorSeq(self, node, otype):
        objectSeq = []
        for obj in node.iterfind(otype):
            objectSeq.append(self.getObjectDescriptor(obj))
        return objectSeq

    def getObjectDescriptor(self, obj):
        objectDesc = IceGrid.ObjectDescriptor()  # @UndefinedVariable
        try:
            category, name = obj.get("identity").split("/")
        except:
            category = ""
            name = obj.get("identity")
        iden = Ice.Identity()  # @UndefinedVariable
        iden.name = name
        iden.category = category
        objectDesc.id = iden
        objectDesc.type = obj.get("type")
        if (obj.get("proxy-options") is not None):
            objectDesc.proxyOptions = obj.get("proxy-options")
        return objectDesc

    def getTemplateDescriptorDict(self, node, serverType):
        templateDescriptorDict = {}
        if serverType == "server-template" or serverType == \
                                                    "category-server-template":
            for tem in node.iterfind(serverType):
                if (tem.find("icebox") is not None):
                    if tem.get("id") is None:
                        raise InvalidXMLTemplate ("Required server-template id ")
                    templateDescriptorDict[tem.get("id")] = \
                        self.getTemplateDescriptor(tem, "icebox")
                else:
                    if tem.get("id") is None:
                        raise InvalidXMLTemplate ("Required server-template id ")
                    if serverType == "server-template":
                        templateDescriptorDict[tem.get("id")] = \
                            self.getTemplateDescriptor(tem, "server")
                    else:
                        templateDescriptorDict[tem.get("id")] = \
                            self.getCategoryTemplateDescriptor(tem)
            if serverType == "category-server-template":
                for template_id in templateDescriptorDict:
                    template = templateDescriptorDict[template_id]
                    template.descriptor.id += '${servercount}'
                    template.parameters.append('servercount')
        else:
            for tem in node.iterfind(serverType):
                templateDescriptorDict[tem.get("id")] = \
                self.getTemplateDescriptor(tem, "service")
        return templateDescriptorDict

    def getTemplateDescriptor(self, tem, serverType):
        templateDescriptor = IceGrid.TemplateDescriptor()  # @UndefinedVariable
        if tem.find("icebox")  is not None:
            templateDescriptor.descriptor = self.getCommunicatorDescriptor(\
                    tem.find("icebox"), "icebox", "name")
        else:
            templateDescriptor.descriptor = self.getCommunicatorDescriptor(\
                    tem.find(serverType), serverType, "name")
        templateDescriptor.parameters = self.getParametersSeq(tem)
        templateDescriptor.parameterDefaults = self.getParameterDefaultsDict(tem)
        return templateDescriptor

    def getCategoryTemplateDescriptor(self, tem):
        templateDescriptor = IceCloud.CategoryTemplateDescriptor()  # @UndefinedVariable
        templateDescriptor.descriptor = self.getCommunicatorDescriptor(\
                    tem.find("server"), "server", "name")
        templateDescriptor.parameters = self.getParametersSeq(tem)
        templateDescriptor.parameterDefaults = self.getParameterDefaultsDict(tem)
        templateDescriptor.category = tem.get("category")
        if templateDescriptor.category is None:
            raise  InvalidXMLTemplate("Category template shoud have a " + \
                            "category. Line " + str(tem.sourceline))
        templateDescriptor.allocationRule = tem.get("allocationRule")
        templateDescriptor.growingRule = tem.get("growingRule")
        templateDescriptor.decreasingRule = tem.get("decreasingRule")
        instances = tem.get("maxInstances")
        if instances is not None:
            instances = int(instances)
            templateDescriptor.maxInstances = instances
        else:
            templateDescriptor.maxInstances = 30000
        return templateDescriptor

    def getServiceInstanceDescriptorSeq(self, server):
        serviceInstanceDescriptorSeq = []
        for service in server.iterfind("service-instance"):
            serviceInstanceDescriptorSeq.append(\
                                self.getServiceInstanceDescriptor(service))

        return serviceInstanceDescriptorSeq


    def getServiceInstanceDescriptor(self, service):
        serviceInstanceDescriptor = IceGrid.ServiceInstanceDescriptor()  # @UndefinedVariable
        serviceInstanceDescriptor.descriptor = \
            self.getCommunicatorDescriptor(service, "service", "name")

        serviceInstanceDescriptor.template = service.get("template")
        serviceInstanceDescriptor.parameterValues = \
            self.getParameterValuesDict(service, \
                                serviceInstanceDescriptor.template, "service")
        serviceInstanceDescriptor.propertySet = self.getPropertySet(service)
        return serviceInstanceDescriptor


    def getCommunicatorDescriptor(self, server, serverType, nodeType):
        communicatorDescriptor = IceGrid.CommunicatorDescriptor()  # @UndefinedVariable
        if serverType == "server" or serverType == "icebox":
            if nodeType == "category":
                communicatorDescriptor = IceCloud.ServerDescriptor()  # @UndefinedVariable
            else:
                communicatorDescriptor = IceGrid.ServerDescriptor()  # @UndefinedVariable
            if serverType == "icebox":
                communicatorDescriptor = IceGrid.IceBoxDescriptor()  # @UndefinedVariable
                services = self.getServiceInstanceDescriptorSeq(server)
                communicatorDescriptor.services = services

            communicatorDescriptor.id = server.get("id")
            if communicatorDescriptor.id is None:
                raise InvalidXMLTemplate("Required server id. Line " +
                                         str(server.sourceline))
            if nodeType == "category":
                communicatorDescriptor.allocationRule = server.get("allocationRule")
                communicatorDescriptor.growingRule = server.get("growingRule")
                communicatorDescriptor.decreasingRule = server.get("decreasingRule")
                dupes = server.get("maxDupes")
                if dupes is not None:
                    dupes = int(dupes)
                    communicatorDescriptor.maxDupes = dupes
                else:
                    communicatorDescriptor.maxDupes = 30000
            communicatorDescriptor.exe = server.get("exe")
            if communicatorDescriptor.exe is None:
                raise InvalidXMLTemplate(communicatorDescriptor.id + \
                ': Required path to executable. Line ' + str(server.sourceline))

            iceVersion = server.get("ice-version")
            if iceVersion is not None:
                communicatorDescriptor.iceVersion = iceVersion
            pwd = server.get("pwd")
            if pwd is not None:
                communicatorDescriptor.pwd = pwd
            communicatorDescriptor.options = self.getOptions(server)
            communicatorDescriptor.envs = self.getEnvs(server)
            activation = server.get("activation")
            if activation is not None:
                communicatorDescriptor.activation = activation
            timeout = server.get("activation-timeout")
            if timeout is not None:
                communicatorDescriptor.activationTimeout = timeout
            deactivationTimeout = server.get("deactivation-timeout")
            if deactivationTimeout is not None:
                communicatorDescriptor.deactivationTimeout = deactivationTimeout
            communicatorDescriptor.applicationDistrib = \
             False  if (server.get("application-distrib") == "false") else True
            communicatorDescriptor.distrib = \
                self.getDistributionDescriptor(server)
            communicatorDescriptor.allocatable = \
             True  if (server.get("allocatable") == "true") else False
            user = server.get("user")
            if user is not None:
                communicatorDescriptor.user = user


        elif serverType == "service":
            communicatorDescriptor = IceGrid.ServiceDescriptor()  # @UndefinedVariable
            if server.get("name") is not None:
                communicatorDescriptor.name = server.get("name")
            if server.get("entry") is not None:
                communicatorDescriptor.entry = server.get("entry")


        communicatorDescriptor.adapters, endpoints = self.getAdapterSeq(server)
        communicatorDescriptor.propertySet = \
             self.getCommunicatorPropertySet(server)
        for i, adapter in enumerate(communicatorDescriptor.adapters):
            adapterName = adapter.name
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = adapter.name + ".Endpoints"
            property.value = endpoints[i]
            communicatorDescriptor.propertySet.properties.append(property)


        communicatorDescriptor.dbEnvs = self.getDbEnvSeq(server)
        communicatorDescriptor.logs = self.getLogs(server, communicatorDescriptor.propertySet)
        if server.find("description")is not None:
            communicatorDescriptor.description = server.find("description").text
        return communicatorDescriptor

    def getLogs(self, server, propertySet):
        logs = []
        for log in server.iterfind("log"):
            path = log.get("path")[:]
            prop = log.get("property")[:]
            logs.append(path)
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = prop[:]
            property.value = path[:]
            propertySet.properties.append(property)
        return logs


    def getAdapterSeq(self, ser):
        adapterDescriptorSeq = []
        endpoints = []
        for adapter in ser.iterfind("adapter"):
            adapterDescriptorSeq.append(self.getAdapterDescriptor(adapter))
            endpoints.append(adapter.get("endpoints"))

        return adapterDescriptorSeq, endpoints

    def getAdapterDescriptor(self, adapter):
        adapterDescriptor = IceGrid.AdapterDescriptor()  # @UndefinedVariable
        adapterDescriptor.name = adapter.get("name")
        if adapterDescriptor.name is None:
            raise InvalidXMLTemplate('Adapter must have a name')
        if adapter.find("description") is not None:
            adapterDescriptor.description = adapter.find("description").text
        adapterDescriptor.id = adapter.get("id")
        if adapterDescriptor.id is None:
            raise InvalidXMLTemplate('Adapter must have an id')
        if adapter.get("replica-group") is not None:
            adapterDescriptor.replicaGroupId = adapter.get("replica-group")
        if adapter.get("priority") is not None:
            adapterDescriptor.priority = adapter.get("priority")
        adapterDescriptor.registerProcess = \
         True  if (adapter.get("register-process") == "true") else False
        adapterDescriptor.serverLifetime = \
         False  if (adapter.get("server-lifetime") == "false") else True
        adapterDescriptor.objects = \
         self.getObjectDescriptorSeq(adapter, "object")
        adapterDescriptor.allocatables = \
         self.getObjectDescriptorSeq(adapter, "allocatable")
        return adapterDescriptor

    def getParametersSeq(self, node):
        parametersSeq = []
        for par in node.iterfind("parameter"):
            parametersSeq.append(par.get("name"))
        return parametersSeq

    def getParameterDefaultsDict(self, node):
        parametersDict = {}
        for par in node.iterfind("parameter"):
            if par.get("default") is not None:
                parametersDict[par.get("name")] = par.get("default")
        return parametersDict

    def getPropertySet(self, node):
        propertySet = IceGrid.PropertySetDescriptor()  # @UndefinedVariable
        propertySet.references = self.getReferences(node)
        propertySet.properties = self.getProperties(node)
        return propertySet

    def getReferences(self, node):
        references = []
        for refid in node.iterfind("properties[@refid]"):
            references.append(refid.get("refid"))
        return references

    def getProperties(self, node):
        properties = []
        for p in node.iterfind("property"):
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = p.get("name")
            property.value = p.get("value")
            properties.append(property)
        return properties

    def getDbProperties(self, dbEnv):
        properties = []
        for p in dbEnv.iterfind("dbproperty"):
            property = IceGrid.PropertyDescriptor()  # @UndefinedVariable
            property.name = p.get("name")
            property.value = p.get("value")
            properties.append(property)
        return properties

    def getPropertySetDict(self, node):
        propertySetDict = {}
        for prop in node.iterfind("properties[@id]"):
            propertySetDict[prop.get("id")] = self.getPropertySet(prop)
        return propertySetDict

    def getCommunicatorPropertySet(self, node):
        aux = node.find("properties")
        propertySet = IceGrid.PropertySetDescriptor()  # @UndefinedVariable
        propertySet.references = []
        propertySet.properties = []
        if aux is not None:
            propertySet = self.getPropertySet(aux)
        return propertySet

    def getDbEnvSeq(self, node):
        dbEnvSeq = []
        for dbEnv in node.iterfind("dbenv"):
            db = IceGrid.DbEnvDescriptor()  # @UndefinedVariable
            db.name = dbEnv.get("name")
            if  dbEnv.find("description") is not None:
                db.description = dbEnv.find("description").text
            if dbEnv.get("home") is not None:
                db.dbHome = dbEnv.get("home")
            db.properties = self.getDbProperties(dbEnv)
            dbEnvSeq.append(db)
        return dbEnvSeq

    def getEnvs(self, node):
        envs = []
        for env in node.iterfind("env"):
            envs.append(env.text)
        return envs

    def getOptions(self, node):
        options = []
        for op in node.iterfind("option"):
            options.append(op.text)
        return options

    def getDistributionDescriptor(self, node):
        distributionDescriptor = IceGrid.DistributionDescriptor()  # @UndefinedVariable
        distrib = node.find("distrib")
        if distrib is not None:
            distributionDescriptor.icepatch = distrib.get("icepatch")
            directories = []
            for d in distrib.iterfind("directory"):
                directories.append(d.text)
            distributionDescriptor.directories = directories
        return distributionDescriptor

    def getParameterValuesDict(self, instance, template, template_type):
        parameterValues = {}
        templates = {}
        if template_type == "server":
            templates = self.serverTemplates
        else:
            templates = self.serviceTemplates
        for key in templates:
            if key == instance.get("template"):
                templateParameters = list(templates[key].parameters)
                for templateParameter in templateParameters:
                        parameter = instance.get(templateParameter)
                        if parameter is not None:
                            parameterValues[templateParameter] = parameter[:]
        return parameterValues

    def getServicePropertySets(self, serverInstance):
        propertySetDict = {}
        for prop in serverInstance.iterfind("properties[@service]"):
            propertySetDict[prop.get("service")] = self.getPropertySet(prop)
        return propertySetDict

    def getServerInstanceDescriptor(self, serverInstance, templates,
                                    category_templates):
        serverInstanceDescriptor = IceGrid.ServerInstanceDescriptor()  # @UndefinedVariable
        serverInstanceDescriptor.template = serverInstance.get("template")
        if serverInstanceDescriptor.template in templates:
            template = templates[serverInstanceDescriptor.template]
        else:
            try:
                template = category_templates[serverInstanceDescriptor.template]
            except:
                raise InvalidXMLTemplate(serverInstanceDescriptor.template +
                        " not defined. Line " + str(serverInstance.sourceline))
        serverInstanceDescriptor.parameterValues = {}
        for parameter in template.parameters:
            newVal = serverInstance.get(parameter)
            if newVal:
                serverInstanceDescriptor.parameterValues[parameter] = newVal
        serverInstanceDescriptor.propertySet = \
         self.getPropertySet(serverInstance)
        serverInstanceDescriptor.servicePropertySets = \
            self.getServicePropertySets(serverInstance)
        return serverInstanceDescriptor


    def getServerInstanceSeq(self, node, templates, category_templates):
        serverInstanceSeq = []
        for serverInstance in node.iterfind("server-instance"):
            serverInstanceSeq.append(\
                            self.getServerInstanceDescriptor(serverInstance,
                                 templates, category_templates))
#
#         for template in category_templates:
#             count = 0
#             for instance in serverInstanceSeq:
#                 if template == instance.template:
#                     instance.parameterValues["servercount"] = str(count)
#                     count += 1

        return serverInstanceSeq


    def getServerDescriptorSeq(self, node, nodeType):
        serverDescriptorSeq = []
        for server in node.iterfind("server"):
            serverDescriptor = self.getCommunicatorDescriptor(server, "server",
                                                              nodeType)
            serverDescriptorSeq.append(serverDescriptor)
        for server in node.iterfind("icebox"):
            serverDescriptor = self.getCommunicatorDescriptor(server, "icebox",
                                                              nodeType)
            serverDescriptorSeq.append(serverDescriptor)
        return serverDescriptorSeq


    def getNodeDescriptor(self, node, templates, category_templates, nodeType):
        if nodeType == "category":
            nodeDescriptor = IceCloud.NodeDescriptor()  # @UndefinedVariable
        else:
            nodeDescriptor = IceGrid.NodeDescriptor()  # @UndefinedVariable
            nodeDescriptor.variables = self.getVariables(node)
            loadFactor = node.get("load-factor")
            nodeDescriptor.propertySets = self.getPropertySetDict(node)
            if loadFactor is not None:
                nodeDescriptor.loadFactor = loadFactor

        nodeDescriptor.serverInstances = self.getServerInstanceSeq(
                                        node, templates, category_templates)
        if  node.find("description") is not None:
            nodeDescriptor.description = node.find("description").text
        nodeDescriptor.servers = self.getServerDescriptorSeq(node, nodeType)
        return nodeDescriptor


    def getNodeDescriptorDict(self, app, nodeType, templates, category_templates):
        nodeDescriptorDict = {}
        for node in app.iterfind("node[@" + nodeType + "]"):
            if node.get(nodeType) is None:
                raise InvalidXMLTemplate("Node must have a " + nodeType + \
                                         '. Line ' + str(node.sourceLine))
            nodeDescriptorDict[node.get(nodeType)] = self.getNodeDescriptor(node,
                                    templates, category_templates, nodeType)
        return nodeDescriptorDict

