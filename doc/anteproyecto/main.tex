\documentclass[a4paper,spanish, 11pt]{article}
\usepackage{custom}
\hyphenation{in-te-ra-cción}

\begin{document}

\frontpage
\clearpage

\tableofcontents
\clearpage

\section{Introducción}

En 1969, Leonard Kleinrock, uno de los creadores de ARPANET, la red precursora de
Internet, auguró que en un futuro, con el desarrollo de las redes de computadores, ciertas
«utilidades de computación» estarían presentes en los hogares y oficinas del mismo modo
que ya lo estaban los servicios eléctricos y telefónicos~\cite{UCLA}.

Actualmente, nos estamos acercando a esa visión. Bajo el paradigma emergente de Cloud
Computing (o computación «en la nube») se pretende que los consumidores puedan acceder a
un conjunto de recursos de computación (servidores, aplicaciones, almacenamiento\...)
ubicados en cualquier parte del mundo, de una manera cómoda y bajo demanda.

\subsection{Aspectos generales del Cloud Computing}
Una infraestructura cloud, es un conjunto de recursos hardware y software que hace posible
un servicio con una serie de características esenciales~\cite{nist}:

\begin{itemize}
\item \textit{Autoservicio bajo demanda}: el cliente decide los recursos que necesita,
 sin apenas interacción con el proveedor.

\item \textit{Acceso a través de Internet}: la información y recursos son accesibles
  virtualmente desde cualquier parte, mediante mecanismos estándar.

\item\textit{Reserva de recursos}: Los recursos se ponen a servicio de múltiples
 clientes, y se asignan dinámicamente dependiendo de las necesidades de los mismos.

\item \textit{Elasticidad}: Los recursos se proporcionan conforme a la demanda de los
  clientes, permitiendo escalar la aplicación y mostrándose ilimitados a ojos de los
  clientes.

\item \textit{Medidas de consumo}: El tiempo y cantidad de recursos usados deben poder ser
  monitorizados y optimizados, con el fin de ofrecer transparencia en el caso de modelos
  de negocio en los que se paga por el uso de tales.
\end{itemize}

\noindent
El concepto de Cloud Computing engloba principalmente tres modelos de servicio:

\begin{itemize}
\item \textbf{El software como un servicio, SaaS (Software as a Service)} Permite a los
  clientes acceder a las aplicaciones que se encuentran ejecutando en una infraestructura
  cloud. El usuario no administra ningún aspecto de esta infraestructura. Podemos
  encontrar un ejemplo de este tipo de servicio en los servidores de correo basados en
  web.

\item \textbf{La plataforma como un servicio, PaaS (Platform as a Service)} Este enfoque
  ofrece a los clientes la posibilidad de desplegar sus propias aplicaciones en una
  infraestructura cloud. El usuario tiene control sobre el comportamiento de la
  aplicación, pero no sobre otros elementos de la infraestructura (redes, servidores,
  sistemas operativos, frecuentemente el lenguaje de las aplicaciones...). Un ejemplo de
  ello es Google App Engine~\cite{appengine}, que permite alojar aplicaciones web escritas
  en Python.

\item \textbf{La infraestructura como servicio, IaaS (Infraestructure as a Service)} En
  este caso, el usuario sí tiene control sobre ciertas características de la
  infraestructura, como pueden ser el almacenamiento o sistemas operativos, y es posible
  que despliegue y ejecute sus aplicaciones sobre ésta. Los computadores utilizados
  generalmente se tratan de máquinas virtuales en lugar de máquinas físicas.  Este tipo de
  servicio es el que implementaremos en el PFC que nos ocupa.
\end{itemize}

Otros enfoques consideran también el modelo de \textit{red como un servicio} (Network as a
Service - NaaS), que provee al usuario de ciertos servicios de conectividad, como por
ejemplo ancho de banda bajo demanda.


Las ventajas que ofrecen los sistemas de Cloud Computing son especialmente útiles en tres
casos particulares~\cite{view}:

\begin{itemize}
\item Cuando la demanda de un servicio varía con el tiempo. Por ejemplo, en sistemas que
  cuentan con un pico de carga unos pocos días al mes, cuando el resto del tiempo estarían
  infrautilizados, el Cloud Computing permite alquilar por tiempo los recursos necesarios,
  en lugar de adquirirlos definitivamente, lo que supone un ahorro de coste económico.

\item Cuando de antemano no se conoce la demanda del sistema. Por ejemplo, en webs en las
  que el número de visitantes puede variar drásticamente.

\item En casos en los que es necesario el procesamiento de grande lotes de datos que es
  posible procesar paralelamente. Se podrían usar 1000 máquinas de un sistema de Cloud
  Computing durante una hora, en lugar de una sola máquina durante 1000 horas.
\end{itemize}

\subsection{Algunas plataformas de Cloud Computing}

Cloud Computing es una de las tendencias tecnológicas actuales. Organizaciones de todo
el mundo, tanto académicas como industriales están investigando y desarrollando
tecnologías e infraestructuras enfocadas al Cloud Computing~\cite{emerging}. A
continuación mostramos algunos ejemplos de plataformas desarrolladas.

\subsubsection{Amazon Elastic Compute Cloud - EC2}

Amazon EC2~\cite{ec2} es un ejemplo significativo de infraestructura como
servicio. Desarrollado por Amazon, permite a los usuarios ejecutar aplicaciones basadas en
Linux. Los usuarios pueden crear instancias de servidores (AMIs) y subirlas aun servicio
de almacenamiento (Amazon Simple Storage Service (S3)~\cite{s3}) y, después, ejecutarlas,
detenerlas o monitorizarlas. El servicio EC2 cobra al usuario según el tiempo que se
ejecuta una instancia, y Amazon S3 según las transferencias de datos realizadas.

\subsubsection{Google App Engine - GAE}

Google App Engine~\cite{appengine} es un ejemplo de plataforma como servicio. Permite al
usuario ejecutar aplicaciones web escritas en Python. Además, también soporta APIs para
almacenamiento de datos, Google Accounts, manipulación de imágenes o servicio de
correo. Las aplicaciones se pueden administrar mediante una consola web. Su uso es
gratuito hasta los 500 MB de almacenamiento y alrededor de los 5 millones de visitas por
mes.

\subsubsection{Windows Azure}
Creada por Microsoft, Windows Azure~\cite{azure} es un ejemplo de plataforma como
servicio. Los usuarios pueden crear, alojar, controlar y escalar las aplicaciones a través
de unos centros de datos controlados por Microsoft.

\subsubsection{CloudStack}

Apache CloudStack es un software de código abierto que permite administrar una gran
cantidad de máquinas virtuales que forman parte de una plataforma de Cloud Computing del tipo
de infraestructura como servicio.  Además de las características propias de una plataforma
de este tipo, ofrece un API compatible con EC2~\cite{ec2} y S3~\cite{s3}, lo que permite a
los usuarios desplegar un modelo híbrido.

\subsubsection{OpenStack}

OpenStack~\cite{openstack} se trata de un proyecto de software libre, al cual se han unido
más de 150 empresas tales como Intel, AMD, Canonical o Cisco. Ofrece un servicio de tipo
IaaS. Está compuesto por diferentes módulos, encargados del cómputo de la aplicación
(Nova), el almacenamiento (Swift) o el sistema de direcciones de red (Quantum), entre
otros.

\subsection{El middleware ZeroC Ice}

Para llevar a cabo la implementación de \textit{IceCloud}, nuestro servicio de Cloud Computing,
haremos uso del middleware ZeroC Ice~\cite{ice}.

Ice ofrece soporte para construir aplicaciones distribuidas. Permite crear sistemas con
tolerancia a fallos y proporciona herramientas para facilitar el balanceo de carga.  Entre
los lenguajes que soporta se incluyen C++, .NET, Java o Python.


\subsubsection{IceGrid}

Uno de los servicios principales de Ice, que será clave en el desarrollo del proyecto, es
IceGrid~\cite{iceman}, el servicio responsable de la gestión de aplicaciones remotas
distribuidas, replicación de objetos, balance de carga y transparencia de localización. A
continuación se explican algunas de las características de IceGrid que serán útiles en
nuestro proyecto:

\begin{itemize}
\item Servicio de localización: permite a los programas clientes acceder indirectamente a
  los servidores, independientemente del host en que se encuentren. Dota a la aplicación
  de flexibilidad ante los cambios.

\item Activación bajo demanda de los servidores: es posible configurar IceGrid para que
  active los servidores únicamente cuando un cliente intenta accederlos.

\item Distribución de la aplicación: mediante su servicio IcePatch2 es posible distribuir
  la aplicación, es decir, copiar los archivos necesarios automáticamente en los nodos que
  lo requieran.

\item Replicación y balanceo de carga: es posible configurar la aplicación para que varios
  nodos respalden un mismo servidor. Esto mejora la fiabilidad y escalabilidad, y
  además permite cierto balanceo de carga.

\item Administración: es posible administrar las aplicaciones tanto por línea de comandos,
  como gráficamente con \textit{icegrid-gui}.

\item Despliegue: es posible indicar las opciones de despliegue de la aplicación y
  describir los programas servidores que deben ser instalados en cada ordenador mediante
  plantillas escritas en XML. Se requiere que el usuario especifique explícitamente los
  nodos donde ejecutarlos, incluso aunque esos nodos sean idénticos para el contexto de la
  aplicación. En este PFC se intentará subsanar ese aspecto para lograr la elasticidad
  necesaria para el sistema de Cloud Computing.
\end{itemize}


\section{Objetivos}

El objetivo principal es diseñar e implementar una infraestructura de computación
\emph{elástica}, que permita repartir dinámicamente una carga de trabajo cualquiera
correspondiente a una aplicación distribuida sobre un conjunto de nodos con ciertas
características especificadas por el usuario.

\subsection{Objetivos específicos}

Para ello, la herramienta debe cumplir los siguientes subobjetivos:

\subsubsection{Creación de plantilla}

Las características del nodo o nodos en que debería ejecutarse cada componente de la
aplicación indicando, por ejemplo, la arquitectura del nodo. Será necesario la creación de
un formato de plantilla en XML que permita especificar las necesidades de memoria o
sistema operativo. También será necesaria la creación de una herramienta que tome como
entrada dicha plantilla y, según la descripción incluida en la misma y los componentes
existentes en el grid, genere otra adecuada a la estructura real del sistema en el formato
usado por IceGrid.

\subsubsection{Seguimiento de los nodos}

Será necesaria la monitorización a cada instante del estado de los nodos para permitir
adaptar la aplicación en tiempo de ejecución según los recursos disponibles. De este modo,
cuando algún nodo sea liberado, se le podrá asignar otro servidor cuyos requerimientos
especificados en la plantilla de la aplicación sean compatibles. Esto será posible gracias
a la clase \textit{NodeDynamicInfo} de API de IceGrid, que contiene información sobre los
servidores y adaptadores contenidos en un nodo, además de un miembro de la clase
\textit{NodeInfo}, que nos informa de las características del nodo (sistema operativo,
tipo de máquina, número de procesadores\...). Así mismo se requiere completar esta
información creando objetos distribuidos que informes sobre todas aquellas variables que
se deseen tener en cuenta como criterio del usuario para la selección automática de los
nodos.

\subsubsection{Computación elástica}

Habrán de implementarse los mecanismos necesarios para computación elástica. Es decir, que
faciliten al usuario poder escalar la aplicación en tiempo de ejecución, creando nuevas
instancias de servidores si es necesario, o liberando los recursos adquiridos mediante la
detención de los servicios ejecutados en los mismos.

\subsubsection{Interacción directa}

En una segunda fase, se propone la gestión directa de los nodos por parte de IceCloud si
las restricciones que impone IceGrid no fueran asumibles. Será necesario evaluar éstas
para determinar si ese objetivo es realmente necesario.

\section{Métodos y fases de trabajo}

El desarrollo del proyecto se realizará a lo largo de una serie de fases que
tienen como fin la consecución de los objetivos descritos en el punto anterior.
Es posible que durante el desarrollo aparezcan nuevos requisitos, por lo que
se tratará de un proceso iterativo, de forma que se podrá pasar varias veces
por las etapas detalladas a continuación.

\begin{enumerate}
\item \textbf{Estudio del middleware ZeroC Ice} En primer lugar se hará un estudio general
  de las funcionalidades que ofrece el middleware ZeroC Ice.  Especialmente, por su
  importancia en el proyecto, se analizará el funcionamiento de IceGrid, el módulo
  encargado del despliegue de aplicaciones, balanceo de carga y replicación de servidores,
  entre otras funciones.

\item \textbf{Análisis y diseño conceptual} Se hará un análisis exhaustivo del sistema a
  desarrollar, realizando a fondo un análisis de requisitos.  También se llevará a cabo
  una fase de diseño, generando un modelo del sistema mediante distintos diagramas.

\item \textbf{Implementación} En esta fase se codificará el sistema, teniendo en cuenta
  los diseños realizados en la etapa anterior. Paralelamente, se diseñarán y realizarán
  pruebas unitarias de cada funcionalidad del sistema.

\item \textbf{Evaluación y pruebas} Se realizarán una serie de pruebas de integración para
  comprobar el correcto funcionamiento del sistema. Concretamente, una de las pruebas será
  la utilización del sistema para llevar a cabo el renderizado de una animación 3D con el
  programa Blender.

\item \textbf{Documentación} Por último, se generará la documentación, recogiendo la
  información generada durante el proceso. Se detallarán los pasos seguidos para la
  elaboración del sistema, las decisiones tomadas a lo largo de la misma, así como los
  problemas e inconvenientes surgidos durante su desarrollo. Tal documentación será
  incluida en el documento final del PFC.
\end{enumerate}


\section{Medios que se pretenden utilizar}

Las herramientas y entornos de programación que se utilizarán son los siguientes:

\subsection{Recursos software}

\begin{itemize}
\item Python e iPython: intérpretes del lenguaje de programación Python.
\item SPE: Entorno de desarrollo para Python~\cite{spe}.
\item Middleware ZeroC Ice~\cite{ice}: Proporciona soporte y servicios avanzados para el desarrollo
de aplicaciones distribuidas.
\item Kile:  Editor e intérprete de órdenes de archivos fuente \TeX \ y \LaTeX. Usado
para la creación de este documento. También será utilizado para la memoria del
proyecto.~\cite{kile}
\item Mercurial~\cite{mercurial}: Sistema de control de versiones usado para gestionar tanto las
distintas versiones del proyecto como la documentación.
\end{itemize}

Los lenguajes que se pretenden utilizar para la realización del proyecto serán:

\begin{itemize}
\item Python: lenguaje interpretado de programación orientado a objetos~\cite{python}.
\item \LaTeX: lenguaje de marcado de documentos, utilizado para realizar este documento y,
  en un futuro, la memoria del proyecto~\cite{latex}.
\item BIB\TeX: herramienta para generar las listas de referencias tanto de este documento
  como de la memoria del proyecto.
\end{itemize}

\subsection{Recursos hardware}
\begin{itemize}
\item Portátil ASUS K53S.
\item Notebook LG X130.
\end{itemize}

\clearpage
\begin{thebibliography}{biblio}
\bibitem{UCLA} L. Kleinrock, UCLA to be First Station in Nationwide Computer Network, UCLA
  Press Release, July 1969.

\bibitem{nist}The NIST Definition of Cloud Computing.  National Institute of Standards and
  Technology.  24 Julio 2011.

\bibitem{itu} International Telecommunication Union (ITU) TELECOMMUNICATION
  STANDARDIZATION SECTOR OF ITU. ITU Focus Group on Cloud Computing - Part 1.  16
  Diciembre 2012.

\bibitem{eric} Cloud computing in Telecommunications. Jan Gabrielsson et al. Ericsson
  Review.1 .  16 Diciembre 2012.

\bibitem{view} Michael Armbrust et al.  A View of Cloud Computing.Revista Communications
  of the ACM, vol. 53 no. 4, pags 50-58. Abril 2010

\bibitem{emerging} Buyya, Rajkumar, et al. Cloud computing and emerging IT platforms:
  Vision, hype, and reality for delivering computing as the 5th utility. Future Generation
  computer systems, 2009, vol. 25, no 6, p. 599-616.

\bibitem{ec2} Amazon Elastic Compute Cloud (Amazon EC2), \url{http://aws.amazon.com/ec2/}

\bibitem{s3} Amazon Simple Storage Service (Amazon S3), \url{http://aws.amazon.com/s3}

\bibitem{appengine} Google App Engine (GAE) , \url{http://appengine.google.com}

\bibitem{azure} Windows Azure , \url{http://www.windowsazure.com/}

\bibitem{cs} CloudStack, \url{http://cloudstack.apache.org/}

\bibitem{openstack} OpenStack, \url{http://www.openstack.org/}

\bibitem{ice} Internet Communications Engine, ZeroC, \url{http://www.zeroc.com}

\bibitem{iceman} Manual Ice \url{http://doc.zeroc.com/display/Ice/IceGrid}

\bibitem{spe} Stani's Python Editor \url{http://sourceforge.net/projects/spe/}

\bibitem{kile} Kile \url{ http://kile.sourceforge.net/}

\bibitem{mercurial} Mercurial \url{ http://mercurial.selenic.com/}
  \bibitem{python} Python Reference Manual (version 2.6.7), van Rossum, G., 12
  Junio 2011, \url{http://docs.python.org/release/2.6.7/reference/index.html}

\bibitem{latex} LaTeX, una imprenta en sus manos; Cascales Salinas, Bernardo y Lucas
  Saorín, Pascual y Mira Ros, José Manuel y Pallarés Ruiz, Antonio Sánchez-Pedreño
  Guillén, Salvador; Aula Documental de Investigación, 2000.
\end{thebibliography}

\end{document}

% Local Variables:
% fill-column: 90
%  coding: utf-8
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
