\chapter{Introducción}

En 1969, Leonard Kleinrock, uno de los creadores de ARPANET, la red precursora de
Internet, auguró que en un futuro, con el desarrollo de las redes de computadores, ciertas
«utilidades de computación» estarían presentes en los hogares y oficinas del mismo modo
que ya lo estaban los servicios eléctricos y telefónicos~\cite{UCLA}.

Hoy en día, nos estamos acercando a esa visión. Bajo el paradigma emergente de Cloud
Computing (o computación «en la nube») se pretende que los consumidores puedan acceder a
un conjunto de recursos de computación (servidores, aplicaciones, almacenamiento\...)
ubicados en cualquier parte del mundo, de una manera cómoda y bajo demanda.

Una infraestructura \emph{cloud} es un conjunto de recursos hardware y software
que hace posible
un servicio accesible a través de Internet mediante el cual el cliente puede
decidir los recursos que necesita, solicitar una \emph{asignación dinámica} y automática
(\emph{elástica}) de los
mismos, ejecutar aplicaciones escalables sobre ellos y pagar
únicamente por el tiempo y cantidad de uso de dichos recursos. Actualmente,
empresas
punteras en el sector de los servicios informáticos como Amazon o Google ofrecen
sus propias plataformas de Cloud Computing.

El concepto de Cloud Computing engloba principalmente tres modelos de
servicio:
\begin{itemize}
 \item \textbf{El software como servicio, \acs{SaaS}}: Permite a los clientes
  acceder a las aplicaciones que se encuentran ejecutando en una
  infraestructura cloud.
 \item \textbf{La plataforma como servicio, \acs{PaaS}}: Ofrece
  a los clientes la posibilidad de desplegar sus propias aplicaciones en una
  infraestructura cloud, sin tener control sobre los elementos de la
  infraestructura (redes, servidores, sistema operativo\...).
 \item \textbf{La infraestructura como servicio, \acs{IaaS}}: Permite a los
 clientes ejecutar y desplegar sus aplicaciones en una infraestructura sobre
 la que es posible decidir ciertos aspectos.
\end{itemize}

El objetivo del presente proyecto es el diseño y la implementación de una
plataforma de Cloud Computing del tipo \acs{IaaS}, usando para ello el
middleware de comunicaciones ZeroC \acs{Ice}. Con su servicio IceGrid, este
middleware proporciona la posibilidad de desplegar aplicaciones en un
sistema distribuido (grid\footnote{Un grid se puede definir básicamente
como una red de computadores (de relativo bajo coste), posiblemente con
diferente hardware y software, con la finalidad de procesar una tarea que
requiere gran cantidad de recursos.}), especificando explícitamente la estructura del
mismo y los servidores alojados en cada nodo del grid. IceGrid no es un servicio cloud
puesto que la definición del grid es estática y previa al arranque del sistema.

Este proyecto aborda el proceso mediante el cual se subsanan esas limitaciones de IceGrid
para crear un servicio \ac{IaaS} a partir de él,
permitiendo describir las aplicaciones con independencia de la arquitectura
del grid y así conseguir una \emph{asignación dinámica} dotando al sistema
de la \textbf{elasticidad} necesaria para que la asignación de recursos
crezca o decrezca de forma automática en función de los recursos disponibles
y las necesidades de la aplicación.

\section{Estructura del documento}

A continuación se detalla la estructura de este documento, describiendo brevemente el
contenido de cada uno de los capítulos que lo componen.

\begin{definitionlist}
\item[Capítulo \ref{chap:antecedentes}: \nameref{chap:antecedentes}]
En este capítulo se realiza una introducción al concepto de Cloud Computing y
sus diferentes modelos de servicio, presentando también algunas de las
plataformas de computación <<en la nube>> más utilizadas actualmente.

Además, se definen las nociones sobre sistemas distribuidos que son necesarias
para la comprensión del resto del documento y se explica en qué consiste el
middleware ZeroC \acs{Ice}, especialmente su servicio IceGrid.

\item[Capítulo \ref{chap:objetivos}: \nameref{chap:objetivos}]

El principal objetivo de este proyecto es el diseño e implementación de una
plataforma de Cloud Computing que permita desplegar aplicaciones sobre un
grid heterogéneo.

En este capítulo se detallarán los objetivos específicos que se pretenden
alcanzar a lo largo del desarrollo del proyecto.

\item[Capítulo \ref{chap:metodology}: \nameref{chap:metodology}]

La metodología elegida para el desarrollo de este proyecto es el prototipado incremental.

En este capítulo se describe la metodología seguida, además de especificar las
herramientas, tanto hardware como software, utilizadas para la elaboración de
este proyecto.

\item[Capítulo \ref{chap:desarrollo}: \nameref{chap:desarrollo}]

En este capítulo se encuentra detallado el trabajo realizado en cada una de
las iteraciones del proyecto.

\item[Capítulo \ref{chap:resultados}: \nameref{chap:resultados}]

Se describen los resultados obtenidos tras el desarrollo del sistema, así como
el esfuerzo realizado, estimaciones del esfuerzo y costes del proyecto, etc.

\item[Capítulo \ref{chap:conclusiones}: \nameref{chap:conclusiones}]

En este capítulo se resumen las conclusiones extraídas a lo largo de la
elaboración de este \acs{PFC} y se aportan varias propuestas de trabajo futuro
para enriquecer y completar el trabajo aquí realizado.

\end{definitionlist}
