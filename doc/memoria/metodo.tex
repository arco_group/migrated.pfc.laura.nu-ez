% -*- coding: utf-8 -*-

\chapter{Método de trabajo y herramientas}
\label{chap:metodology}

En este capítulo se introduce la metodología elegida para el desarrollo del
proyecto y se describen las herramientas, tanto hardware como software,
utilizadas para realización del mismo

\section{Metodología de trabajo}
\label{sec:metodologia}

Al comenzar el proyecto, teníamos una idea general de los requisitos a
desarrollar para implementar una plataforma de Cloud Computing, pero estos
requisitos no eran detallados, por lo que se decidió optar por un modelo de
desarrollo \emph{evolutivo} con el que refinar los resultados progresivamente
a lo largo de diferentes iteraciones.

El esquema \ref{fig:modeloevolutivo}
muestra el diagrama de flujo de un modelo evolutivo, en el que se generan
varias versiones intermedias del sistema antes de obtener la versión final.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.8\textwidth]{modeloevolutivo.png}
    \caption{Diagrama de flujo del modelo evolutivo}
    \label{fig:modeloevolutivo}
  \end{center}
\end{figure}

\subsection{Prototipado evolutivo}
\label{sec:prototipado_evolutivo}

El \emph{prototipado evolutivo}~\cite{sommerville} es un paradigma basado en la
realización de prototipos funcionales a lo largo del desarrollo del sistema
hasta llegar a un producto final. Se parte de los requisitos mejor comprendidos
y con mayor prioridad, y se van definiendo los detalles conforme avanza el
desarrollo a través de diferentes versiones.

Con un paradigma de prototipado es esencial la comunicación con el cliente. Tras
la elaboración de un prototipo, se entrega al cliente y éste evalúa los
resultados, aportando retroalimentación para mejorar los
requisitos~\cite{enfoquepractico}, sirviendo así como mecanismo para identificar
los requisitos del software. El prototipo evaluado se modifica según los
nuevos requisitos hasta transformase en el sistema real.

El esquema general de este paradigma se muestra en
la figura~\ref{fig:prototipado}. En cada iteración se diseña y construye un
prototipo que es integrado en el sistema y puesto a disposición del cliente. Tras
la entrega, se procede a realizar la siguiente iteración. Basándose en la
evaluación del cliente se definen nuevas funcionalidades y se elabora el
siguiente prototipo.

\begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{prototipado.png}
    \caption{Paradigma de prototipado evolutivo}
    \label{fig:prototipado}
  \end{center}
\end{figure}

Las ventajas del prototipado evolutivo son las siguientes:
\begin{itemize}
\item Los clientes obtienen prototipos utilizables del sistema prácticamente
desde el inicio del desarrollo, lo que aumenta la confianza en la obtención de
un producto final que cumpla con los requisitos, ya que los prototipos aportan
la sensación de sistema real que satisface las necesidades inmediatas del
cliente.

\item Al evaluar los prototipos intermedios, se facilita la tarea de identificar
si los requisitos planteados son correctos y viables.

\item Se minimiza el riesgo de errores, y éstos pueden solventarse más fácilmente.
En cada iteración se realizan pruebas de los nuevos cambios y se comprueba que se
siguen cumpliendo los requisitos de las iteraciones anteriores. En caso de
errores en una de las iteraciones, no se trasladan a las iteraciones posteriores.

\item Las funcionalidades con mayor prioridad y más importantes son desarrolladas
en las primeras fases, y son, por tanto, las más probadas a lo largo del proceso,
lo que otorga robustez y fiabilidad al sistema.
\end{itemize}

No obstante, el prototipado evolutivo presenta generalmente dos problemas:
\begin{itemize}
\item Debido a la constante interacción con el cliente para introducir nuevos
requisitos, los recursos del proceso de desarrollo son difíciles de estimar y
no es fácil producir documentación que refleje cada versión del sistema.

\item El mantenimiento del sistema puede ser problemático debido a la
  especificación de nuevas funcionalidades en su desarrollo puesto que los
  cambios continuos corrompen la arquitectura del software y hacen que introducir
  nuevos cambios sea cada vez más difícil y costoso.
\end{itemize}

Estas desventajas se hacen particularmente manifiestas en sistemas grandes. Para
sistemas de pequeño y medio tamaño como el planteado en este \acs{PFC}, el
enfoque evolutivo no ofrece ningún problema, pero para sistemas grandes es
recomendable usar un modelo híbrido que combine características de un modelo
en cascada para desarrollar las partes del sistema bien conocidas y
características del enfoque evolutivo para definir con más precisión
los requisitos del sistema que presenten mayor incertidumbre.

\section{Herramientas}
\label{sec:herramientas}

\subsection{Lenguajes de programación}
\label{sec:lenguajes}
Para la elaboración de este proyecto se han utilizado los siguientes lenguajes
de programación:

\begin{description}
\item[Python]

  Lenguaje de programación de alto nivel, interpretado, multiparadigma y
  orientado a objetos, desarrollado por Guido van Roosum. Ha sido el lenguaje
  para implementanción tanto del sistema, como de las pruebas realizadas debido
  a su sencillez y multitud de librerías.

\item[Slice]

Lenguaje creado por ZeroC para definir las interfaces de los
objetos y ofrecer independencia entre su especificación y la implementación de
los mismos, permitiendo que dicha implementación pueda realizarse en distintos
lenguajes.

\item[\acx{XML}]

Lenguaje de marcas desarrollado por \ac{W3C} utilizado para almacenar datos de forma legible. Permite el intercambio
de información estructurada entre diferentes plataformas. La estructura de un
documento \ac{XML} se compone de partes bien definidas representables en forma de árbol.
Por ello, ha sido el lenguaje elegido para especificar categorías en las que agrupar los
nodos del sistema y para especificar las aplicaciones que deben ser desplegadas
en IceCloud.

\end{description}

\subsection{Herramientas hardware}
\label{hardware}
Para el desarrollo de IceCloud se ha utilizado un equipo portátil con las siguientes
características:
\begin{itemize}
 \item Intel\textsuperscript{\circledR} Core{\texttrademark}  i5-2450M.
 (2.50GHz x 4)
 \item 8GB RAM
 \item 500GB Disco duro.
\end{itemize}

\subsection{Software}
A continuación se detallan las herramientas y bibliotecas software que han sido
necesarias para el desarrollo de este proyecto:

\subsubsection{Herramientas de virtualización}
\begin{itemize}
 \item \textbf{Oracle VM VirtualBox} Es un software de virtualización
 desarrollado por Oracle que permite utilizar máquinas virtuales para simular
 máquinas reales. Ha sido utilizado para simular el grid sobre el que se
 ejecuta IceCloud.
\end{itemize}


\subsubsection{Sistemas operativos}
\begin{itemize}
 \item \textbf{Linux Mint} Como principal sistema operativo en la realización de este
 proyecto se ha utilizado Linux Mint Mate 16 ``Petra'' con núcleo Linux
 3.11.0-12-amd64 instalado en el equipo portátil indicado en la sección~\ref{hardware}.
\item \textbf{Debian GNU/Linux} En las máquinas virtuales utilizadas para formar parte del grid
  de IceCloud, se ha utilizado el sistema operativo Debian 7.0 ``Wheezy'' 32-bit con
  núcleo Linux 3.2.0-4-486.
\end{itemize}

\subsubsection{Aplicaciones de desarrollo}
\label{sec:aplicaciones}

\begin{itemize}
\item \textbf{ZeroC Ice} Middleware de comunicaciones orientado a objetos,
  multilenguaje y multiplataforma desarrollado por la empresa ZeroC~\cite{ice}.
  Utilizado para realizar la comunicación entre clientes y servidores, y,
  especialmente como base para definir el grid y llevar a cabo el despliegue de
  aplicaciones mediante su servicio IceGrid.

\item \textbf{Eclipse} Entorno de programación~\cite{eclipse} utilizado para el
  desarrollo de la aplicación IceCloud. La versión utilizada ha sido 4.3 ``Kepler'',
  con el plugin \textbf{PyDev}~\cite{pydev} que ofrece un entorno de desarrollo
  de proyectos en Python para Eclipse.

\item \textbf{PyUnit} (también conocido como \textbf{unittest}). Framework integrado en
Eclipse mediante PyDev utilizado para la
implementación y ejecución de pruebas en Python.

\item \textbf{python-doublex} Framework de dobles de prueba para Python creado por
David Villa~\cite{doublex}. Utilizado
para la realización de los tests unitarios del sistema.

\item \textbf{lxml} Biblioteca para el procesamiento de ficheros XML en Python~\cite{lxml}.
Utilizada para procesar los archivos de definición de categorías y aplicaciones.

\item \textbf{lshw} Herramienta que suministra información sobre
 el hardware de la máquina ~\cite{lshw}. Con ella, es posible obtener información sobre la
 configuración de la memoria, la versión de~\acs{CPU}, firmware, caché, etc. Su
 salida se puede obtener en formato \ac{XML}.
 Utilizada para obtener características de los nodos del grid.

\item \textbf{Blender} Programa dedicado al modelado, iluminación, renderizado, animación
y diseño de gráficos 3D. Se ha utilizado en la implementación de una aplicación
distribuida para el renderizado de frames con la finalidad de probar IceCloud
con una aplicación con utilidad real~\cite{blender}.

\end{itemize}

\subsubsection{Documentación y gráficos}
\label{sec:documentacion}

\begin{itemize}
\item \textbf{\LaTeX} Lenguaje de marcado para composición y edición de documentos utilizado
para la realización de esta memoria~\cite{latex}.

\item \textbf{BibTex} Herramienta para la descripción de referencias para documentos
  escritos en \LaTeX. Usada para crear las referencias de este documento.

\item \textbf{Kile} Editor de documentos e intérprete de órdenes de archivos fuente
{\TeX} y {\LaTeX}~\cite{kile}.
\item \textbf{Dia} Programa para la creación de diagramas, con soporte para modelado
\ac{UML}. Utilizado para la creación de algunos de los diagramas mostrados en este
documento.

\item \textbf{Gimp} Programa de manipulación de imágenes de \ac{GNU}. Utilizado para la
creación de algunas de las imágenes incluidas en esta memoria.

\end{itemize}

\subsubsection{Control de versiones}

\begin{itemize}
\item \textbf{Mercurial} Sistema de control de versiones distribuido. Usado para gestionar
los cambios del código del proyecto y la
  documentación~\cite{mercurial}~\cite{mercurialbook}, junto a un repositorio
  alojado en \emph{BitBucket}\footnote{\url{https://bitbucket.org/arco_group/pfc.icecloud}}.
\end{itemize}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
