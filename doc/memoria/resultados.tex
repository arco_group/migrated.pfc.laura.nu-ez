\chapter{Resultados}
\label{chap:resultados}

Em esta sección se muestra un ejemplo de aplicación de la herramienta
desarrollada. Además, se presenta un análisis del esfuerzo de desarrollo del
proyecto y una estimación del coste que ha supuesto la realización del mismo.

\section{Aplicación de la herramienta}

Para comprobar el funcionamiento de IceCloud con una aplicación real, se ha implementado
una aplicación destinada a renderizar frames de una animación con Blender, creada en la
iteración 8 (ver sección~\ref{it8}) del proceso de desarrollo.  El funcionamiento de esta
aplicación se basa en la existencia de una serie de servidores \textit{Renderer} que
solicitan frames a un servidor \textit{Deliverer} y le envían los resultados tras
renderizarlos (ver figura~\ref{fig:servidoresblender}).

 \begin{figure}[!h]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{servidoresblender.png}
    \caption{Estructura de la aplicación de renderizado}
    \label{fig:servidoresblender}
  \end{center}
\end{figure}

La prueba ha sido realizada en un grid compuesto por tres nodos
(\textit{grid-node0}, \textit{grid-node1} y \textit{grid-node2}) con las
siguientes características de memoria \ac{RAM}:

\begin{itemize}
 \item \textbf{grid-node0}: 7762 MB
 \item \textbf{grid-node1}: 4000 MB
 \item \textbf{grid-node2}: 2000 MB
\end{itemize}

 El nodo \textit{grid-node0} ha sido alojado en el portátil descrito en
 la sección \ref{hardware}, y el resto han sido alojados en máquinas virtuales
 ejecutadas en ese mismo portátil. Dadas las limitaciones que esto conlleva en
 lo referente a la fluctuación del uso de los recursos de las máquinas (como,
 por ejemplo, la dependencia del uso CPU entre máquinas virtuales), los
 servidores de la aplicación se han definidos en términos de la memoria \ac{RAM}
 de los nodos, por lo que el resto de características son irrelevantes.

 Según el fichero de categorías suministrado, todos los nodos pertenecen a la
 categoría \textit{any-server}.
 La aplicación, que se utilizará en este ejemplo para renderizar una animación
 con 320 frames, cuenta con los siguientes elementos:
 \begin{itemize}
  \item Un servidor IcePatch alojado en \textit{grid-node0}, ya que este nodo
  se encuentra en misma máquina donde está el código fuente de la aplicación.
  \item Un servidor \textit{Deliverer} alojado también en \textit{grid-node0}.
  \item Un servidor \textit{Renderer} alojado en un servidor de categoría
  \texttt{any-server}, con \\
  \mbox{\texttt{allocationRule = "max\_free\_ram"}},
  \mbox{\texttt{growingRule = "free\_ram lt 4000"}},\\
  \mbox{\texttt{decreasingRule = "free\_ram gt 4000"}} y
  \mbox{\texttt{maxDupes = "8"}}. Es decir, un servidor
  que se alojará en el nodo con la máxima memoria \ac{RAM} libre, crecerá cuando la
  memoria libre sea menor de 4000 MB hasta un máximo de 8 copias
  y decrecerá cuando sea mayor de 4000 MB.
 \end{itemize}

 En el cuadro \ref{cuadro:evolucionrenders}
 se muestra la evolución de los servidores al ejecutarse la
 aplicación en función del tiempo respecto a la creación del primer servidor. Y
 en la figura \ref{fig:evolucion} se muestran varias capturas del estado
 del grid en diferentes instantes de la ejecución de la aplicación.

 \begin{table}[h!]
  \centering
  \input{tables/evolucion.tex}
  \caption{Evolución de los servidores}
  \label{cuadro:evolucionrenders}
\end{table}

 \begin{figure}[!h]
  \begin{center}
    \includegraphics[width=\textwidth]{evolucion.png}
    \caption{Evolución de los servidores durante la ejecución}
    \label{fig:evolucion}
  \end{center}
\end{figure}

Tras finalizar la ejecución, los servidores habían renderizado el número de
frames que se muestra en el cuadro~\ref{cuadro:framesrenderizados}. Se
aprecia una diferencia considerable entre los servidores alojados en el
nodo \textit{grid-node0} y el resto. Esto se debe a que el uso de CPU en el
proceso de renderizado es crucial, y el nodo \textit{grid-node0}, a diferencia
de los ejecutados en máquinas virtuales, dispone de  varios hilos de ejecución.
El total de frames renderizados es superior al número de frames de la
aplicación, debido a que el \textit{Deliverer} puede haber suministrado frames
a un servidor
que ya estaban en proceso de renderizado por parte de otros servidores, sin que
hubiesen terminado aún.

 \begin{table}[h!]
  \centering
  \input{tables/framesrenderizados.tex}
  \caption{Número total de frames renderizados por servidor}
  \label{cuadro:framesrenderizados}
\end{table}

\section{Costes y recursos}

A continuación se muestra un resumen de las líneas de código desarrolladas para
los lenguajes utilizados (ver cuadro~\ref{cuadro:lineascodigo}).

 \begin{table}[h!]
  \centering
  \input{tables/sloc_lenguajes.tex}
  \caption{Total líneas de código por lenguaje}
  \label{cuadro:lineascodigo}
\end{table}

En el listado \ref{code:costes} se muestra una estimación del esfuerzo realizado
en el desarrollo de este
proyecto. Para obtener estos datos, se ha utilizado la herramienta
\texttt{sloccount}, que analiza los
ficheros de un directorio dado, calcula el tamaño del proyecto en líneas de
código y emplea el modelo \ac{COCOMO} para obtener la estimación del
tiempo y los costes necesarios para desarrollar el sistema. Dicha herramienta
no tiene en cuenta los ficheros Slice. Tampoco se ha valorado la elaboración de
esta memoria.

\lstset{rulesepcolor=\color{gray95},
	numbers=none
}

\begin{listing}[
  language = Bash,
  caption = {Estimación de costes del proyecto},
  label = code:costes]
Total Physical Source Lines of Code (SLOC)                = 5,409
Development Effort Estimate, Person-Years (Person-Months) = 1.18 (14.12)
 (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))
Schedule Estimate, Years (Months)                         = 0.57 (6.84)
 (Basic COCOMO model, Months = 2.5 * (person-months**0.38))
Estimated Average Number of Developers (Effort/Schedule)  = 2.07
Total Estimated Cost to Develop                           = \$ 159,006
 (average salary = \$56,286/year, overhead = 2.40).
\end{listing}



\section{Repositorio}

El código correspondiente al desarrollo del proyecto IceCloud y la
documentación de esta memoria están disponibles en un repositorio alojado
en \emph{BitBucket}\footnote{\url{https://bitbucket.org/}}, que puede ser
descargado con la siguiente orden:

\begin{listing}
hg clone https://bitbucket.org/arco_group/pfc.icecloud
\end{listing}

\lstset{numbers=left,
	rulesepcolor = \color{black}
}
